checkout package
================

Subpackages
-----------

.. toctree::

    checkout.management
    checkout.migrations
    checkout.templatetags
    checkout.tests

Submodules
----------

checkout.admin module
---------------------

.. automodule:: checkout.admin
    :members:
    :undoc-members:
    :show-inheritance:

checkout.forms module
---------------------

.. automodule:: checkout.forms
    :members:
    :undoc-members:
    :show-inheritance:

checkout.models module
----------------------

.. automodule:: checkout.models
    :members:
    :undoc-members:
    :show-inheritance:

checkout.service module
-----------------------

.. automodule:: checkout.service
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tasks module
---------------------

.. automodule:: checkout.tasks
    :members:
    :undoc-members:
    :show-inheritance:

checkout.urls module
--------------------

.. automodule:: checkout.urls
    :members:
    :undoc-members:
    :show-inheritance:

checkout.views module
---------------------

.. automodule:: checkout.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: checkout
    :members:
    :undoc-members:
    :show-inheritance:
