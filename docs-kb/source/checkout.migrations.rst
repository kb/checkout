checkout.migrations package
===========================

Submodules
----------

checkout.migrations.0001\_initial module
----------------------------------------

.. automodule:: checkout.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

checkout.migrations.0002\_auto\_20150625\_1159 module
-----------------------------------------------------

.. automodule:: checkout.migrations.0002_auto_20150625_1159
    :members:
    :undoc-members:
    :show-inheritance:

checkout.migrations.0003\_auto\_20150903\_0849 module
-----------------------------------------------------

.. automodule:: checkout.migrations.0003_auto_20150903_0849
    :members:
    :undoc-members:
    :show-inheritance:

checkout.migrations.0004\_auto\_20150907\_1607 module
-----------------------------------------------------

.. automodule:: checkout.migrations.0004_auto_20150907_1607
    :members:
    :undoc-members:
    :show-inheritance:

checkout.migrations.0005\_auto\_20151002\_2010 module
-----------------------------------------------------

.. automodule:: checkout.migrations.0005_auto_20151002_2010
    :members:
    :undoc-members:
    :show-inheritance:

checkout.migrations.0006\_customerpayment module
------------------------------------------------

.. automodule:: checkout.migrations.0006_customerpayment
    :members:
    :undoc-members:
    :show-inheritance:

checkout.migrations.0007\_objectpaymentplaninstalment\_retry\_count module
--------------------------------------------------------------------------

.. automodule:: checkout.migrations.0007_objectpaymentplaninstalment_retry_count
    :members:
    :undoc-members:
    :show-inheritance:

checkout.migrations.0008\_auto\_20160519\_2033 module
-----------------------------------------------------

.. automodule:: checkout.migrations.0008_auto_20160519_2033
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: checkout.migrations
    :members:
    :undoc-members:
    :show-inheritance:
