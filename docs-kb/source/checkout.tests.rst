checkout.tests package
======================

Submodules
----------

checkout.tests.factories module
-------------------------------

.. automodule:: checkout.tests.factories
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.helper module
----------------------------

.. automodule:: checkout.tests.helper
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.test\_checkout\_settings module
----------------------------------------------

.. automodule:: checkout.tests.test_checkout_settings
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.test\_checkout\_state module
-------------------------------------------

.. automodule:: checkout.tests.test_checkout_state
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.test\_customer module
------------------------------------

.. automodule:: checkout.tests.test_customer
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.test\_customer\_payment module
---------------------------------------------

.. automodule:: checkout.tests.test_customer_payment
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.test\_management\_command module
-----------------------------------------------

.. automodule:: checkout.tests.test_management_command
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.test\_payment\_plan module
-----------------------------------------

.. automodule:: checkout.tests.test_payment_plan
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.test\_tasks module
---------------------------------

.. automodule:: checkout.tests.test_tasks
    :members:
    :undoc-members:
    :show-inheritance:

checkout.tests.test\_view\_perm module
--------------------------------------

.. automodule:: checkout.tests.test_view_perm
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: checkout.tests
    :members:
    :undoc-members:
    :show-inheritance:
