checkout.templatetags package
=============================

Submodules
----------

checkout.templatetags.checkout\_tags module
-------------------------------------------

.. automodule:: checkout.templatetags.checkout_tags
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: checkout.templatetags
    :members:
    :undoc-members:
    :show-inheritance:
