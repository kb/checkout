example\_checkout package
=========================

Subpackages
-----------

.. toctree::

    example_checkout.management
    example_checkout.tests

Submodules
----------

example\_checkout.base module
-----------------------------

.. automodule:: example_checkout.base
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.celery module
-------------------------------

.. automodule:: example_checkout.celery
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.dev\_greg module
----------------------------------

.. automodule:: example_checkout.dev_greg
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.dev\_malcolm module
-------------------------------------

.. automodule:: example_checkout.dev_malcolm
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.dev\_patrick module
-------------------------------------

.. automodule:: example_checkout.dev_patrick
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.dev\_test module
----------------------------------

.. automodule:: example_checkout.dev_test
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.forms module
------------------------------

.. automodule:: example_checkout.forms
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.models module
-------------------------------

.. automodule:: example_checkout.models
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.urls module
-----------------------------

.. automodule:: example_checkout.urls
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.views module
------------------------------

.. automodule:: example_checkout.views
    :members:
    :undoc-members:
    :show-inheritance:

example\_checkout.wsgi module
-----------------------------

.. automodule:: example_checkout.wsgi
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: example_checkout
    :members:
    :undoc-members:
    :show-inheritance:
