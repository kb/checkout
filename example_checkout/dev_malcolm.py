# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from .base import *

DATABASE = "dev_test_checkout_malcolm"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": DATABASE,
        "USER": get_env_variable("DATABASE_USER"),
        "PASSWORD": get_env_variable("DATABASE_PASS"),
        "HOST": get_env_variable("DATABASE_HOST"),
        "PORT": "",
    }
}

MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)

INSTALLED_APPS += ("django_extensions", "debug_toolbar")


# force the debug toolbar to be displayed
def show_toolbar(request):
    return True


DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": show_toolbar,
    "INTERCEPT_REDIRECTS": False,
    "ENABLE_STACKTRACES": True,
}
