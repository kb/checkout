# -*- encoding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.urls import reverse

from checkout.models import (
    CheckoutState,
    default_checkout_state,
    get_contact_model,
)
from stock.models import Product


class SalesLedgerManager(models.Manager):
    def create_sales_ledger(self, contact, product, quantity):
        obj = self.model(contact=contact, product=product, quantity=quantity)
        obj.save()
        return obj

    def pks_with_email(self, email, object_pks):
        """Find the IDs of sales ledger records matching the email address.

        Keyword arguments:
        email -- find sales ledger records matching this email address
        object_pks -- only include sales ledger records from ``object_pks``

        """
        qs = get_contact_model().objects.matching_email(email)
        return self.model.objects.filter(
            pk__in=object_pks, contact__pk__in=set([x.pk for x in qs])
        ).values_list("pk", flat=True)


class SalesLedger(models.Model):
    """List of prices."""

    contact = models.ForeignKey(
        settings.CONTACT_MODEL, on_delete=models.CASCADE
    )
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    state = models.ForeignKey(
        CheckoutState, default=default_checkout_state, on_delete=models.CASCADE
    )
    objects = SalesLedgerManager()

    class Meta:
        ordering = ("pk",)
        verbose_name = "Sales ledger"
        verbose_name_plural = "Sales ledger"

    def __str__(self):
        return "{}".format(self.checkout_description)

    def get_absolute_url(self):
        """just for testing."""
        return reverse("project.settings")

    @property
    def checkout_can_charge(self):
        """We can always take a payment for this object!"""
        return True

    @property
    def checkout_email(self):
        return self.contact.email()

    @property
    def checkout_first_name(self):
        return "{}".format(self.contact.user.first_name.title())

    @property
    def checkout_name(self):
        return "{}".format(self.contact.user.get_full_name())

    @property
    def checkout_description(self):
        result = "{} x {} @ {}".format(
            self.product.name, self.quantity, self.product.price
        )
        return [result]

    def checkout_fail(self, checkout):
        """Update the object to record the payment failure.

        Called from within a transaction so you can update the model.

        """
        self.state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
        self.save()

    # def checkout_fail_url(self, checkout_pk):
    #    """just for testing."""
    #    return reverse("checkout.list.audit")

    def checkout_success(self, checkout):
        """Update the object to record the payment success.

        Called from within a transaction so you can update the model.

        """
        self.state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
        self.save()

    def checkout_success_url(self, checkout_pk):
        """just for testing."""
        return reverse(
            "example.sales.ledger.checkout.success", args=[checkout_pk]
        )

    @property
    def checkout_total(self):
        return self.product.price * self.quantity

    def instalment_fail(self, checkout, due):
        pass

    def instalment_success(self, checkout):
        pass


class ExampleContentObjectManager(models.Manager):
    def pks_with_email(self, email, object_pks):
        """Find the IDs of example records matching the email address.

        Keyword arguments:
        email -- find matching rows
        object_pks -- only include rows from ``object_pks``

        """
        return self.model.objects.filter(
            pk__in=object_pks, email=email
        ).values_list("pk", flat=True)


class ExampleContentObject(models.Model):
    """List of prices."""

    email = models.EmailField()
    description = models.TextField()
    first_name = models.CharField(max_length=30)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=8, decimal_places=2)
    state = models.ForeignKey(
        CheckoutState, default=default_checkout_state, on_delete=models.CASCADE
    )
    objects = ExampleContentObjectManager()

    class Meta:
        ordering = ("pk",)
        verbose_name = "Example Content Object"
        verbose_name_plural = "Example Content Objects"

    def __str__(self):
        return "{}".format(self.checkout_description)

    def get_absolute_url(self):
        """just for testing."""
        return reverse("project.settings")

    @property
    def checkout_can_charge(self):
        """We can always take a payment for this object!"""
        return True

    @property
    def checkout_email(self):
        return self.contact.email()

    @property
    def checkout_first_name(self):
        return "{}".format(self.first_name.title())

    @property
    def checkout_name(self):
        return "{}".format(self.first_name)

    @property
    def checkout_description(self):
        result = "{} x {} @ {}".format(
            self.description, self.quantity, self.price
        )
        return [result]

    def checkout_fail(self, checkout):
        """Update the object to record the payment failure.

        Called from within a transaction so you can update the model.

        """
        self.state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
        self.save()

    def checkout_success(self, checkout):
        """Update the object to record the payment success.

        Called from within a transaction so you can update the model.

        """
        self.state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
        self.save()

    @property
    def checkout_total(self):
        return self.price * self.quantity

    def instalment_fail(self, checkout, due):
        pass

    def instalment_success(self, checkout):
        pass
