# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, reverse_lazy, re_path
from django.views.generic import RedirectView

from .views import (
    CheckoutPayOnSessionView,
    ExampleCheckoutPayNowOrInvoice,
    ExampleContactDetailView,
    ExampleObjectPaymentPlanCardRefreshView,
    ExampleObjectPaymentPlanDepositChargeUpdateView,
    ExampleProcessPaymentsFormView,
    ExampleRefreshExpiryDatesFormView,
    ExampleReportContactOsFeesView,
    HomeView,
    ObjectPaymentPlanInstalmentSetupPayOnSessionRedirectView,
    SalesLedgerChargeUpdateView,
    SalesLedgerCheckoutPaymentIntentPopupView,
    SalesLedgerCheckoutPaymentIntentView,
    SalesLedgerCheckoutSetupIntentUpdateView,
    SalesLedgerCheckoutSuccessView,
    SalesLedgerPaymentIntentPopupRedirectView,
    SalesLedgerPaymentIntentRedirectView,
    SalesLedgerSetupIntentPopupRedirectView,
    SettingsView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^checkout/", view=include("checkout.urls")),
    path(
        "checkout/pay/on/session/<str:uuid>/",
        view=CheckoutPayOnSessionView.as_view(),
        name="web.checkout.pay.on.session",
    ),
    re_path(r"^contact/", view=include("contact.urls")),
    re_path(r"^gdpr/", view=include("gdpr.urls")),
    re_path(r"^mail/", view=include("mail.urls")),
    re_path(r"^stock/", view=include("stock.urls")),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(
            url=reverse_lazy("project.home"), permanent=False
        ),
        name="project.dash",
    ),
    path(
        "example/checkout/<str:uuid>/paynow/or/invoice/",
        view=ExampleCheckoutPayNowOrInvoice.as_view(),
        name="example.checkout.paynow.or.invoice",
    ),
    re_path(
        r"^example/contact/(?P<pk>\d+)/$",
        view=ExampleContactDetailView.as_view(),
        name="contact.detail",
    ),
    re_path(
        r"^example/object/payment/plan/card/refresh/$",
        view=ExampleObjectPaymentPlanCardRefreshView.as_view(),
        name="web.checkout.refresh.card",
    ),
    re_path(
        r"^example/object/payment/plan/deposit/charge/(?P<pk>\d+)/$",
        view=ExampleObjectPaymentPlanDepositChargeUpdateView.as_view(),
        name="example.object.payment.plan.deposit.charge",
    ),
    re_path(
        r"^example/process/payments/$",
        view=ExampleProcessPaymentsFormView.as_view(),
        name="example.process.payments",
    ),
    re_path(
        r"^example/refresh/card/expiry/dates/$",
        view=ExampleRefreshExpiryDatesFormView.as_view(),
        name="example.refresh.card.expiry.dates",
    ),
    re_path(
        r"^example/report/contact/osfees/$",
        view=ExampleReportContactOsFeesView.as_view(),
        name="report.contact.osfees",
    ),
    re_path(
        r"^example/object/payment/plan/instalment/(?P<pk>\d+)/pay/on/session/redirect/$",
        view=ObjectPaymentPlanInstalmentSetupPayOnSessionRedirectView.as_view(),
        name="example.payment.plan.instalment.setup.pay.on.session.redirect",
    ),
    re_path(
        r"^sales/ledger/(?P<pk>\d+)/charge/$",
        view=SalesLedgerChargeUpdateView.as_view(),
        name="example.sales.ledger.charge",
    ),
    path(
        "sales/ledger/checkout/payment/<str:uuid>/",
        view=SalesLedgerCheckoutPaymentIntentView.as_view(),
        name="example.sales.ledger.checkout.payment.intent",
    ),
    path(
        "sales/ledger/checkout/payment/<str:uuid>/popup/",
        view=SalesLedgerCheckoutPaymentIntentPopupView.as_view(),
        name="example.sales.ledger.checkout.payment.intent.popup",
    ),
    path(
        "sales/ledger/checkout/setup/intent/<str:uuid>/",
        view=SalesLedgerCheckoutSetupIntentUpdateView.as_view(),
        name="example.sales.ledger.checkout.setup.intent",
    ),
    path(
        "sales/ledger/checkout/<str:uuid>/success/",
        view=SalesLedgerCheckoutSuccessView.as_view(),
        name="example.sales.ledger.checkout.success",
    ),
    re_path(
        r"^sales/ledger/(?P<pk>\d+)/setup/intent/popup/redirect/$",
        view=SalesLedgerSetupIntentPopupRedirectView.as_view(),
        name="example.sales.ledger.setup.intent.popup.redirect",
    ),
    re_path(
        r"^sales/ledger/(?P<pk>\d+)/payment/intent/popup/redirect/$",
        view=SalesLedgerPaymentIntentPopupRedirectView.as_view(),
        name="example.sales.ledger.payment.intent.popup.redirect",
    ),
    re_path(
        r"^sales/ledger/(?P<pk>\d+)/payment/intent/redirect/$",
        view=SalesLedgerPaymentIntentRedirectView.as_view(),
        name="example.sales.ledger.payment.intent.redirect",
    ),
]

# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
