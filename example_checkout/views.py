# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.contrib import messages
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import (
    DetailView,
    FormView,
    ListView,
    UpdateView,
    RedirectView,
    TemplateView,
)

from base.view_utils import BaseMixin
from checkout.forms import ObjectPaymentPlanInstalmentEmptyForm
from checkout.models import (
    Checkout,
    CheckoutSettings,
    ObjectPaymentPlan,
    ObjectPaymentPlanInstalment,
)
from checkout.views import (
    CheckoutPaymentIntentMixin,
    CheckoutPayNowOrInvoiceMixin,
    CheckoutSetupIntentMixin,
    CheckoutSuccessMixin,
    ObjectPaymentPlanSetupIntentCardRefreshMixin,
)
from contact.views import ContactDetailMixin
from .forms import EmptyForm, SalesLedgerEmptyForm
from .models import SalesLedger


class SalesLedgerCheckoutMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                checkout_email=self.object.content_object.checkout_email,
                checkout_name=self.object.content_object.checkout_name,
                checkout_total=self.object.content_object.checkout_total,
            )
        )
        return context


class ExampleContactDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactDetailMixin,
    BaseMixin,
    DetailView,
):
    template_name = "example/contact_detail.html"


class ExampleCheckoutPayNowOrInvoice(
    CheckoutPayNowOrInvoiceMixin,
    SalesLedgerCheckoutMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "example/checkout_modal.html"

    def get_success_url(self):
        return reverse("project.home")


class ExampleObjectPaymentPlanCardRefreshView(
    LoginRequiredMixin,
    ObjectPaymentPlanSetupIntentCardRefreshMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "example/checkout_intent.html"

    def get_success_url(self):
        # use the example view for checkout success
        return reverse(
            "example.sales.ledger.checkout.success", args=[self.object.uuid]
        )


# class ExampleObjectPaymentPlanDepositListView(ListView):
#
#    model = ObjectPaymentPlan
#    paginate_by = 10
#    template_name = 'example/object_payment_plan_deposit_list.html'
#
#    def get_queryset(self):
#        return ObjectPaymentPlan.objects.outstanding_payment_plans


class ExampleObjectPaymentPlanDepositChargeUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, UpdateView
):
    form_class = ObjectPaymentPlanInstalmentEmptyForm
    model = ObjectPaymentPlanInstalment
    template_name = "example/object_payment_plan_deposit_charge.html"

    def form_valid(self, form):
        if self.object.object_payment_plan.charge_deposit(self.request.user):
            messages.success(
                self.request,
                "Charged Deposit at {}".format(
                    timezone.now().strftime("%H:%M:%S on the %d/%m/%Y")
                ),
            )
        else:
            self.object.refresh_from_db()
            messages.error(
                self.request,
                "Could not charge deposit ({}) ('{}')".format(
                    self.object.decline_message, self.object.decline_code
                ),
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("checkout.object.payment.plan.instalment.audit.list")


class ExampleProcessPaymentsFormView(FormView):
    form_class = EmptyForm
    template_name = "example/process_payments.html"

    def form_valid(self, form):
        ObjectPaymentPlanInstalment.objects.process_payments()
        messages.success(
            self.request,
            "Processed payments at {}".format(
                timezone.now().strftime("%H:%M:%S on the %d/%m/%Y")
            ),
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("checkout.object.payment.plan.instalment.audit.list")


class ExampleRefreshExpiryDatesFormView(FormView):
    form_class = EmptyForm
    template_name = "example/refresh_expiry_dates.html"

    def form_valid(self, form):
        ObjectPaymentPlan.objects.refresh_card_expiry_dates()
        messages.success(
            self.request,
            "Completed card refresh at {}".format(
                timezone.now().strftime("%H:%M:%S on the %d/%m/%Y")
            ),
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("example.refresh.card.expiry.dates")


class ExampleReportContactOsFeesView(BaseMixin, TemplateView):
    template_name = "example/settings.html"


class HomeView(ListView):
    model = SalesLedger
    template_name = "example/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        outstanding_payment_plans = (
            ObjectPaymentPlan.objects.outstanding_payment_plans
        )
        pay_on_session = ObjectPaymentPlanInstalment.objects.pay_on_session()
        context.update(
            dict(
                outstanding_payment_plans=outstanding_payment_plans,
                pay_on_session=pay_on_session,
            )
        )
        return context


class ObjectPaymentPlanInstalmentSetupPayOnSessionRedirectView(
    # LoginRequiredMixin,
    RedirectView
):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs["pk"]
        object_payment_plan_instalment = (
            ObjectPaymentPlanInstalment.objects.get(pk=pk)
        )
        with transaction.atomic():
            checkout = Checkout.objects.create_checkout_pay_on_session(
                object_payment_plan_instalment, self.request.user
            )
        return reverse(
            "web.object.payment.plan.instalment.checkout.pay.on.session",
            args=[checkout.uuid],
        )


class CheckoutPayOnSessionView(
    CheckoutPaymentIntentMixin, BaseMixin, UpdateView
):
    """Allow a customer to pay on-session.

    .. note:: The user must be logged in... [23/01/2020 PJK] Why?

    """

    template_name = "example/checkout_pay_on_session.html"


class SalesLedgerChargeUpdateView(LoginRequiredMixin, BaseMixin, UpdateView):
    model = SalesLedger
    form_class = SalesLedgerEmptyForm
    template_name = "example/charge.html"

    def form_valid(self, form):
        Checkout.objects.charge(self.object, self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("checkout.list.audit")


class SalesLedgerCheckoutPaymentIntentView(
    CheckoutPaymentIntentMixin, SalesLedgerCheckoutMixin, BaseMixin, UpdateView
):
    """Take a payment.

    .. note:: A payment can be taken by the logged in user or an anonymous user.

    """

    template_name = "example/checkout_intent.html"

    def get_success_url(self):
        """See ``CheckoutPaymentIntentMixin`` for detail..."""
        return self.object.content_object.checkout_success_url(self.object.pk)


class SalesLedgerCheckoutPaymentIntentPopupView(
    CheckoutPaymentIntentMixin, SalesLedgerCheckoutMixin, BaseMixin, UpdateView
):
    """Take a payment.

    .. note:: A payment can be taken by the logged in user or an anonymous user.

    """

    template_name = "example/checkout_modal.html"

    def get_success_url(self):
        """See ``CheckoutPaymentIntentMixin`` for detail..."""
        return self.object.content_object.checkout_success_url(self.object.pk)


class SalesLedgerCheckoutSetupIntentUpdateView(
    CheckoutSetupIntentMixin, SalesLedgerCheckoutMixin, BaseMixin, UpdateView
):
    """Collect card details for a payment plan.

    .. note:: The card details can be collected from logged in user or an
              anonymous user.

    """

    template_name = "example/checkout_intent.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(success_url=self.get_success_url()))
        return context

    def get_success_url(self):
        # use the example view for checkout success
        return reverse(
            "example.sales.ledger.checkout.success", args=[self.object.uuid]
        )


class SalesLedgerCheckoutSuccessView(
    CheckoutSuccessMixin, BaseMixin, DetailView
):
    template_name = "example/checkout_success.html"


class SalesLedgerPaymentIntentRedirectView(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        sales_ledger_pk = kwargs["pk"]
        sales_ledger = SalesLedger.objects.get(pk=sales_ledger_pk)
        with transaction.atomic():
            checkout = Checkout.objects.create_checkout_payment_intent(
                sales_ledger, self.request.user
            )
        return reverse(
            "example.sales.ledger.checkout.payment.intent", args=[checkout.uuid]
        )


class SalesLedgerPaymentIntentPopupRedirectView(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        sales_ledger_pk = kwargs["pk"]
        sales_ledger = SalesLedger.objects.get(pk=sales_ledger_pk)
        with transaction.atomic():
            checkout = Checkout.objects.create_checkout_payment_intent(
                sales_ledger, self.request.user
            )
        return reverse(
            "example.sales.ledger.checkout.payment.intent.popup",
            args=[checkout.uuid],
        )


class SalesLedgerSetupIntentPopupRedirectView(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        sales_ledger_pk = kwargs["pk"]
        sales_ledger = SalesLedger.objects.get(pk=sales_ledger_pk)
        checkout_settings = CheckoutSettings.objects.settings()
        with transaction.atomic():
            checkout = Checkout.objects.create_checkout_setup_intent(
                sales_ledger,
                self.request.user,
                checkout_settings.default_payment_plan,
            )
        return reverse(
            "example.sales.ledger.checkout.setup.intent",
            args=[checkout.uuid],
        )


class SettingsView(BaseMixin, TemplateView):
    template_name = "example/settings.html"
