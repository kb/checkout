# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from example_checkout.tasks import send_on_session_payment_emails


class Command(BaseCommand):
    help = "Checkout - email customer asking them to login and pay on-session"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = send_on_session_payment_emails()
        self.stdout.write(
            "{} - Complete - {} records".format(self.help, count or 0)
        )
