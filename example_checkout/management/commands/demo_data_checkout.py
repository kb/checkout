# -*- encoding: utf-8 -*-
from decimal import Decimal

from django.core.management.base import BaseCommand

from checkout.models import CheckoutSettings, ObjectPaymentPlan, PaymentPlan
from contact.models import Contact, ContactEmail
from example_checkout.models import SalesLedger
from finance.models import VatSettings
from login.tests.scenario import get_user_staff, get_user_web
from mail.models import Notify
from stock.models import Product, ProductCategory, ProductType


class Command(BaseCommand):
    help = "Create demo data for 'checkout'"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        vat_settings = VatSettings()
        vat_settings.save()
        Notify.objects.create_notify("test@pkimber.net")
        stock = ProductType.objects.init_product_type("stock", "Stock")
        stationery = ProductCategory.objects.init_product_category(
            "stationery", "Stationery", stock
        )
        pencil = Product.objects.init_product(
            "pencil", "Pencil", "", Decimal("1.32"), stationery
        )
        # payment plan
        try:
            payment_plan = PaymentPlan.objects.get(slug="default")
        except PaymentPlan.DoesNotExist:
            payment_plan = PaymentPlan.objects.create_payment_plan(
                "default", "KB Payment Plan", Decimal("50"), 2, 1
            )
        # payment plan - option 2
        try:
            payment_plan_option_2 = PaymentPlan.objects.get(
                slug="default-option-2"
            )
        except PaymentPlan.DoesNotExist:
            payment_plan_option_2 = PaymentPlan.objects.create_payment_plan(
                "default-option-2",
                "KB Payment Plan Option 2",
                Decimal("50"),
                4,
                1,
            )
        # contact, sales ledger (the 'content_object') and payment plans
        try:
            contact_1 = Contact.objects.get(user=get_user_web())
        except Contact.DoesNotExist:
            contact_1 = Contact.objects.create_contact(user=get_user_web())
            ContactEmail.objects.create_contact_email(
                contact_1, "web@pkimber.net"
            )
            sales_ledger_1 = SalesLedger.objects.create_sales_ledger(
                contact_1, pencil, 2
            )
            SalesLedger.objects.create_sales_ledger(contact_1, pencil, 1)
            ObjectPaymentPlan.objects.create_object_payment_plan(
                sales_ledger_1, payment_plan, Decimal("1000")
            )
        try:
            contact_2 = Contact.objects.get(user=get_user_staff())
        except Contact.DoesNotExist:
            contact_2 = Contact.objects.create_contact(user=get_user_staff())
            ContactEmail.objects.create_contact_email(
                contact_2, "staff@pkimber.net"
            )
            sales_ledger_2 = SalesLedger.objects.create_sales_ledger(
                contact_2, pencil, 6
            )
            ObjectPaymentPlan.objects.create_object_payment_plan(
                sales_ledger_2, payment_plan, Decimal("400")
            )
        # checkout settings
        checkout_settings = CheckoutSettings(
            default_payment_plan=payment_plan,
            default_payment_plan_option_2=payment_plan_option_2,
        )
        checkout_settings.save()
        self.stdout.write("{} - Complete".format(self.help))
