# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus


@pytest.mark.django_db
def test_project_home(client):
    response = client.get(reverse("project.home"))
    assert HTTPStatus.OK == response.status_code
