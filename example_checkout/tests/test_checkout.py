# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime
from decimal import Decimal
from django.contrib.auth.models import AnonymousUser
from django.core.management import call_command
from django.db import IntegrityError
from django.urls import reverse
from freezegun import freeze_time
from unittest import mock

from checkout.models import (
    Checkout,
    CheckoutAction,
    CheckoutError,
    CheckoutState,
    Customer,
)
from checkout.tests.factories import (
    CheckoutAdditionalFactory,
    CheckoutFactory,
    CustomerFactory,
    PaymentPlanFactory,
)
from checkout.tests.helper import MockCustomer, MockIntent
from contact.tests.factories import ContactEmailFactory, ContactFactory
from example_checkout.tests.factories import (
    ExampleContentObjectFactory,
    SalesLedgerFactory,
)
from login.tests.factories import UserFactory
from mail.models import Message
from mail.tests.factories import NotifyFactory
from stock.tests.factories import ProductFactory


# @pytest.mark.django_db
# def test_check_can_pay():
#     VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory()
#     checkout = sales_ledger.create_checkout()
#     try:
#         checkout.check_can_pay
#         pass
#     except CheckoutError:
#         assert False, 'payment is due - so can be paid'
#
#
# @pytest.mark.django_db
# def test_check_can_pay_not():
#     VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory()
#     payment = sales_ledger.create_payment()
#     payment.set_paid()
#     with pytest.raises(PayError):
#         payment.check_can_pay
#
#
# @pytest.mark.django_db
# def test_check_can_pay_too_early():
#     """This should never happen... but test anyway."""
#     VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory()
#     payment = sales_ledger.create_payment()
#     payment.created = timezone.now() + relativedelta(hours=+1, minutes=+2)
#     payment.save()
#     with pytest.raises(PayError):
#         payment.check_can_pay
#
#
# @pytest.mark.django_db
# def test_check_can_pay_too_late():
#     VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory()
#     payment = sales_ledger.create_payment()
#     payment.created = timezone.now() + relativedelta(hours=-1, minutes=-3)
#     payment.save()
#     with pytest.raises(PayError):
#         payment.check_can_pay
#
#
# @pytest.mark.django_db
# def test_mail_template_context():
#     VatSettingsFactory()
#     product = ProductFactory(name='Colour Pencils', price=Decimal('10.00'))
#     sales_ledger = SalesLedgerFactory(
#         email='test@pkimber.net',
#         title='Mr Patrick Kimber',
#         product=product,
#     )
#     payment = sales_ledger.create_payment()
#     assert {
#         'test@pkimber.net': dict(
#             description='Colour Pencils (£10.00 + £2.00 vat)',
#             name='Mr Patrick Kimber',
#             total='£12.00',
#         ),
#     } == payment.mail_template_context()


# @pytest.mark.django_db
# def test_make_payment():
#     #VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory()
#     sales_ledger.create_checkout(token='123')


# @pytest.mark.django_db
# def test_manager_payments_audit():
#     VatSettingsFactory()
#     PaymentLineFactory(payment=PaymentFactory(
#         name='p1',
#         state=PaymentState.objects.due(),
#         content_object=SalesLedgerFactory()
#     ))
#     PaymentLineFactory(payment=PaymentFactory(
#         name='p2',
#         state=PaymentState.objects.later(),
#         content_object=SalesLedgerFactory()
#     ))
#     PaymentLineFactory(payment=PaymentFactory(
#         name='p3',
#         state=PaymentState.objects.fail(),
#         content_object=SalesLedgerFactory()
#     ))
#     PaymentLineFactory(payment=PaymentFactory(
#         name='p4',
#         state=PaymentState.objects.paid(),
#         content_object=SalesLedgerFactory()
#     ))
#     assert ['p4', 'p3', 'p2', 'p1'] == [
#         p.name for p in Payment.objects.payments_audit()
#     ]
#
#
# @pytest.mark.django_db
# def test_manager_payments():
#     VatSettingsFactory()
#     PaymentLineFactory(payment=PaymentFactory(
#         name='p1',
#         state=PaymentState.objects.due(),
#         content_object=SalesLedgerFactory()
#     ))
#     PaymentLineFactory(payment=PaymentFactory(
#         name='p2',
#         state=PaymentState.objects.later(),
#         content_object=SalesLedgerFactory()
#     ))
#     PaymentLineFactory(payment=PaymentFactory(
#         name='p3',
#         state=PaymentState.objects.fail(),
#         content_object=SalesLedgerFactory()
#     ))
#     PaymentLineFactory(payment=PaymentFactory(
#         name='p4',
#         state=PaymentState.objects.paid(),
#         content_object=SalesLedgerFactory()
#     ))
#     assert ['p4', 'p3'] == [
#         p.name for p in Payment.objects.payments()
#     ]


@pytest.mark.django_db
def test_age_in_minutes():
    with freeze_time(datetime(2019, 8, 20, 6, 0, 0, tzinfo=pytz.utc)):
        checkout = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
        )
    with freeze_time(datetime(2019, 8, 20, 6, 50, 0, tzinfo=pytz.utc)):
        assert 50 == checkout.age_in_minutes()
    with freeze_time(datetime(2019, 8, 20, 7, 10, 0, tzinfo=pytz.utc)):
        assert 70 == checkout.age_in_minutes()


@pytest.mark.django_db
def test_attach_payment_method_to_customer_already_has_customer():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=SalesLedgerFactory(),
        customer=CustomerFactory(),
    )
    with pytest.raises(CheckoutError) as e:
        checkout.attach_payment_method_to_customer("abc")
    assert "Checkout {} already has a customer".format(checkout.pk) in str(
        e.value
    )


@pytest.mark.django_db
def test_attach_payment_method_to_customer_not_payment_plan():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=SalesLedgerFactory(),
        customer=None,
    )
    with pytest.raises(CheckoutError) as e:
        checkout.attach_payment_method_to_customer("abc")
    assert (
        "Payment methods can only be attached to a payment "
        "plan: {} ('payment')".format(checkout.pk)
    ) in str(e.value)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "checkout_state,expect",
    [(CheckoutState.SUCCESS, ["b"]), (CheckoutState.FAIL, [])],
)
def test_success_for_content_object(checkout_state, expect):
    """Has there been a successful payment for the content object?"""
    content_object = SalesLedgerFactory()
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
    CheckoutFactory(
        action=action,
        content_object=content_object,
        description="a",
        state=CheckoutState.objects.get(slug=CheckoutState.FAIL),
    )
    CheckoutFactory(
        action=action,
        description="b",
        content_object=content_object,
        state=CheckoutState.objects.get(slug=checkout_state),
    )
    assert expect == [
        x.description
        for x in Checkout.objects.success_for_content_object(content_object)
    ]


@pytest.mark.django_db
def test_client_secret_payment_intent():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=SalesLedgerFactory(),
    )
    with mock.patch("stripe.PaymentIntent.retrieve") as mock_payment_intent:
        mock_payment_intent.return_value = MockIntent(status="succeeded")
        result = checkout.client_secret_payment_intent()
    assert "shhhh" == result


@pytest.mark.django_db
def test_client_secret_payment_intent_not_payment():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
        content_object=SalesLedgerFactory(),
    )
    with pytest.raises(CheckoutError) as e:
        checkout.client_secret_payment_intent()
    assert (
        "Can only get secrets for payments (checkout {}, "
        "action 'payment_plan')".format(checkout.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_client_secret_setup_intent():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
        content_object=SalesLedgerFactory(),
    )
    with mock.patch("stripe.SetupIntent.retrieve") as mock_setup_intent:
        mock_setup_intent.return_value = MockIntent(status="succeeded")
        result = checkout.client_secret_setup_intent()
    assert "shhhh" == result


@pytest.mark.django_db
def test_client_secret_setup_intent_not_payment_plan():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=SalesLedgerFactory(),
    )
    with pytest.raises(CheckoutError) as e:
        checkout.client_secret_setup_intent()
    assert (
        "Can only get secrets for payment plans and card refresh "
        "(checkout {}, action 'payment')".format(checkout.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_content_object_url():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=SalesLedgerFactory(),
    )
    assert reverse("project.settings") == checkout.content_object_url()


@pytest.mark.django_db
def test_create_and_save_payment_intent_on_session():
    call_command("init_app_checkout")
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("12.34")),
    )
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION),
        content_object=content_object,
        customer=None,
        total=Decimal("100"),
    )
    customer = CustomerFactory(email="code@pkimber.net")
    with mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = {"id": "543"}
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        checkout.create_and_save_payment_intent_on_session()
    checkout.refresh_from_db()
    assert customer == checkout.customer
    assert "543" == checkout.payment_intent_id


@pytest.mark.django_db
def test_create_and_save_payment_intent_on_session_invalid_action():
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("12.34")),
    )
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=content_object,
    )
    CustomerFactory(email="code@pkimber.net")
    with mock.patch("stripe.PaymentMethod.list") as mock_payment_method:
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        with pytest.raises(CheckoutError) as e:
            checkout.create_and_save_payment_intent_on_session()
    assert (
        "Can only create an on-session payment intent for on-session "
        "payments. Checkout {}, action 'payment'".format(checkout.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_create_and_save_payment_intent_on_session_no_payment_methods():
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("12.34")),
    )
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=content_object,
    )
    customer = CustomerFactory(email="code@pkimber.net", customer_id="abcdef")
    with mock.patch("stripe.PaymentMethod.list"):
        with pytest.raises(CheckoutError) as e:
            checkout.create_and_save_payment_intent_on_session()
    assert (
        "No payment methods found for customer {}, 'abcdef'".format(customer.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_create_and_save_payment_intent_invalid_action():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
        content_object=SalesLedgerFactory(),
    )
    with pytest.raises(CheckoutError) as e:
        checkout.create_and_save_payment_intent()
    assert (
        "Can only create a payment intent for payments. "
        "Checkout {}, action 'payment_plan'".format(checkout.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_create_and_save_setup_intent_invalid_action():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=SalesLedgerFactory(),
    )
    with pytest.raises(CheckoutError) as e:
        checkout.create_and_save_setup_intent()
    assert (
        "Can only create a setup intent for payment plans (or card refresh). "
        "Checkout {}, action 'payment'".format(checkout.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_create_checkout_payment_intent_on_session():
    call_command("init_app_checkout")
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("12.34")),
    )
    CustomerFactory(email="code@pkimber.net")
    user = UserFactory()
    with mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method, mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent:
        mock_payment_intent.return_value = {"id": "543"}
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        checkout = Checkout.objects.create_checkout_payment_intent_on_session(
            content_object, user
        )
    assert "543" == checkout.payment_intent_id
    assert "" == checkout.setup_intent_id
    assert CheckoutAction.PAY_ON_SESSION == checkout.action.slug
    assert date.today() == checkout.checkout_date.date()
    assert content_object == checkout.content_object
    assert "Apple x 1 @ 12.34" == checkout.description
    assert Decimal("12.34") == checkout.total
    assert user == checkout.user


@pytest.mark.django_db
def test_create_checkout_payment_intent_on_session_user_not_authenticated():
    call_command("init_app_checkout")
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("12.34")),
    )
    CustomerFactory(email="code@pkimber.net")
    with mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method, mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent:
        mock_payment_intent.return_value = {"id": "543"}
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        checkout = Checkout.objects.create_checkout_payment_intent_on_session(
            content_object, AnonymousUser()
        )
    assert "543" == checkout.payment_intent_id
    assert "" == checkout.setup_intent_id
    assert CheckoutAction.PAY_ON_SESSION == checkout.action.slug
    assert date.today() == checkout.checkout_date.date()
    assert content_object == checkout.content_object
    assert "Apple x 1 @ 12.34" == checkout.description
    assert Decimal("12.34") == checkout.total
    assert checkout.user is None


@pytest.mark.django_db
def test_create_checkout_payment_intent():
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Orange", price=Decimal("34.56")),
    )
    user = UserFactory()
    with mock.patch("stripe.PaymentIntent.create") as mock_payment_intent:
        mock_payment_intent.return_value = {"id": "543"}
        checkout = Checkout.objects.create_checkout_payment_intent(
            content_object, user
        )
    assert "543" == checkout.payment_intent_id
    assert "" == checkout.setup_intent_id
    assert CheckoutAction.PAYMENT == checkout.action.slug
    assert date.today() == checkout.checkout_date.date()
    assert content_object == checkout.content_object
    assert "Orange x 1 @ 34.56" == checkout.description
    assert Decimal("34.56") == checkout.total
    assert user == checkout.user


@pytest.mark.django_db
def test_create_checkout_setup_intent():
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Pear", price=Decimal("12.34")),
    )
    user = UserFactory()
    with mock.patch("stripe.SetupIntent.create") as mock_setup_intent:
        mock_setup_intent.return_value = {"id": "654"}
        checkout = Checkout.objects.create_checkout_setup_intent(
            content_object, user, PaymentPlanFactory()
        )
    assert "" == checkout.payment_intent_id
    assert "654" == checkout.setup_intent_id
    assert CheckoutAction.PAYMENT_PLAN == checkout.action.slug
    assert date.today() == checkout.checkout_date.date()
    assert content_object == checkout.content_object
    assert "Pear x 1 @ 12.34" == checkout.description
    assert Decimal("12.34") == checkout.total
    assert user == checkout.user


@pytest.mark.django_db
def test_date_of_birth():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
        content_object=SalesLedgerFactory(),
    )
    CheckoutAdditionalFactory(
        checkout=checkout, date_of_birth=date(2019, 8, 22)
    )
    assert date(2019, 8, 22) == checkout.date_of_birth()


@pytest.mark.django_db
def test_date_of_birth_no_additional_data():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
        content_object=SalesLedgerFactory(),
    )
    assert checkout.date_of_birth() is None


@pytest.mark.django_db
@pytest.mark.parametrize(
    "decline_filter",
    [True, False, None],
)
def test_decline_message(decline_filter):
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN)
    content_object = SalesLedgerFactory()
    with freeze_time(datetime(2019, 8, 20, 6, 0, 0, tzinfo=pytz.utc)):
        CheckoutFactory(
            action=action,
            content_object=content_object,
            decline_code="insufficient_funds",
            decline_message="The card has insufficient funds to complete...",
        )
    assert (
        "('insufficient_funds' at 20/08/2019 07:00) "
        "The card has insufficient funds to complete..."
    ) == Checkout.objects.decline_message(
        content_object, decline_filter=decline_filter
    )


@pytest.mark.django_db
def test_for_email():
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    checkout_1 = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=SalesLedgerFactory(contact=contact),
        customer=None,
    )
    CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=ExampleContentObjectFactory(email="patrick@pkimber.net"),
        customer=None,
    )
    checkout_3 = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
        content_object=ExampleContentObjectFactory(email="code@pkimber.net"),
        customer=None,
    )
    assert [checkout_3.pk, checkout_1.pk] == [
        x.pk for x in Checkout.objects.for_email("code@pkimber.net")
    ]


@pytest.mark.django_db
def test_invoice_data_no_additional_data():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
        content_object=SalesLedgerFactory(),
    )
    assert [] == checkout.invoice_data()


@pytest.mark.django_db
def test_no_content_object():
    """Payments must be linked to a content object."""
    with pytest.raises(IntegrityError):
        CheckoutFactory()


@pytest.mark.django_db
def test_notify_invoice():
    NotifyFactory(email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("12.34")),
    )
    with freeze_time(datetime(2019, 8, 20, 6, 1, 0, tzinfo=pytz.utc)):
        checkout = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.INVOICE),
            state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
            content_object=content_object,
        )
    CheckoutAdditionalFactory(
        checkout=checkout,
        company_name="KB",
        address_1="2 High Street",
        email="test@pkimber.net",
        date_of_birth=date(1972, 3, 22),
    )
    checkout.notify()
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert "PENDING - Invoice from Patrick" == message.subject
    assert (
        "20/08/2019 06:01 - PENDING - Invoice from Patrick, "
        "code@pkimber.net:\n\n\n\n"
        "\n\nInvoice: KB, 2 High Street, test@pkimber.net"
        "\n\nDate of birth: 22/03/1972"
    ) == message.description


@pytest.mark.django_db
def test_notify_payment_plan():
    NotifyFactory(email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("12.34")),
    )
    with freeze_time(datetime(2019, 8, 20, 6, 1, 0, tzinfo=pytz.utc)):
        checkout = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
            state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
            content_object=content_object,
        )
    CheckoutAdditionalFactory(
        checkout=checkout,
        address_1="2 High Street",
        town="Hatherleigh",
        country="UK",
    )
    checkout.notify()
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert "PENDING - Payment plan from Patrick" == message.subject
    assert (
        "20/08/2019 06:01 - PENDING - Payment Plan from Patrick, "
        "code@pkimber.net:\n\n\n\n"
        "\n\nPrevious address: 2 High Street, Hatherleigh, UK"
    ) == message.description


@pytest.mark.django_db
def test_payment_cancel():
    call_command("init_app_checkout")
    cancel = CheckoutState.objects.get(slug=CheckoutState.CANCEL)
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    # setup
    content_object = SalesLedgerFactory()
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION),
        content_object=content_object,
    )
    assert pending == checkout.state
    assert pending == content_object.state
    # test
    checkout.payment_cancel()
    # check
    checkout.refresh_from_db()
    assert cancel == checkout.state
    content_object.refresh_from_db()
    assert pending == content_object.state


@pytest.mark.django_db
def test_payment_fail():
    call_command("init_app_checkout")
    fail = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    # setup
    content_object = SalesLedgerFactory()
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION),
        content_object=content_object,
    )
    assert pending == checkout.state
    assert pending == content_object.state
    # test
    checkout.payment_fail()
    # check
    checkout.refresh_from_db()
    assert fail == checkout.state
    content_object.refresh_from_db()
    assert fail == content_object.state


@pytest.mark.django_db
def test_payment_intent_fulfillment():
    """Test fulfilment for payments.

    .. note:: The user has 60 minutes (see ``age_in_minutes``) to complete the
              Stripe transaction.

    """
    call_command("init_app_checkout")
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    with freeze_time(datetime(2019, 5, 21, 13, 23, 0, tzinfo=pytz.utc)):
        checkout_payment_old = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
    with freeze_time(datetime(2019, 5, 21, 14, 30, 0, tzinfo=pytz.utc)):
        checkout_pay_on_session = CheckoutFactory(
            action=CheckoutAction.objects.get(
                slug=CheckoutAction.PAY_ON_SESSION
            ),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        checkout_payment = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        checkout_payment_plan = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        assert 0 == Customer.objects.count()
        with mock.patch("stripe.PaymentIntent.retrieve") as mock_payment_intent:
            mock_payment_intent.return_value = {"status": "succeeded"}
            count = Checkout.objects.payment_intent_fulfillment()
    assert 2 == count
    checkout_pay_on_session.refresh_from_db()
    checkout_payment.refresh_from_db()
    checkout_payment_old.refresh_from_db()
    checkout_payment_plan.refresh_from_db()
    assert CheckoutState.PENDING == checkout_payment_plan.state.slug
    assert CheckoutState.PENDING == checkout_pay_on_session.state.slug
    assert CheckoutState.SUCCESS == checkout_payment.state.slug
    assert CheckoutState.SUCCESS == checkout_payment_old.state.slug
    # do not 'attach_payment_method_to_customer'
    assert 0 == Customer.objects.count()


@pytest.mark.django_db
def test_payment_intent_fulfillment_fail_old():
    """Test fulfilment for old payments.

    .. note:: The user has 1440 minutes or 24 hours (see ``age_in_minutes``)
              to complete the Stripe transaction.

    """
    call_command("init_app_checkout")
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    with freeze_time(datetime(2019, 5, 21, 12, 23, 0, tzinfo=pytz.utc)):
        checkout_payment_old = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
            state=pending,
        )
    with freeze_time(datetime(2019, 6, 21, 14, 30, 0, tzinfo=pytz.utc)):
        checkout_payment = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
            state=pending,
        )
        with mock.patch("stripe.PaymentIntent.retrieve") as mock_payment_intent:
            mock_payment_intent.return_value = {"status": "purple"}
            count = Checkout.objects.payment_intent_fulfillment()
    assert 2 == count
    checkout_payment.refresh_from_db()
    checkout_payment_old.refresh_from_db()
    assert CheckoutState.CANCEL == checkout_payment_old.state.slug
    assert CheckoutState.PENDING == checkout_payment.state.slug


@pytest.mark.django_db
def test_payment_intent_on_session_fulfillment():
    """Test fulfilment for setup intents (payment plan)."""
    call_command("init_app_checkout")
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    with freeze_time(datetime(2019, 5, 21, 13, 23, 0, tzinfo=pytz.utc)):
        checkout_payment_plan_old = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
    with freeze_time(datetime(2019, 5, 21, 14, 30, 0, tzinfo=pytz.utc)):
        contact = ContactFactory()
        ContactEmailFactory(contact=contact, email="code@pkimber.net")
        checkout_pay_on_session = CheckoutFactory(
            action=CheckoutAction.objects.get(
                slug=CheckoutAction.PAY_ON_SESSION
            ),
            content_object=SalesLedgerFactory(contact=contact),
            customer=None,
            state=pending,
        )
        checkout_payment = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        checkout_payment_plan = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        assert 0 == Customer.objects.count()
        with mock.patch(
            "stripe.Customer.create"
        ) as mock_customer_create, mock.patch(
            "stripe.PaymentIntent.retrieve"
        ) as mock_payment_intent, mock.patch(
            "stripe.PaymentMethod.attach"
        ):
            mock_customer_create.return_value = MockCustomer()
            mock_payment_intent.return_value = MockIntent(status="succeeded")
            count = Checkout.objects.payment_intent_on_session_fulfillment()
    assert 1 == count
    checkout_pay_on_session.refresh_from_db()
    checkout_payment.refresh_from_db()
    checkout_payment_plan.refresh_from_db()
    checkout_payment_plan_old.refresh_from_db()
    assert CheckoutState.SUCCESS == checkout_pay_on_session.state.slug
    assert CheckoutState.PENDING == checkout_payment.state.slug
    assert CheckoutState.PENDING == checkout_payment_plan.state.slug
    assert CheckoutState.PENDING == checkout_payment_plan_old.state.slug
    # 'attach_payment_method_to_customer'
    # assert ["code@pkimber.net"] == [x.email for x in Customer.objects.all()]


@pytest.mark.django_db
def test_payment_intent_on_session_fulfillment_canceled():
    """Test fulfilment for 'canceled' setup intents (payment plan)."""
    call_command("init_app_checkout")
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    checkout_payment_plan = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION),
        content_object=SalesLedgerFactory(),
        customer=None,
        state=pending,
    )
    with mock.patch("stripe.PaymentIntent.retrieve") as mock_payment_intent:
        mock_payment_intent.return_value = {"status": "canceled"}
        # with mock.patch("stripe.SetupIntent.retrieve") as mock_setup_intent:
        # with mock.patch("stripe.Customer.create") as mock_customer_create:
        # with mock.patch("stripe.PaymentMethod.attach"):
        # mock_customer_create.return_value = MockCustomer()
        # mock_setup_intent.return_value = MockIntent(status="canceled")
        count = Checkout.objects.payment_intent_on_session_fulfillment()
    assert 1 == count
    checkout_payment_plan.refresh_from_db()
    assert CheckoutState.CANCEL == checkout_payment_plan.state.slug


@pytest.mark.django_db
def test_payment_intent_on_session_fulfillment_pending():
    """Test fulfilment for old setup intents (payment plan).

    .. note:: There is no longer a time limit on setup intents.

    """
    call_command("init_app_checkout")
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    with freeze_time(datetime(2019, 5, 21, 13, 23, 0, tzinfo=pytz.utc)):
        checkout_payment_plan_old = CheckoutFactory(
            action=CheckoutAction.objects.get(
                slug=CheckoutAction.PAY_ON_SESSION
            ),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
    with freeze_time(datetime(2019, 5, 21, 14, 30, 0, tzinfo=pytz.utc)):
        checkout_payment_plan = CheckoutFactory(
            action=CheckoutAction.objects.get(
                slug=CheckoutAction.PAY_ON_SESSION
            ),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        with mock.patch("stripe.PaymentIntent.retrieve") as mock_payment_intent:
            mock_payment_intent.return_value = {"status": "purple"}
            # with mock.patch("stripe.SetupIntent.retrieve") as mock_setup_intent:
            # with mock.patch("stripe.Customer.create") as mock_customer_create:
            # with mock.patch("stripe.PaymentMethod.attach"):
            # mock_customer_create.return_value = MockCustomer()
            # mock_setup_intent.return_value = MockIntent(status="purple")
            count = Checkout.objects.payment_intent_on_session_fulfillment()
    assert 2 == count
    checkout_payment_plan.refresh_from_db()
    checkout_payment_plan_old.refresh_from_db()
    assert CheckoutState.PENDING == checkout_payment_plan_old.state.slug
    assert CheckoutState.PENDING == checkout_payment_plan.state.slug


@pytest.mark.django_db
def test_payment_intent_on_session_fulfillment_success():
    """Test on-session payment intent where content object already paid.

    For example, the instalment for a payment plan may have been paid by another
    checkout - or just marked as paid.

    """
    call_command("init_app_checkout")
    content_object = SalesLedgerFactory()
    # action
    action_payment_plan = CheckoutAction.objects.get(
        slug=CheckoutAction.PAY_ON_SESSION
    )
    # state
    state_pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    state_success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    CheckoutFactory(
        action=action_payment_plan,
        content_object=content_object,
        state=state_success,
    )
    checkout_payment_plan = CheckoutFactory(
        action=action_payment_plan,
        content_object=content_object,
        state=state_pending,
    )
    with mock.patch("stripe.PaymentIntent.retrieve") as mock_payment_intent:
        mock_payment_intent.return_value = {"status": "purple"}
        # with mock.patch("stripe.SetupIntent.retrieve") as mock_setup_intent:
        # with mock.patch("stripe.Customer.create") as mock_customer_create:
        # with mock.patch("stripe.PaymentMethod.attach"):
        # mock_customer_create.return_value = MockCustomer()
        # mock_setup_intent.return_value = MockIntent(status="purple")
        count = Checkout.objects.payment_intent_on_session_fulfillment()
    assert 1 == count
    checkout_payment_plan.refresh_from_db()
    assert CheckoutState.CANCEL == checkout_payment_plan.state.slug


# @pytest.mark.django_db
# def test_is_payment_plan():
#    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN)
#    checkout = CheckoutFactory(
#        action=action, content_object=SalesLedgerFactory()
#    )
#    assert bool(checkout.is_payment_plan) is True


# @pytest.mark.django_db
# def test_is_payment_plan_not():
#    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
#    checkout = CheckoutFactory(
#        action=action, content_object=SalesLedgerFactory()
#    )
#    assert bool(checkout.is_payment_plan) is False


@pytest.mark.django_db
def test_pending():
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
    failed = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    CheckoutFactory(
        action=action,
        content_object=SalesLedgerFactory(),
        description="a",
        state=failed,
    )
    CheckoutFactory(
        action=action,
        content_object=SalesLedgerFactory(),
        description="b",
        state=pending,
    )
    CheckoutFactory(
        action=action,
        content_object=SalesLedgerFactory(),
        description="c",
        state=success,
    )
    CheckoutFactory(
        action=action,
        content_object=SalesLedgerFactory(),
        description="d",
        state=pending,
    )
    assert set(["d", "b"]) == set(
        [x.description for x in Checkout.objects.pending()]
    )


@pytest.mark.django_db
def test_pending_action():
    call_command("init_app_checkout")
    action_payment = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
    action_pay_on_session = CheckoutAction.objects.get(
        slug=CheckoutAction.PAY_ON_SESSION
    )
    failed = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    CheckoutFactory(
        action=action_pay_on_session,
        content_object=SalesLedgerFactory(),
        description="a",
        state=failed,
    )
    CheckoutFactory(
        action=action_payment,
        content_object=SalesLedgerFactory(),
        description="b",
        state=pending,
    )
    CheckoutFactory(
        action=action_pay_on_session,
        content_object=SalesLedgerFactory(),
        description="c",
        state=success,
    )
    CheckoutFactory(
        action=action_pay_on_session,
        content_object=SalesLedgerFactory(),
        description="d",
        state=pending,
    )
    assert set(["d"]) == set(
        [
            x.description
            for x in Checkout.objects.pending(CheckoutAction.PAY_ON_SESSION)
        ]
    )


@pytest.mark.django_db
def test_pending_action_list():
    call_command("init_app_checkout")
    action_payment = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
    action_pay_on_session = CheckoutAction.objects.get(
        slug=CheckoutAction.PAY_ON_SESSION
    )
    failed = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    CheckoutFactory(
        action=action_payment,
        content_object=SalesLedgerFactory(),
        description="a",
        state=failed,
    )
    CheckoutFactory(
        action=action_payment,
        content_object=SalesLedgerFactory(),
        description="b",
        state=pending,
    )
    CheckoutFactory(
        action=action_payment,
        content_object=SalesLedgerFactory(),
        description="c",
        state=success,
    )
    CheckoutFactory(
        action=action_pay_on_session,
        content_object=SalesLedgerFactory(),
        description="d",
        state=pending,
    )
    assert set(["d", "b"]) == set(
        [
            x.description
            for x in Checkout.objects.pending(
                [CheckoutAction.PAYMENT, CheckoutAction.PAY_ON_SESSION]
            )
        ]
    )


# @pytest.mark.django_db
# def test_notification_message():
#     VatSettingsFactory()
#     payment = PaymentFactory(content_object=SalesLedgerFactory())
#     product = ProductFactory(name='Paintbrush')
#     PaymentLineFactory(payment=payment, product=product)
#     payment.set_paid()
#     factory = RequestFactory()
#     request = factory.get(reverse('project.home'))
#     subject, message = payment.mail_subject_and_message(request)
#     assert 'payment received from Mr' in message
#     assert 'Paintbrush' in message
#     assert 'http://testserver/' in message


@pytest.mark.django_db
def test_setup_intent_fulfillment():
    """Test fulfilment for setup intents (payment plan).

    .. note:: The user has 60 minutes (see ``age_in_minutes``) to complete the
              Stripe transaction.

    """
    call_command("init_app_checkout")
    CheckoutAction(slug=CheckoutAction.PAYMENT_PLAN_OPTION_2).save()
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    with freeze_time(datetime(2019, 5, 21, 13, 23, 0, tzinfo=pytz.utc)):
        # 1
        contact_1 = ContactFactory()
        ContactEmailFactory(contact=contact_1, email="one@pkimber.net")
        checkout_payment_plan_old = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
            content_object=SalesLedgerFactory(contact=contact_1),
            customer=None,
            state=pending,
        )
    with freeze_time(datetime(2019, 5, 21, 14, 30, 0, tzinfo=pytz.utc)):
        # 2
        checkout_pay_on_session_2 = CheckoutFactory(
            action=CheckoutAction.objects.get(
                slug=CheckoutAction.PAY_ON_SESSION
            ),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        checkout_payment_2 = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        contact_2 = ContactFactory()
        ContactEmailFactory(contact=contact_2, email="two@pkimber.net")
        checkout_payment_plan_2 = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
            content_object=SalesLedgerFactory(contact=contact_2),
            customer=None,
            state=pending,
        )
        # 3
        checkout_pay_on_session_3 = CheckoutFactory(
            action=CheckoutAction.objects.get(
                slug=CheckoutAction.PAY_ON_SESSION
            ),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        checkout_payment_3 = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
            customer=None,
            state=pending,
        )
        contact_3 = ContactFactory()
        ContactEmailFactory(contact=contact_3, email="three@pkimber.net")
        checkout_payment_plan_3 = CheckoutFactory(
            action=CheckoutAction.objects.get(
                slug=CheckoutAction.PAYMENT_PLAN_OPTION_2
            ),
            content_object=SalesLedgerFactory(contact=contact_3),
            customer=None,
            state=pending,
        )
        # test
        assert 0 == Customer.objects.count()
        with mock.patch(
            "stripe.SetupIntent.retrieve"
        ) as mock_setup_intent, mock.patch(
            "stripe.Customer.create"
        ) as mock_customer_create, mock.patch(
            "stripe.Customer.modify"
        ), mock.patch(
            "stripe.PaymentMethod.attach"
        ):
            mock_customer_create.return_value = MockCustomer()
            mock_setup_intent.return_value = MockIntent(status="succeeded")
            count = Checkout.objects.setup_intent_fulfillment()
    assert 3 == count
    # 1
    checkout_payment_plan_old.refresh_from_db()
    # 2
    checkout_pay_on_session_2.refresh_from_db()
    checkout_payment_2.refresh_from_db()
    checkout_payment_plan_2.refresh_from_db()
    # 3
    checkout_pay_on_session_3.refresh_from_db()
    checkout_payment_3.refresh_from_db()
    checkout_payment_plan_3.refresh_from_db()
    # 1
    assert CheckoutState.SUCCESS == checkout_payment_plan_old.state.slug
    # 2
    assert CheckoutState.PENDING == checkout_pay_on_session_2.state.slug
    assert CheckoutState.PENDING == checkout_payment_2.state.slug
    assert CheckoutState.SUCCESS == checkout_payment_plan_2.state.slug
    # 3
    assert CheckoutState.PENDING == checkout_pay_on_session_3.state.slug
    assert CheckoutState.PENDING == checkout_payment_3.state.slug
    assert CheckoutState.SUCCESS == checkout_payment_plan_3.state.slug
    # 'attach_payment_method_to_customer'
    assert set(
        ["one@pkimber.net", "two@pkimber.net", "three@pkimber.net"]
    ) == set([x.email for x in Customer.objects.all()])


@pytest.mark.django_db
def test_setup_intent_fulfillment_fail_old():
    """Test fulfilment for old payments.

    .. note:: The user has 1440 minutes or 24 hours (see ``age_in_minutes``)
              to complete the Stripe transaction.

    """
    call_command("init_app_checkout")
    pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    with freeze_time(datetime(2019, 5, 21, 12, 23, 0, tzinfo=pytz.utc)):
        checkout_payment_old = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
            content_object=SalesLedgerFactory(),
            state=pending,
        )
    with freeze_time(datetime(2019, 6, 21, 14, 30, 0, tzinfo=pytz.utc)):
        checkout_payment = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
            content_object=SalesLedgerFactory(),
            state=pending,
        )
        with mock.patch(
            "stripe.SetupIntent.retrieve"
        ) as mock_setup_intent, mock.patch(
            "stripe.Customer.create"
        ) as mock_customer_create, mock.patch(
            "stripe.PaymentMethod.attach"
        ):
            mock_customer_create.return_value = MockCustomer()
            mock_setup_intent.return_value = MockIntent(status="purple")
            count = Checkout.objects.setup_intent_fulfillment()
    assert 2 == count
    checkout_payment.refresh_from_db()
    checkout_payment_old.refresh_from_db()
    assert CheckoutState.CANCEL == checkout_payment_old.state.slug
    assert CheckoutState.PENDING == checkout_payment.state.slug


# @pytest.mark.django_db
# def test_setup_intent_fulfillment_canceled():
#     """Test fulfilment for 'canceled' setup intents (payment plan)."""
#     call_command("init_app_checkout")
#     pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
#     checkout_payment_plan = CheckoutFactory(
#         action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
#         content_object=SalesLedgerFactory(),
#         customer=None,
#         state=pending,
#     )
#     with mock.patch("stripe.SetupIntent.retrieve") as mock_setup_intent:
#         with mock.patch("stripe.Customer.create") as mock_customer_create:
#             with mock.patch("stripe.PaymentMethod.attach"):
#                 mock_customer_create.return_value = MockCustomer()
#                 mock_setup_intent.return_value = MockIntent(status="canceled")
#                 count = Checkout.objects.setup_intent_fulfillment()
#     assert 1 == count
#     checkout_payment_plan.refresh_from_db()
#     assert CheckoutState.FAIL == checkout_payment_plan.state.slug


# @pytest.mark.django_db
# def test_setup_intent_fulfillment_pending():
#     """Test fulfilment for old setup intents (payment plan).
#
#     .. note:: There is no longer a time limit on setup intents.
#
#     """
#     call_command("init_app_checkout")
#     pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
#     with freeze_time(datetime(2019, 5, 21, 13, 23, 0, tzinfo=pytz.utc)):
#         checkout_payment_plan_old = CheckoutFactory(
#             action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
#             content_object=SalesLedgerFactory(),
#             customer=None,
#             state=pending,
#         )
#     with freeze_time(datetime(2019, 5, 21, 14, 30, 0, tzinfo=pytz.utc)):
#         checkout_payment_plan = CheckoutFactory(
#             action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
#             content_object=SalesLedgerFactory(),
#             customer=None,
#             state=pending,
#         )
#         with mock.patch("stripe.SetupIntent.retrieve") as mock_setup_intent:
#             with mock.patch("stripe.Customer.create") as mock_customer_create:
#                 with mock.patch("stripe.PaymentMethod.attach"):
#                     mock_customer_create.return_value = MockCustomer()
#                     mock_setup_intent.return_value = MockIntent(status="purple")
#                     count = Checkout.objects.setup_intent_fulfillment()
#     assert 2 == count
#     checkout_payment_plan.refresh_from_db()
#     checkout_payment_plan_old.refresh_from_db()
#     assert CheckoutState.PENDING == checkout_payment_plan_old.state.slug
#     assert CheckoutState.PENDING == checkout_payment_plan.state.slug
#
#
# @pytest.mark.django_db
# def test_setup_intent_fulfillment_success():
#     """Test fulfilment for setup intent where paid elsewhere (payment plan)."""
#     call_command("init_app_checkout")
#     content_object = SalesLedgerFactory()
#     # action
#     action_payment_plan = CheckoutAction.objects.get(
#         slug=CheckoutAction.PAYMENT_PLAN
#     )
#     # state
#     state_pending = CheckoutState.objects.get(slug=CheckoutState.PENDING)
#     state_success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
#     CheckoutFactory(
#         action=action_payment_plan,
#         content_object=content_object,
#         state=state_success,
#     )
#     checkout_payment_plan = CheckoutFactory(
#         action=action_payment_plan,
#         content_object=content_object,
#         state=state_pending,
#     )
#     with mock.patch("stripe.SetupIntent.retrieve") as mock_setup_intent:
#         with mock.patch("stripe.Customer.create") as mock_customer_create:
#             with mock.patch("stripe.PaymentMethod.attach"):
#                 mock_customer_create.return_value = MockCustomer()
#                 mock_setup_intent.return_value = MockIntent(status="purple")
#                 count = Checkout.objects.setup_intent_fulfillment()
#     assert 1 == count
#     checkout_payment_plan.refresh_from_db()
#     assert CheckoutState.FAIL == checkout_payment_plan.state.slug


# @pytest.mark.django_db
# def test_set_paid():
#     VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory(title='Carol')
#     assert not sales_ledger.is_paid
#     payment = sales_ledger.create_payment()
#     assert not payment.is_paid
#     payment.set_paid()
#     # refresh
#     payment = Payment.objects.get(pk=payment.pk)
#     assert payment.is_paid
#     # refresh
#     sales_ledger = SalesLedger.objects.get(title='Carol')
#     assert sales_ledger.is_paid
#
#
# @pytest.mark.django_db
# def test_set_payment_failed():
#     VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory(title='Carol')
#     assert not sales_ledger.is_paid
#     payment = sales_ledger.create_payment()
#     assert not payment.is_paid
#     payment.set_payment_failed()
#     # refresh
#     payment = Payment.objects.get(pk=payment.pk)
#     assert not payment.is_paid
#     sales_ledger = SalesLedger.objects.get(title='Carol')
#     assert not sales_ledger.is_paid
#     assert PaymentState.FAIL == payment.state.slug


@pytest.mark.django_db
def test_str():
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    assert "code@pkimber.net" == str(
        CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(contact=contact),
        )
    )


# @pytest.mark.django_db
# def test_total():
#     VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory(
#         product=ProductFactory(price=Decimal('2.50')),
#         quantity=Decimal('2'),
#     )
#     payment = sales_ledger.create_payment()
#     assert Decimal('6.00') == payment.total
#
#
# @pytest.mark.django_db
# def test_unique_together():
#     VatSettingsFactory()
#     sales_ledger = SalesLedgerFactory()
#     sales_ledger.create_payment()
#     with pytest.raises(IntegrityError):
#         sales_ledger.create_payment()
