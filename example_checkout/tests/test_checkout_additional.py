# -*- encoding: utf-8 -*-
import pytest

from checkout.models import CheckoutAction
from checkout.tests.factories import CheckoutAdditionalFactory, CheckoutFactory

from example_checkout.tests.factories import SalesLedgerFactory


@pytest.mark.django_db
def test_str():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.INVOICE),
        content_object=SalesLedgerFactory(),
    )
    checkout_additional = CheckoutAdditionalFactory(
        checkout=checkout, email="code@pkimber.net"
    )
    assert "code@pkimber.net" == str(checkout_additional)
