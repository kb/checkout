# -*- encoding: utf-8 -*-
import factory

from decimal import Decimal

from contact.tests.factories import ContactFactory
from example_checkout.models import ExampleContentObject, SalesLedger
from stock.tests.factories import ProductFactory


class ExampleContentObjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ExampleContentObject

    quantity = 1
    price = Decimal("15")

    @factory.sequence
    def description(n):
        return "{:02d}_description".format(n)

    @factory.sequence
    def email(n):
        return "{}@pkimber.net".format(n)

    @factory.sequence
    def first_name(n):
        return "{:02d}_first_name".format(n)


class SalesLedgerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SalesLedger

    contact = factory.SubFactory(ContactFactory)
    product = factory.SubFactory(ProductFactory)
    quantity = 1
