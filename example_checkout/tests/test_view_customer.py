# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from base.url_utils import url_with_querystring
from checkout.models import CheckoutAction, CheckoutState, Customer
from checkout.tests.factories import (
    CheckoutFactory,
    CustomerFactory,
    ObjectPaymentPlanFactory,
    ObjectPaymentPlanInstalmentFactory,
)
from contact.tests.factories import ContactEmailFactory, ContactFactory
from example_checkout.tests.factories import SalesLedgerFactory
from login.tests.fixture import TEST_PASSWORD, UserFactory
from mail.models import Message
from mail.tests.factories import MailTemplateFactory


@pytest.mark.django_db
def test_customer_detail_view(client):
    obj = CustomerFactory()
    url = url_with_querystring(reverse("checkout.customer"), email=obj.email)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_customer_detail_view_invalid_email(client):
    url = url_with_querystring(
        reverse("checkout.customer"), email="does.not.exist@test.com"
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # with pytest.raises(Customer.DoesNotExist) as e:
    #    client.get(url)
    # assert 'Cannot find a customer record' in str(e.value)
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_customer_detail_view_object_payment_plan(client):
    customer = CustomerFactory(email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory())
    ContactEmailFactory(contact=contact, email=customer.email)
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory(contact=contact)
    )
    CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.CARD_REFRESH),
        content_object=object_payment_plan,
        customer=customer,
        state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
    )
    url = url_with_querystring(
        reverse("checkout.customer"), email=customer.email
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_customer_card_refresh_request(client):
    MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_CARD_REFRESH_REQUEST)
    customer = CustomerFactory(name="patrick kimber")
    url = reverse("checkout.customer.card.refresh.request", args=[customer.pk])
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("checkout.customer") in response.url
    message = Message.objects.first()
    assert message is not None
    mail = message.mail_set.first()
    assert mail is not None
    assert 2 == mail.mailfield_set.count()
    data = {f.key: f.value for f in mail.mailfield_set.all()}
    assert "Patrick Kimber" == data["name"]
    assert reverse("web.checkout.refresh.card") in data["url"]
