# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from decimal import Decimal

from checkout.models import CheckoutState, PaymentRun, PaymentRunItem
from checkout.tests.factories import (
    ObjectPaymentPlanFactory,
    ObjectPaymentPlanInstalmentFactory,
)
from example_checkout.tests.factories import SalesLedgerFactory


def _instalment():
    """Create an instalment."""
    today = date.today()
    return ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-1),
        amount=Decimal("2"),
        count=2,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
    )


@pytest.mark.django_db
def test_payment_run_item_manager():
    payment_run = PaymentRun.objects.create_payment_run()
    obj = PaymentRunItem.objects.create_payment_run_item(
        payment_run, _instalment()
    )
    assert date.today() == obj.created.date()


@pytest.mark.django_db
def test_payment_run_item_str():
    payment_run = PaymentRun.objects.create_payment_run()
    instalment = _instalment()
    obj = PaymentRunItem.objects.create_payment_run_item(
        payment_run, instalment
    )
    assert "for instalment {}".format(instalment.pk) in str(obj)
