# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from django.urls import reverse
from unittest import mock

from base.tests.test_utils import PermTestCase
from base.url_utils import url_with_querystring
from checkout.tests.factories import (
    CustomerFactory,
    ObjectPaymentPlanFactory,
    ObjectPaymentPlanInstalmentFactory,
)
from contact.tests.factories import ContactFactory
from login.tests.fixture import perm_check
from .factories import SalesLedgerFactory


@pytest.mark.django_db
def test_contact_detail(perm_check):
    contact = ContactFactory()
    url = reverse("contact.detail", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_customer(perm_check):
    obj = CustomerFactory()
    url = url_with_querystring(reverse("checkout.customer"), email=obj.email)
    perm_check.staff(url)


@pytest.mark.django_db
def test_list(perm_check):
    perm_check.staff(reverse("checkout.list"))


@pytest.mark.django_db
def test_list_audit(perm_check):
    perm_check.staff(reverse("checkout.list.audit"))


@pytest.mark.django_db
def test_object_payment_plan_delete(perm_check):
    x = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    perm_check.staff(
        reverse("checkout.object.payment.plan.delete", args=[x.pk])
    )


@pytest.mark.django_db
def test_object_payment_plan_detail(perm_check):
    x = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    perm_check.staff(reverse("checkout.object.payment.plan", args=[x.pk]))


@pytest.mark.django_db
def test_object_payment_plan_instalment(perm_check):
    obj = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    perm_check.staff(
        reverse("checkout.object.payment.plan.instalment", args=[obj.pk])
    )


@pytest.mark.django_db
def test_object_payment_plan_instalment_charge(perm_check):
    obj = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    perm_check.staff(
        reverse("checkout.object.payment.plan.instalment.charge", args=[obj.pk])
    )


@pytest.mark.django_db
def test_object_payment_plan_instalment_email(perm_check):
    x = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    perm_check.staff(
        reverse("checkout.object.payment.plan.instalment.email", args=[x.pk])
    )


@pytest.mark.django_db
def test_object_payment_plan_instalment_paid(perm_check):
    obj = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    perm_check.staff(
        reverse("checkout.object.payment.plan.instalment.paid", args=[obj.pk])
    )


@pytest.mark.django_db
def test_object_payment_plan_card_fail_list(perm_check):
    perm_check.staff(reverse("checkout.object.payment.plan.card.fail.list"))


@pytest.mark.django_db
def test_object_payment_plan_instalment_audit_list(perm_check):
    perm_check.staff(
        reverse("checkout.object.payment.plan.instalment.audit.list")
    )


@pytest.mark.django_db
def test_object_payment_plan_list(perm_check):
    perm_check.staff(reverse("checkout.object.payment.plan.list"))


@pytest.mark.django_db
def test_pay_on_session_list_view(perm_check):
    perm_check.staff(reverse("checkout.list.pay.on.session"))


@pytest.mark.django_db
def test_payment_intent_list_view(perm_check):
    with mock.patch("stripe.PaymentIntent.list"):
        perm_check.superuser(reverse("checkout.list.payment.intent"))


@pytest.mark.django_db
def test_setup_intent_list_view(perm_check):
    with mock.patch("stripe.SetupIntent.list"):
        perm_check.superuser(reverse("checkout.list.setup.intent"))
