# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus
from unittest import mock

from checkout.models import Checkout, CheckoutAction, CheckoutError
from checkout.tests.factories import CheckoutFactory, PaymentPlanFactory
from checkout.tests.helper import MockIntent
from example_checkout.tests.factories import SalesLedgerFactory
from login.tests.fixture import UserFactory
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_checkout_payment_intent_get(client):
    """Collect payment from a user.

    Test the ``CheckoutPaymentIntentMixin``.

    .. note:: Is it possible to test the ``POST`` as Stripe does all the work in
              JavaScript?

    """
    user = UserFactory()
    sales_ledger = SalesLedgerFactory(
        quantity=2, product=ProductFactory(price=Decimal("10.00"))
    )
    with mock.patch("stripe.PaymentIntent.create") as mock_create, mock.patch(
        "stripe.PaymentIntent.retrieve"
    ) as mock_retrieve:
        mock_create.return_value = {"id": "543"}
        mock_retrieve.return_value = MockIntent(status="succeeded")
        checkout = Checkout.objects.create_checkout_payment_intent(
            sales_ledger, user
        )
        response = client.get(
            reverse(
                "example.sales.ledger.checkout.payment.intent",
                args=[checkout.uuid],
            )
        )
    assert HTTPStatus.OK == response.status_code
    assert "client_secret" in response.context
    assert "checkout_total" in response.context
    assert "shhhh" == response.context["client_secret"]
    assert Decimal("20") == response.context["checkout_total"]


@pytest.mark.django_db
def test_checkout_payment_intent_get_too_old(client):
    with freeze_time(date(2019, 6, 8)):
        checkout = CheckoutFactory(
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=SalesLedgerFactory(),
        )
    with freeze_time(date(2019, 9, 29)), pytest.raises(CheckoutError) as e:
        client.get(
            reverse(
                "example.sales.ledger.checkout.payment.intent",
                args=[checkout.uuid],
            )
        )
    assert (
        "Cannot view this checkout transaction.  It is too old "
        "(or has travelled in time, 08/06/2019 29/09/2019 82 days)"
    ) in str(e.value)


@pytest.mark.django_db
def test_checkout_setup_intent_get(client):
    """Setup a payment plan for a user (a Stripe ``setup_intent``).

    Test the ``CheckoutSetupIntentMixin``.

    .. note:: Is it possible to test the ``POST`` as Stripe does all the work in
              JavaScript?

    """
    user = UserFactory()
    sales_ledger = SalesLedgerFactory(
        quantity=2, product=ProductFactory(price=Decimal("10.00"))
    )
    with mock.patch("stripe.SetupIntent.create") as mock_create, mock.patch(
        "stripe.SetupIntent.retrieve"
    ) as mock_retrieve:
        mock_create.return_value = {"id": "654"}
        mock_retrieve.return_value = MockIntent(status="succeeded")
        checkout = Checkout.objects.create_checkout_setup_intent(
            sales_ledger, user, PaymentPlanFactory()
        )
        response = client.get(
            reverse(
                "example.sales.ledger.checkout.setup.intent",
                args=[checkout.uuid],
            )
        )
    assert HTTPStatus.OK == response.status_code
    assert "client_secret" in response.context
    assert "shhhh" == response.context["client_secret"]


# TODO Pre SCA - Delete each one as we re-create above :)


#
#
# @pytest.mark.django_db
# def test_post_card_payment_can_charge(client):
#    """Check if the object can be charged when we post to the form."""
#    with mock.patch("stripe.Charge.create"), mock.patch(
#        "stripe.Customer.create"
#    ) as mock_customer_create, mock.patch(
#        "example_checkout.models.SalesLedger.checkout_can_charge",
#        new_callable=mock.PropertyMock,
#    ) as mock_checkout_can_charge:
#        # mock return
#        data = {"id": "xyz"}
#        return_value = namedtuple("ReturnValue", data.keys())(**data)
#        mock_customer_create.return_value = return_value
#        mock_checkout_can_charge.return_value = False
#        # factories
#        NotifyFactory()
#        action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
#        state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
#        checkout = CheckoutFactory(
#            action=action,
#            content_object=SalesLedgerFactory(),
#            state=state,
#            total=Decimal("12.34"),
#        )
#        url = reverse("example.sales.ledger.checkout", args=[checkout.pk])
#        with pytest.raises(CheckoutError) as e:
#            client.post(url, {"stripeToken": "my-testing-token"})
#        assert "Cannot charge 'SalesLedger'" in str(e.value)
#
#
# @pytest.mark.django_db
# def test_post_card_payment_fail(client):
#    with mock.patch("stripe.Charge.create") as mock_charge, mock.patch(
#        "stripe.Customer.create"
#    ) as mock_customer_create:
#        # mock return
#        mock_charge.side_effect = CheckoutError("Mock")
#        data = {"id": "xyz"}
#        return_value = namedtuple("ReturnValue", data.keys())(**data)
#        mock_customer_create.return_value = return_value
#        # factories
#        NotifyFactory()
#        action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
#        state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
#        checkout = CheckoutFactory(
#            action=action,
#            content_object=SalesLedgerFactory(),
#            state=state,
#            total=Decimal("12.34"),
#        )
#        url = reverse("example.sales.ledger.checkout", args=[checkout.pk])
#        response = client.post(url, {"stripeToken": "my-testing-token"})
#        assert HTTPStatus.FOUND == response.status_code
#        assert 1 == Checkout.objects.count()
#        checkout.refresh_from_db()
#        # checkout = Checkout.objects.first()
#        assert CheckoutAction.PAYMENT == checkout.action.slug
#        assert Decimal("0") < checkout.total
#        # check email notification
#        assert 1 == Message.objects.count()
#        message = Message.objects.first()
#        assert "FAIL - Payment" in message.subject
#        assert "FAIL - Payment from" in message.description
#
#
# @pytest.mark.django_db
# def test_post_card_payment_plan(client):
#    with mock.patch("stripe.Customer.create") as mock_customer_create:
#        # mock return
#        data = {"id": "xyz"}
#        return_value = namedtuple("ReturnValue", data.keys())(**data)
#        mock_customer_create.return_value = return_value
#        # factories
#        CheckoutSettingsFactory()
#        NotifyFactory()
#        product = ProductFactory(price=Decimal("12.34"))
#        action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN)
#        state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
#        checkout = CheckoutFactory(
#            action=action,
#            content_object=SalesLedgerFactory(product=product),
#            state=state,
#            total=Decimal("12.34"),
#        )
#        url = reverse("example.sales.ledger.checkout", args=[checkout.pk])
#        response = client.post(url, {"stripeToken": "my-testing-token"})
#        assert HTTPStatus.FOUND == response.status_code
#        assert 1 == Checkout.objects.count()
#        # checkout = Checkout.objects.first()
#        checkout.refresh_from_db()
#        assert checkout.action.is_payment_plan() is True
#        assert Decimal("12.34") == checkout.total
#        # check email notification
#        assert 1 == Message.objects.count()
#        message = Message.objects.first()
#        assert "SUCCESS - Payment plan" in message.subject
#        assert "SUCCESS - Payment Plan from" in message.description
#
#
# @pytest.mark.django_db
# def test_post_card_refresh(client):
#    with mock.patch("stripe.Customer.create") as mock_customer_create:
#        # mock return
#        data = {"id": "xyz"}
#        return_value = namedtuple("ReturnValue", data.keys())(**data)
#        mock_customer_create.return_value = return_value
#        # factories
#        NotifyFactory()
#        action = CheckoutAction.objects.get(slug=CheckoutAction.CARD_REFRESH)
#        state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
#        checkout = CheckoutFactory(
#            action=action,
#            content_object=SalesLedgerFactory(),
#            state=state,
#            # total=Decimal("12.34"),
#        )
#        url = reverse("example.sales.ledger.checkout", args=[checkout.pk])
#        response = client.post(url, {"stripeToken": "my-testing-token"})
#        assert HTTPStatus.FOUND == response.status_code
#        assert 1 == Checkout.objects.count()
#        # checkout = Checkout.objects.first()
#        checkout.refresh_from_db()
#        assert CheckoutAction.CARD_REFRESH == checkout.action.slug
#        assert checkout.total is None
#        # check email notification
#        assert 1 == Message.objects.count()
#        message = Message.objects.first()
#        assert "SUCCESS - Card refresh" in message.subject
#        assert "SUCCESS - Card Refresh from" in message.description
#
#
# @pytest.mark.django_db
# def test_post_invoice(client):
#    NotifyFactory()
#    action = CheckoutAction.objects.get(slug=CheckoutAction.INVOICE)
#    state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
#    checkout = CheckoutFactory(
#        action=action,
#        content_object=SalesLedgerFactory(),
#        state=state,
#        total=Decimal("12.34"),
#    )
#    url = reverse("example.sales.ledger.checkout", args=[checkout.pk])
#    data = {
#        # "action": CheckoutAction.INVOICE,
#        "company_name": "KB",
#        "address_1": "My Address",
#        "town": "Hatherleigh",
#        "county": "Devon",
#        "postcode": "EX20",
#        "country": "UK",
#        "contact_name": "Patrick",
#        "email": "test@test.com",
#    }
#    response = client.post(url, data)
#    assert HTTPStatus.FOUND == response.status_code
#    assert 1 == Checkout.objects.count()
#    # checkout = Checkout.objects.first()
#    checkout.refresh_from_db()
#    assert checkout.action.is_invoice() is True
#    invoice = checkout.checkoutadditional
#    assert "test@test.com" == invoice.email
#    assert Decimal("0") < checkout.total
#    # check email notification
#    assert 1 == Message.objects.count()
#    message = Message.objects.first()
#    assert "SUCCESS - Invoice" in message.subject
#    assert "SUCCESS - Invoice" in message.description
