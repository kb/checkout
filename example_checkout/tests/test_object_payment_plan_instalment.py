# -*- encoding: utf-8 -*-
import pytest
import pytz
import urllib.parse

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.management import call_command
from django.db import transaction
from django.urls import reverse
from freezegun import freeze_time
from unittest import mock

from checkout.models import (
    Checkout,
    CheckoutError,
    CheckoutAction,
    CheckoutState,
    Customer,
    ObjectPaymentPlan,
    ObjectPaymentPlanInstalment,
)
from checkout.tests.factories import (
    CheckoutFactory,
    CheckoutStateFactory,
    CustomerFactory,
    ObjectPaymentPlanFactory,
    ObjectPaymentPlanInstalmentFactory,
    PaymentPlanFactory,
)
from checkout.tests.helper import check_checkout, MockIntent
from contact.tests.factories import ContactEmailFactory, ContactFactory
from login.tests.factories import UserFactory
from mail.models import MailTemplate, Message, Notify
from mail.tests.factories import (
    MailTemplateFactory,
    MailFactory,
    MessageFactory,
    NotifyFactory,
)
from .factories import ExampleContentObjectFactory, SalesLedgerFactory


def _can_login_and_pay(contact):
    return True


def _can_login_and_pay_not(contact):
    return False


@pytest.mark.django_db
def test_audit_content_type():
    x = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    action = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    CheckoutFactory(action=action, content_object=SalesLedgerFactory())
    checkout = CheckoutFactory(action=action, content_object=x)
    result = Checkout.objects.audit_content_type(
        ContentType.objects.get_for_model(ObjectPaymentPlanInstalment)
    )
    assert [checkout.pk] == [o.pk for o in result]


@pytest.mark.django_db
def test_can_charge_deposit_fail():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    instalment = ObjectPaymentPlanInstalmentFactory(
        deposit=True,
        due=date.today(),
        object_payment_plan=object_payment_plan,
        state=state,
    )
    assert instalment.checkout_can_charge is True


@pytest.mark.django_db
def test_can_charge_deposit_pending():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    instalment = ObjectPaymentPlanInstalmentFactory(
        deposit=True,
        due=date.today(),
        object_payment_plan=object_payment_plan,
        state=state,
    )
    assert instalment.checkout_can_charge is True


@pytest.mark.django_db
def test_can_charge_deposit_not_pending():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=CheckoutState.REQUEST)
    instalment = ObjectPaymentPlanInstalmentFactory(
        deposit=True,
        due=date.today(),
        object_payment_plan=object_payment_plan,
        state=state,
    )
    assert instalment.checkout_can_charge is False


@pytest.mark.django_db
def test_can_charge_overdue():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=CheckoutState.REQUEST)
    instalment = ObjectPaymentPlanInstalmentFactory(
        due=date.today() + relativedelta(days=-10),
        object_payment_plan=object_payment_plan,
        state=state,
    )
    assert instalment.checkout_can_charge is True


@pytest.mark.django_db
def test_can_charge_due_not_yet():
    """The payment is not due yet, so can we charge?

    PJK 18/03/2016  Changing the behaviour of the ``checkout_can_charge``
    method on ``ObjectPaymentPlanInstalment``.  We want to allow a member of
    staff to charge the card for payments which are not yet due.

    The automated routine uses the ``due`` method so it shouldn't take payments
    early.

    """
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=CheckoutState.REQUEST)
    instalment = ObjectPaymentPlanInstalmentFactory(
        due=date.today() + relativedelta(days=10),
        object_payment_plan=object_payment_plan,
        state=state,
    )
    assert instalment.checkout_can_charge is True


@pytest.mark.django_db
@pytest.mark.parametrize(
    "checkout_state",
    [
        CheckoutState.FAIL,
        CheckoutState.PAY_ON_SESSION,
        CheckoutState.PENDING,
        CheckoutState.REQUEST,
    ],
)
def test_can_charge_instalment(checkout_state):
    """Can we attempt a charge on the card for this instalment?

    04/09/2020, I am adding the ``FAIL`` state.  The ``charge_stripe`` method
    looks for *fail*, *pending* and *request* states, so we can do the same
    here.  We add *pay on session* because we might just want to try again.

    """
    call_command("init_app_checkout")
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=checkout_state)
    instalment = ObjectPaymentPlanInstalmentFactory(
        deposit=False,
        due=date.today(),
        object_payment_plan=object_payment_plan,
        state=state,
    )
    assert instalment.checkout_can_charge is True


@pytest.mark.django_db
def test_can_charge_pending():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    instalment = ObjectPaymentPlanInstalmentFactory(
        due=date.today() + relativedelta(days=-1),
        object_payment_plan=object_payment_plan,
        state=state,
    )
    assert instalment.checkout_can_charge is True


@pytest.mark.django_db
def test_can_charge_pending_plan_deleted():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory(), deleted=True
    )
    state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    instalment = ObjectPaymentPlanInstalmentFactory(
        due=date.today() + relativedelta(days=-1),
        object_payment_plan=object_payment_plan,
        state=state,
    )
    assert instalment.checkout_can_charge is False


@pytest.mark.django_db
def test_can_charge_success():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    instalment = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan, state=state
    )
    assert instalment.checkout_can_charge is False


@pytest.mark.django_db
def test_can_mark_paid():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    instalment = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan, state=state
    )
    assert instalment.can_mark_paid() is True


@pytest.mark.django_db
def test_can_mark_paid_deleted():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory(), deleted=True
    )
    state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    instalment = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan, state=state
    )
    assert instalment.can_mark_paid() is False


@pytest.mark.django_db
def test_charge_stripe_invalid_state():
    CustomerFactory(email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    x = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory(contact=contact)
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
    )
    with mock.patch("stripe.PaymentMethod.list") as mock_payment_method:
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        with pytest.raises(CheckoutError) as e:
            x.charge_stripe(UserFactory())
    assert (
        "Instalment '{}' must be in the failed, pending "
        "or request state: 'success'".format(x.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_charge_stripe_no_payment_method():
    customer = CustomerFactory(customer_id="A100", email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    x = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory(contact=contact)
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
    )
    with mock.patch("stripe.PaymentMethod.list") as mock_payment_method:
        mock_payment_method.return_value = {"data": {}}
        with pytest.raises(CheckoutError) as e:
            x.charge_stripe(UserFactory())
    assert (
        "No payment methods found for customer {}, 'A100'".format(customer.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_charge_stripe_requires_payment_method():
    CustomerFactory(email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    x = ObjectPaymentPlanInstalmentFactory(
        count=2,
        deposit=False,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory(contact=contact)
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.REQUEST),
    )
    assert 0 == Checkout.objects.count()
    with mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="pi_1ABCdA12abcdeAbCd3aABCD4",
            decline_code="insufficient_funds",
            message="Your card has insufficient funds.",
            status="requires_payment_method",
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        assert x.charge_stripe(UserFactory()) is False
    assert 1 == Checkout.objects.count()
    checkout = Checkout.objects.first()
    assert "pi_1ABCdA12abcdeAbCd3aABCD4" == checkout.payment_intent_id
    assert "insufficient_funds" == checkout.decline_code
    assert "Your card has insufficient funds." == checkout.decline_message
    x.refresh_from_db()
    assert CheckoutState.FAIL == x.state.slug


@pytest.mark.django_db
def test_charge_stripe_requires_payment_method_authentication_required():
    call_command("init_app_checkout")
    CustomerFactory(email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    x = ObjectPaymentPlanInstalmentFactory(
        count=2,
        deposit=False,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory(contact=contact)
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.REQUEST),
    )
    assert 0 == Checkout.objects.count()
    with mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="pi_1ABCdA12abcdeAbCd3aABCD4",
            decline_code=Checkout.AUTHENTICATION_REQUIRED,
            status="requires_payment_method",
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        assert x.charge_stripe(UserFactory()) is False
    assert 1 == Checkout.objects.count()
    checkout = Checkout.objects.first()
    assert "pi_1ABCdA12abcdeAbCd3aABCD4" == checkout.payment_intent_id
    assert "authentication_required" == checkout.decline_code
    assert "" == checkout.decline_message
    x.refresh_from_db()
    assert CheckoutState.PAY_ON_SESSION == x.state.slug


@pytest.mark.django_db
def test_charge_stripe_requires_payment_method_expired_card():
    call_command("init_app_checkout")
    CustomerFactory(email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    x = ObjectPaymentPlanInstalmentFactory(
        count=2,
        deposit=False,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory(contact=contact)
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.REQUEST),
    )
    assert 0 == Checkout.objects.count()
    with mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="pi_1ABCdA12abcdeAbCd3aABCD4",
            code="expired_card",
            message="Your card has expired.",
            status="requires_payment_method",
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        assert x.charge_stripe(UserFactory()) is False
    assert 1 == Checkout.objects.count()
    checkout = Checkout.objects.first()
    assert "pi_1ABCdA12abcdeAbCd3aABCD4" == checkout.payment_intent_id
    assert "expired_card" == checkout.decline_code
    assert "Your card has expired." == checkout.decline_message
    x.refresh_from_db()
    assert CheckoutState.PAY_ON_SESSION == x.state.slug


@pytest.mark.django_db
def test_check_checkout():
    with transaction.atomic():
        # this must be run within a transaction
        payment_plan = ObjectPaymentPlan.objects.create_object_payment_plan(
            SalesLedgerFactory(), PaymentPlanFactory(), Decimal("100")
        )
    instalment = ObjectPaymentPlanInstalment.objects.get(
        object_payment_plan=payment_plan
    )
    check_checkout(instalment)


@pytest.mark.django_db
def test_checkout_description():
    payment_plan = PaymentPlanFactory(
        name="pkimber", deposit=50, count=2, interval=1
    )
    # create the plan and the deposit
    contact_pp = ObjectPaymentPlan.objects.create_object_payment_plan(
        SalesLedgerFactory(), payment_plan, Decimal("100")
    )
    # check deposit description
    deposit = ObjectPaymentPlanInstalment.objects.filter(
        object_payment_plan=contact_pp
    )
    assert 1 == deposit.count()
    assert ["deposit"] == deposit[0].checkout_description
    # create the instalments
    contact_pp.create_instalments()
    # check
    instalments = ObjectPaymentPlanInstalment.objects.filter(
        object_payment_plan=contact_pp
    ).order_by("count")
    assert 3 == instalments.count()
    assert ["1 of 2 instalments"] == instalments[1].checkout_description


@pytest.mark.django_db
def test_checkout_fail():
    with transaction.atomic():
        # this must be run within a transaction
        payment_plan = ObjectPaymentPlan.objects.create_object_payment_plan(
            SalesLedgerFactory(), PaymentPlanFactory(), Decimal("100")
        )
    instalment = ObjectPaymentPlanInstalment.objects.get(
        object_payment_plan=payment_plan
    )
    action = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    checkout = CheckoutFactory(
        action=action, content_object=instalment, state=state
    )
    assert instalment.state.is_pending() is True
    instalment.checkout_fail(checkout)
    assert instalment.state.is_fail() is True


@pytest.mark.django_db
def test_checkout_fail_already_success():
    action_charge = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    state_success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    object_payment_plan_instalment = ObjectPaymentPlanInstalmentFactory(
        count=1,
        deposit=True,
        due=date.today(),
        object_payment_plan=object_payment_plan,
        state=state_success,
    )
    checkout = CheckoutFactory(
        action=action_charge,
        content_object=object_payment_plan_instalment,
        state=state_success,
    )
    with pytest.raises(CheckoutError) as e:
        object_payment_plan_instalment.checkout_fail(checkout)
    assert (
        "Instalment {} has already been paid successfully "
        "(checkout {})".format(object_payment_plan_instalment.pk, checkout.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_checkout_name():
    user = UserFactory(first_name="Patrick", last_name="Kimber")
    contact = ContactFactory(user=user)
    pp = ObjectPaymentPlan.objects.create_object_payment_plan(
        SalesLedgerFactory(contact=contact),
        PaymentPlanFactory(),
        Decimal("100"),
    )
    instalment = ObjectPaymentPlanInstalment.objects.get(object_payment_plan=pp)
    assert "Patrick Kimber" == instalment.checkout_name


@pytest.mark.django_db
def test_checkout_success():
    contact_pp = ObjectPaymentPlan.objects.create_object_payment_plan(
        SalesLedgerFactory(), PaymentPlanFactory(), Decimal("100")
    )
    instalment = ObjectPaymentPlanInstalment.objects.get(
        object_payment_plan=contact_pp
    )
    assert instalment.state.is_pending() is True
    action = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    checkout = CheckoutFactory(
        action=action, content_object=instalment, state=state
    )
    instalment.checkout_success(checkout)
    assert instalment.state.is_success() is True


@pytest.mark.django_db
def test_checkout_success_already_success():
    action_charge = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    state_success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    object_payment_plan_instalment = ObjectPaymentPlanInstalmentFactory(
        count=1,
        deposit=True,
        due=date.today(),
        object_payment_plan=object_payment_plan,
        state=state_success,
    )
    checkout = CheckoutFactory(
        action=action_charge,
        content_object=object_payment_plan_instalment,
        state=state_success,
    )
    with pytest.raises(CheckoutError) as e:
        object_payment_plan_instalment.checkout_success(checkout)
    assert (
        "Instalment {} has already been paid successfully "
        "(checkout {})".format(object_payment_plan_instalment.pk, checkout.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_checkout_total():
    payment_plan = PaymentPlanFactory(
        name="pkimber", deposit=50, count=2, interval=1
    )
    user = UserFactory(first_name="Patrick", last_name="Kimber")
    contact = ContactFactory(user=user)
    pp = ObjectPaymentPlan.objects.create_object_payment_plan(
        SalesLedgerFactory(contact=contact), payment_plan, Decimal("100")
    )
    pp.create_instalments()
    assert (
        Decimal("50")
        == ObjectPaymentPlanInstalment.objects.get(count=1).checkout_total
    )
    assert (
        Decimal("25")
        == ObjectPaymentPlanInstalment.objects.get(count=2).checkout_total
    )
    assert (
        Decimal("25")
        == ObjectPaymentPlanInstalment.objects.get(count=3).checkout_total
    )


@pytest.mark.django_db
def test_checkout_email():
    user = UserFactory(email="me@test.com")
    contact = ContactFactory(user=user)
    pp = ObjectPaymentPlan.objects.create_object_payment_plan(
        SalesLedgerFactory(contact=contact),
        PaymentPlanFactory(),
        Decimal("100"),
    )
    obj = ObjectPaymentPlanInstalment.objects.get(object_payment_plan=pp)
    assert "me@test.com" == obj.checkout_email


@pytest.mark.django_db
def test_create_pay_on_session_email(caplog):
    """

    For more testing of ``create_pay_on_session_email`` see
    ``test_send_on_session_payment_emails``.

    """
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
    payment_plan = PaymentPlanFactory(name="Fruit")
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory(), payment_plan=payment_plan
    )
    x = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan, due=date(2020, 8, 28)
    )
    with freeze_time(date(2020, 8, 17)):
        checkout = CheckoutFactory(
            action=action,
            description="b",
            content_object=x,
            state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
        )
    x.create_pay_on_session_email(UserFactory(), True)
    assert (
        "'create_pay_on_session_email', already has a successful checkout "
        "(id {} created 17/08/2020 00:00) for ObjectPaymentPlanInstalment "
        "id {} (Fruit 2020-08-28 99.99)".format(checkout.pk, x.pk)
    ) in str(caplog.records)


@pytest.mark.django_db
def test_current_instalment():
    plan = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        count=1,
        deposit=True,
        due=today + relativedelta(days=1),
        object_payment_plan=plan,
    )
    obj = ObjectPaymentPlanInstalmentFactory(
        count=2, due=today + relativedelta(days=2), object_payment_plan=plan
    )
    assert 1 == obj.current_instalment()


@pytest.mark.django_db
def test_current_instalment_count():
    plan = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        count=1,
        deposit=True,
        due=today + relativedelta(days=1),
        object_payment_plan=plan,
    )
    obj = ObjectPaymentPlanInstalmentFactory(
        count=0, due=today + relativedelta(days=2), object_payment_plan=plan
    )
    with pytest.raises(CheckoutError) as e:
        obj.current_instalment()
    assert "'count' value for an instalment should be greater" in str(e.value)


@pytest.mark.django_db
def test_current_instalment_deposit():
    plan = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    today = date.today()
    obj = ObjectPaymentPlanInstalmentFactory(
        count=1,
        deposit=True,
        due=today + relativedelta(days=1),
        object_payment_plan=plan,
    )
    with pytest.raises(CheckoutError) as e:
        obj.current_instalment()
    assert "Cannot ask the deposit record" in str(e.value)


# @pytest.mark.django_db
# def test_checkout_list():
#     c1 = CheckoutFactory(
#         action=CheckoutAction.objects.charge,
#         content_object=SalesLedgerFactory(),
#     )
#     c2 = CheckoutFactory(
#         action=CheckoutAction.objects.charge,
#         content_object=ObjectPaymentPlanInstalmentFactory(
#             object_payment_plan=ObjectPaymentPlanFactory(
#                 content_object=SalesLedgerFactory(),
#             ),
#         ),
#     )
#     c3 = CheckoutFactory(
#         action=CheckoutAction.objects.charge,
#         content_object=ObjectPaymentPlanInstalmentFactory(
#             object_payment_plan=ObjectPaymentPlanFactory(
#                 content_object=SalesLedgerFactory(),
#             ),
#         ),
#     )
#     checkout_list = ObjectPaymentPlanInstalment.objects.checkout_list
#     assert 2 == checkout_list.count()
#     assert c1 not in checkout_list
#     assert c2 in checkout_list
#     assert c3 in checkout_list


@pytest.mark.django_db
def test_due():
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        count=1,
        due=today + relativedelta(days=-1),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        count=2,
        due=today + relativedelta(days=-2),
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    result = [p.amount for p in ObjectPaymentPlanInstalment.objects.due()]
    assert [Decimal("1"), Decimal("2")] == result


@pytest.mark.django_db
def test_due_fail_retry():
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        count=1,
        due=today + relativedelta(days=-1),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        count=2,
        due=today + relativedelta(days=-2),
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    ObjectPaymentPlanInstalmentFactory(
        count=3,
        due=today + relativedelta(days=-2),
        amount=Decimal("3"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
        state=state,
        retry_count=99,
    )
    ObjectPaymentPlanInstalmentFactory(
        count=4,
        due=today + relativedelta(days=-2),
        amount=Decimal("4"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
        state=state,
        retry_count=2,
    )
    qs = ObjectPaymentPlanInstalment.objects.due().order_by("amount")
    result = [p.amount for p in qs]
    assert [Decimal("1"), Decimal("2"), Decimal("4")] == result


@pytest.mark.django_db
def test_due_plan_deleted():
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-1),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    object_payment_plan = ObjectPaymentPlanFactory(
        deleted=True, content_object=SalesLedgerFactory()
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        due=today + relativedelta(days=2),
        amount=Decimal("2"),
    )
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-3),
        amount=Decimal("3"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    result = [p.amount for p in ObjectPaymentPlanInstalment.objects.due()]
    assert [Decimal("1"), Decimal("3")] == result


@pytest.mark.django_db
def test_due_plan_deposit():
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-1),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        deposit=True,
        due=today + relativedelta(days=-2),
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-3),
        amount=Decimal("3"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    result = [p.amount for p in ObjectPaymentPlanInstalment.objects.due()]
    assert [Decimal("1"), Decimal("3")] == result


@pytest.mark.django_db
def test_due_not_due():
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-1),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=1),
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-3),
        amount=Decimal("3"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    result = [p.amount for p in ObjectPaymentPlanInstalment.objects.due()]
    assert [Decimal("1"), Decimal("3")] == result


@pytest.mark.django_db
def test_due_not_pending():
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-1),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-2),
        state=state,
        retry_count=99,
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-3),
        amount=Decimal("3"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    result = [p.amount for p in ObjectPaymentPlanInstalment.objects.due()]
    assert [Decimal("1"), Decimal("3")] == result


@pytest.mark.django_db
def test_due_within_date_range():
    """We must only retry payments which are within x days of the due date."""
    today = date.today()
    for count in range(10):
        ObjectPaymentPlanInstalmentFactory(
            count=count,
            due=today + relativedelta(days=-count),
            amount=Decimal(count),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=SalesLedgerFactory()
            ),
        )
    result = [p.amount for p in ObjectPaymentPlanInstalment.objects.due()]
    result.sort()
    assert [
        Decimal("0"),
        Decimal("1"),
        Decimal("2"),
        Decimal("3"),
        Decimal("4"),
    ] == result


@pytest.mark.django_db
def test_factory():
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )


@pytest.mark.django_db
def test_for_content_object():
    x = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    action = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    CheckoutFactory(action=action, content_object=SalesLedgerFactory())
    checkout = CheckoutFactory(action=action, content_object=x)
    result = Checkout.objects.for_content_object(x)
    assert [checkout.pk] == [o.pk for o in result]


@pytest.mark.django_db
@pytest.mark.parametrize(
    "deposit_due_date", [date(2015, 3, 10), date(2015, 3, 1), date(2015, 2, 10)]
)
def test_create_instalments_first_of_month(deposit_due_date):
    """Test create instalments.

    .. note:: The instalment dates are calculated from ``now`` *not* from the
              deposit due date: https://www.kbsoftware.co.uk/crm/ticket/3604/

    """
    obj = ObjectPaymentPlanInstalmentFactory(
        amount=Decimal("50"),
        count=1,
        deposit=True,
        due=deposit_due_date,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    with freeze_time("2015-03-10"):
        obj.object_payment_plan.create_instalments()
    # deposit
    assert (
        deposit_due_date == ObjectPaymentPlanInstalment.objects.get(count=1).due
    )
    # instalment 1
    assert (
        date(2015, 4, 1) == ObjectPaymentPlanInstalment.objects.get(count=2).due
    )
    # instalment 2
    assert (
        date(2015, 5, 1) == ObjectPaymentPlanInstalment.objects.get(count=3).due
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "deposit_due_date", [date(2015, 3, 17), date(2015, 3, 1), date(2015, 2, 10)]
)
def test_create_instalments_first_of_month_after_15th(deposit_due_date):
    """Test create instalments.

    .. note:: The instalment dates are calculated from ``now`` *not* from the
              deposit due date: https://www.kbsoftware.co.uk/crm/ticket/3604/

    """
    obj = ObjectPaymentPlanInstalmentFactory(
        amount=Decimal("50"),
        count=1,
        deposit=True,
        due=deposit_due_date,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    with freeze_time("2015-03-17"):
        obj.object_payment_plan.create_instalments()
    # deposit
    assert (
        deposit_due_date == ObjectPaymentPlanInstalment.objects.get(count=1).due
    )
    # instalment 1
    assert (
        date(2015, 5, 1) == ObjectPaymentPlanInstalment.objects.get(count=2).due
    )
    # instalment 2
    assert (
        date(2015, 6, 1) == ObjectPaymentPlanInstalment.objects.get(count=3).due
    )


@pytest.mark.django_db
def test_failure_emails():
    contact_1 = ContactFactory(user=UserFactory(email="one@pkimber.net"))
    contact_2 = ContactFactory(user=UserFactory(email="two@pkimber.net"))
    contact_3 = ContactFactory(user=UserFactory(email="three@pkimber.net"))
    contact_4 = ContactFactory(user=UserFactory(email="four@pkimber.net"))
    contact_5 = ContactFactory(user=UserFactory(email="five@pkimber.net"))
    contact_6 = ContactFactory(user=UserFactory(email="six@pkimber.net"))
    sales_ledger_1 = SalesLedgerFactory(contact=contact_1)
    sales_ledger_2 = SalesLedgerFactory(contact=contact_2)
    sales_ledger_3 = SalesLedgerFactory(contact=contact_3)
    sales_ledger_4 = SalesLedgerFactory(contact=contact_4)
    sales_ledger_5 = SalesLedgerFactory(contact=contact_5)
    sales_ledger_6 = SalesLedgerFactory(contact=contact_6)
    template = MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_PAYMENT_FAILED)
    with freeze_time("2019-06-08"):
        message = MessageFactory(
            content_object=sales_ledger_2, template=template
        )
        MailFactory(email="two@pkimber.net", message=message)
    with freeze_time("2019-06-10"):
        instalment_1 = ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 6, 1),
            amount=Decimal("1"),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=sales_ledger_1
            ),
        )
        # instalment 2 is not included because an email has already been sent
        ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 6, 1),
            amount=Decimal("2"),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=sales_ledger_2
            ),
        )
        # instalment 3
        object_payment_plan_3 = ObjectPaymentPlanFactory(
            content_object=sales_ledger_3
        )
        instalment_3 = ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 6, 1),
            amount=Decimal("3"),
            object_payment_plan=object_payment_plan_3,
            state=CheckoutState.objects.get(slug=CheckoutState.FAIL),
        )
        # check the method selects the first instalment i.e. ``instalment_3``.
        ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 6, 2),
            amount=Decimal("3.1"),
            object_payment_plan=object_payment_plan_3,
        )
        # instalment 4 is due in 8 days time, so won't be included
        ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 7, 1),
            amount=Decimal("4"),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=sales_ledger_4
            ),
        )
        # instalment 5 is not included because it succeeded
        ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 6, 1),
            amount=Decimal("5"),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=sales_ledger_5
            ),
            state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
        )
        instalment_6 = ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 6, 1),
            amount=Decimal("6"),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=sales_ledger_6
            ),
            retry_count=99,
        )
        assert {
            "one@pkimber.net": instalment_1.pk,
            "three@pkimber.net": instalment_3.pk,
            "six@pkimber.net": instalment_6.pk,
        } == ObjectPaymentPlanInstalment.objects.failure_emails()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "checkout_state_slug",
    [
        CheckoutState.FAIL,
        CheckoutState.PENDING,
        CheckoutState.REQUEST,
        CheckoutState.SUCCESS,
    ],
)
def test_pay_on_session(checkout_state_slug):
    contact_1 = ContactFactory(
        user=UserFactory(email="a@pkimber.net", first_name="A", last_name="A")
    )
    contact_2 = ContactFactory(
        user=UserFactory(email="b@pkimber.net", first_name="Bob", last_name="B")
    )
    CheckoutStateFactory(slug=CheckoutState.PAY_ON_SESSION)
    sales_ledger_1 = SalesLedgerFactory(contact=contact_1)
    sales_ledger_2 = SalesLedgerFactory(contact=contact_2)
    ObjectPaymentPlanInstalmentFactory(
        due=date(2019, 5, 30),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_1
        ),
        state=CheckoutState.objects.get(slug=checkout_state_slug),
    )
    instalment_2 = ObjectPaymentPlanInstalmentFactory(
        count=2,
        due=date(2019, 5, 30),
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_2
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION),
    )
    assert [instalment_2.pk] == [
        x.pk for x in ObjectPaymentPlanInstalment.objects.pay_on_session()
    ]


@pytest.mark.django_db
def test_pay_on_session_deleted_object_payment_plan():
    contact = ContactFactory(
        user=UserFactory(email="a@pkimber.net", first_name="A", last_name="A")
    )
    CheckoutStateFactory(slug=CheckoutState.PAY_ON_SESSION)
    sales_ledger = SalesLedgerFactory(contact=contact)
    object_payment_plan = ObjectPaymentPlanFactory(content_object=sales_ledger)
    object_payment_plan.set_deleted(UserFactory())
    ObjectPaymentPlanInstalmentFactory(
        due=date(2019, 5, 30),
        amount=Decimal("1"),
        object_payment_plan=object_payment_plan,
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
    )
    ObjectPaymentPlanInstalmentFactory(
        count=2,
        due=date(2019, 6, 30),
        amount=Decimal("2"),
        object_payment_plan=object_payment_plan,
        state=CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION),
    )
    assert 0 == ObjectPaymentPlanInstalment.objects.pay_on_session().count()


@pytest.mark.django_db
def test_pay_on_session_for_email():
    contact_1 = ContactFactory(user=UserFactory())
    ContactEmailFactory(contact=contact_1, email="a@pkimber.net")
    contact_2 = ContactFactory(user=UserFactory())
    ContactEmailFactory(contact=contact_2, email="b@pkimber.net")
    CheckoutStateFactory(slug=CheckoutState.PAY_ON_SESSION)
    sales_ledger_1 = SalesLedgerFactory(contact=contact_1)
    sales_ledger_2 = SalesLedgerFactory(contact=contact_2)
    object_payment_plan_1 = ObjectPaymentPlanFactory(
        content_object=sales_ledger_1
    )
    object_payment_plan_2 = ObjectPaymentPlanFactory(
        content_object=sales_ledger_2
    )
    ObjectPaymentPlanInstalmentFactory(
        due=date(2019, 5, 30),
        amount=Decimal("1"),
        object_payment_plan=object_payment_plan_1,
        state=CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION),
    )
    instalment_2 = ObjectPaymentPlanInstalmentFactory(
        count=2,
        due=date(2019, 5, 30),
        amount=Decimal("2"),
        object_payment_plan=object_payment_plan_2,
        state=CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION),
    )
    assert [instalment_2.pk] == [
        x.pk
        for x in ObjectPaymentPlanInstalment.objects.pay_on_session_for_email(
            "b@pkimber.net"
        )
    ]


@pytest.mark.django_db
def test_pks_with_email():
    # one, two and three
    pks = []
    for name in ("one", "two"):
        contact = ContactFactory(user=UserFactory())
        ContactEmailFactory(
            contact=contact, email="{}@pkimber.net".format(name)
        )
        x = ObjectPaymentPlanInstalmentFactory(
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=SalesLedgerFactory(contact=contact),
                payment_plan=PaymentPlanFactory(name="{}-sales".format(name)),
            )
        )
        pks.append(x.pk)
    # four, five and six
    for name in ("one", "three"):
        x = ObjectPaymentPlanInstalmentFactory(
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=ExampleContentObjectFactory(
                    description=name, email="{}@pkimber.net".format(name)
                ),
                payment_plan=PaymentPlanFactory(name="{}-example".format(name)),
            )
        )
        pks.append(x.pk)
    result = ObjectPaymentPlanInstalment.objects.pks_with_email(
        "one@pkimber.net", pks
    )
    assert 2 == len(result)
    assert set(["one-sales", "one-example"]) == set(
        [
            x.object_payment_plan.payment_plan.name
            for x in ObjectPaymentPlanInstalment.objects.filter(pk__in=result)
        ]
    )


@pytest.mark.django_db
def test_reminder():
    """Automated Payment Reminder.

    1. I think I need to get ``outstanding_payment_plans``
       (``ObjectPaymentPlanManager``)
       This doesn't seem to worry about any dates.
    2. We can check the mail tables to see if we have already reminded the user
       this month.
    3. It would be good if the task could be run every day, but to do that I
       will need to check to see if the payment is due within the next 7 days.
       I can do that by checking the ``due`` field.
    4. I need to make sure we don't include deposit records in this list.

    https://www.kbsoftware.co.uk/crm/ticket/3085/

    """
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-1),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=5),
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-3),
        amount=Decimal("3"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    result = [x.amount for x in ObjectPaymentPlanInstalment.objects.reminder()]
    assert [Decimal("2")] == result


@pytest.mark.django_db
def test_reminder_emails():
    today = date.today()
    contact_1 = ContactFactory(
        user=UserFactory(
            email="one@pkimber.net", first_name="Patrick", last_name="K"
        )
    )
    contact_2 = ContactFactory(user=UserFactory(email="two@pkimber.net"))
    contact_3 = ContactFactory(user=UserFactory(email="three@pkimber.net"))
    contact_4 = ContactFactory(user=UserFactory(email="four@pkimber.net"))
    sales_ledger_1 = SalesLedgerFactory(contact=contact_1)
    sales_ledger_2 = SalesLedgerFactory(contact=contact_2)
    sales_ledger_3 = SalesLedgerFactory(contact=contact_3)
    sales_ledger_4 = SalesLedgerFactory(contact=contact_4)
    template = MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_REMINDER)
    message = MessageFactory(content_object=sales_ledger_2, template=template)
    MailFactory(email="two@pkimber.net", message=message)
    instalment_1_due = today + relativedelta(days=4)
    instalment_1 = ObjectPaymentPlanInstalmentFactory(
        due=instalment_1_due,
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_1
        ),
    )
    # instalment 2 is not included because an email has already been sent
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=5),
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_2
        ),
    )
    # instalment 3
    object_payment_plan_3 = ObjectPaymentPlanFactory(
        content_object=sales_ledger_3
    )
    instalment_3 = ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=5),
        amount=Decimal("3"),
        object_payment_plan=object_payment_plan_3,
    )
    # check the method selects the first instalment i.e. ``instalment_3``.
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=6),
        amount=Decimal("3.1"),
        object_payment_plan=object_payment_plan_3,
    )
    # instalment 4 is due in 8 days time, so won't be included
    ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=8),
        amount=Decimal("4"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_4
        ),
    )
    assert {
        "one@pkimber.net": instalment_1.pk,
        "three@pkimber.net": instalment_3.pk,
    } == ObjectPaymentPlanInstalment.objects.reminder_emails()


@pytest.mark.django_db
def test_send_failure_emails():
    NotifyFactory(email="patrick@kbsoftware.co.uk")
    contact_1 = ContactFactory(
        user=UserFactory(email="a@pkimber.net", first_name="A", last_name="B")
    )
    contact_2 = ContactFactory(
        user=UserFactory(email="x@pkimber.net", first_name="X", last_name="Y")
    )
    sales_ledger_1 = SalesLedgerFactory(contact=contact_1)
    sales_ledger_2 = SalesLedgerFactory(contact=contact_2)
    MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_PAYMENT_FAILED)
    with freeze_time("2019-06-08"):
        instalment_1 = ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 6, 1),
            amount=Decimal("1"),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=sales_ledger_1
            ),
        )
        instalment_2 = ObjectPaymentPlanInstalmentFactory(
            due=date(2019, 5, 30),
            amount=Decimal("2"),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=sales_ledger_2
            ),
            state=CheckoutState.objects.get(slug=CheckoutState.FAIL),
        )
        ObjectPaymentPlanInstalment.objects.send_failure_emails()
    assert 3 == Message.objects.count()
    # instalment_1
    qs = Message.objects.for_content_object(instalment_1)
    assert 1 == qs.count()
    message = qs.first()
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert {"name": "A"} == {x.key: x.value for x in mail.mailfield_set.all()}
    # instalment_2
    qs = Message.objects.for_content_object(instalment_2)
    assert 1 == qs.count()
    message = qs.first()
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert {"name": "X"} == {x.key: x.value for x in mail.mailfield_set.all()}
    # notify
    qs = Message.objects.for_content_object(Notify.objects.first())
    assert 1 == qs.count()
    message = qs.first()
    assert "Payment run - failure emails" == message.subject
    assert "Sent 2 payment run failure emails" == message.description
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert "patrick@kbsoftware.co.uk" == mail.email


@pytest.mark.django_db
@pytest.mark.parametrize(
    # "count,deposit,template_slug,template_slug_not_checked,last_login,has_url",
    "count,deposit,template_slug,last_login,has_url",
    [
        # (
        #    2,
        #    False,
        #    ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH,
        #    # ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON,
        #    datetime(2019, 5, 1, 0, 0, 0, tzinfo=pytz.utc),
        #    False,
        # ),
        (
            2,
            False,
            ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON,
            # ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH,
            datetime(2019, 1, 1, 0, 0, 0, tzinfo=pytz.utc),
            True,
        ),
        (
            1,
            True,
            ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON,
            # ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH,
            None,
            True,
        ),
        # (
        #    1,
        #    True,
        #    ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH,
        #    # ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON,
        #    datetime(2019, 5, 1, 0, 0, 0, tzinfo=pytz.utc),
        #    False,
        # ),
    ],
)
def test_send_on_session_payment_emails(
    count,
    deposit,
    template_slug,
    # template_slug_not_checked,
    last_login,
    has_url,
):
    """Email the user if Stripe requests an on on-session payment.

    - Check to see if the user has an active account (logged in recently).
    - If they do, then use an email template telling them to log in and pay.
    - If they don't, send an email with a checkout URL for payment.

    For more testing of ``create_pay_on_session_email`` see
    ``test_create_pay_on_session_email``.

    """
    call_command("init_app_checkout")
    NotifyFactory(email="patrick@kbsoftware.co.uk")
    # contact 1
    user_1 = UserFactory(first_name="A", last_name="A", last_login=last_login)
    contact_1 = ContactFactory(user=user_1)
    ContactEmailFactory(contact=contact_1, email="a@pkimber.net")
    sales_ledger_1 = SalesLedgerFactory(contact=contact_1)
    # contact 2
    user_2 = UserFactory(first_name="Bob", last_name="B", last_login=last_login)
    contact_2 = ContactFactory(user=user_2)
    ContactEmailFactory(contact=contact_2, email="b@pkimber.net")
    sales_ledger_2 = SalesLedgerFactory(contact=contact_2)
    # contact 3
    user_3 = UserFactory(first_name="C", last_name="C", last_login=last_login)
    contact_3 = ContactFactory(user=user_3)
    ContactEmailFactory(contact=contact_3, email="c@pkimber.net")
    sales_ledger_3 = SalesLedgerFactory(contact=contact_3)
    # customer
    CustomerFactory(email="b@pkimber.net")
    with freeze_time(date(2019, 5, 30)):
        # already sent a notification to this email address
        message = MessageFactory(
            content_object=sales_ledger_3,
            template=MailTemplate.objects.get(slug=template_slug),
        )
        MailFactory(email="c@pkimber.net", message=message)
    ObjectPaymentPlanInstalmentFactory(
        due=date(2019, 5, 30),
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_1
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.FAIL),
    )
    instalment_2 = ObjectPaymentPlanInstalmentFactory(
        count=count,
        deposit=deposit,
        due=date(2019, 5, 30),
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_2
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION),
    )
    ObjectPaymentPlanInstalmentFactory(
        count=3,
        due=date(2019, 5, 30),
        amount=Decimal("3"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_3
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION),
    )
    with mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method, mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, freeze_time(
        date(2019, 6, 3)
    ):
        mock_payment_intent.return_value = {"id": "543"}
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        ObjectPaymentPlanInstalment.objects.send_on_session_payment_emails(
            _can_login_and_pay
        )
    # checkout
    assert 2 == Message.objects.exclude(pk=message.pk).count()
    # instalment_2
    qs = Message.objects.for_content_object(instalment_2)
    assert 1 == qs.count()
    message = qs.first()
    assert 1 == message.mail_set.count()
    assert template_slug == message.template.slug
    mail = message.mail_set.first()
    assert "b@pkimber.net" == mail.email
    mail_field_expect = {
        "name": "Bob",
        "amount": "2.00",
    }
    qs = Checkout.objects.for_content_object(instalment_2)
    if deposit:
        mail_field_expect.update({"description": "deposit"})
    else:
        mail_field_expect.update({"description": "1 of 1 instalments"})
    if has_url:
        assert 1 == qs.count()
        checkout = qs.first()
        url = urllib.parse.urljoin(
            settings.HOST_NAME,
            reverse("web.checkout.pay.on.session", args=[checkout.uuid]),
        )
        mail_field_expect.update(
            {"url": '<a href="{}" mc:disable-tracking>{}</a>'.format(url, url)}
        )
    else:
        assert 0 == qs.count()
    assert mail_field_expect == {
        x.key: x.value for x in mail.mailfield_set.all()
    }
    # notify
    qs = Message.objects.for_content_object(Notify.objects.first())
    assert 1 == qs.count()
    message = qs.first()
    assert "On session payment emails" == message.subject
    assert "Sent 1 on session payment emails" == message.description
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert "patrick@kbsoftware.co.uk" == mail.email
    mail_qs = instalment_2.mail()
    assert 1 == mail_qs.count()
    mail = mail_qs.first()
    assert "b@pkimber.net" == mail.email


@pytest.mark.django_db
def test_send_payment_reminder_emails():
    today = date.today()
    contact_1 = ContactFactory(
        user=UserFactory(email="a@pkimber.net", first_name="a", last_name="B")
    )
    contact_2 = ContactFactory(
        user=UserFactory(email="x@pkimber.net", first_name="X", last_name="Y")
    )
    sales_ledger_1 = SalesLedgerFactory(contact=contact_1)
    sales_ledger_2 = SalesLedgerFactory(contact=contact_2)
    MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_REMINDER)
    instalment_1_due = today + relativedelta(days=4)
    instalment_1 = ObjectPaymentPlanInstalmentFactory(
        due=instalment_1_due,
        amount=Decimal("1"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_1
        ),
    )
    instalment_2_due = today + relativedelta(days=5)
    instalment_2 = ObjectPaymentPlanInstalmentFactory(
        due=instalment_2_due,
        amount=Decimal("2"),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger_2
        ),
    )
    ObjectPaymentPlanInstalment.objects.send_payment_reminder_emails()
    assert 2 == Message.objects.count()
    # instalment_1
    qs = Message.objects.for_content_object(instalment_1)
    assert 1 == qs.count()
    message = qs.first()
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert 2 == mail.mailfield_set.count()
    assert {"name": "A", "due": instalment_1_due.strftime("%a %d %b %Y")} == {
        x.key: x.value for x in mail.mailfield_set.all()
    }
    # instalment_2
    qs = Message.objects.for_content_object(instalment_2)
    assert 1 == qs.count()
    message = qs.first()
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert 2 == mail.mailfield_set.count()
    assert {"name": "X", "due": instalment_2_due.strftime("%a %d %b %Y")} == {
        x.key: x.value for x in mail.mailfield_set.all()
    }


@pytest.mark.django_db
def test_str():
    str(
        ObjectPaymentPlanInstalmentFactory(
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=SalesLedgerFactory()
            )
        )
    )


@pytest.mark.parametrize(
    "last_login,can_login_and_pay,expect",
    [
        (
            datetime(2019, 5, 1, 0, 0, 0, tzinfo=pytz.utc),
            _can_login_and_pay,
            False,
        ),
        (
            datetime(2019, 10, 1, 0, 0, 0, tzinfo=pytz.utc),
            _can_login_and_pay,
            True,
        ),
        (
            datetime(2019, 11, 10, 0, 0, 0, tzinfo=pytz.utc),
            _can_login_and_pay,
            True,
        ),
        (
            datetime(2050, 1, 1, 0, 0, 0, tzinfo=pytz.utc),
            _can_login_and_pay,
            True,
        ),
        (
            datetime(2019, 11, 10, 0, 0, 0, tzinfo=pytz.utc),
            _can_login_and_pay_not,
            False,
        ),
    ],
)
@pytest.mark.django_db
def test_user_has_active_account(last_login, can_login_and_pay, expect):
    user = UserFactory(email="code@pkimber.net", last_login=last_login)
    contact = ContactFactory(user=user)
    ContactEmailFactory(contact=contact, email=user.email)
    with freeze_time("2019-11-14"):
        result = ObjectPaymentPlanInstalment.objects._user_has_active_account(
            user.email, can_login_and_pay
        )
    assert expect == result
