# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.core.management import call_command
from unittest import mock

from checkout.models import (
    Checkout,
    CheckoutError,
    CheckoutState,
    PaymentRun,
    PaymentRunItem,
)
from checkout.tests.factories import (
    CustomerFactory,
    ObjectPaymentPlanFactory,
    ObjectPaymentPlanInstalmentFactory,
)
from checkout.tests.helper import MockIntent
from contact.tests.factories import ContactFactory
from example_checkout.tests.factories import SalesLedgerFactory
from login.tests.factories import UserFactory
from mail.models import Message
from mail.tests.factories import NotifyFactory


def _instalment():
    """Create an instalment."""
    return ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
    )


@pytest.mark.django_db
def test_item_count():
    obj = PaymentRun.objects.create_payment_run()
    PaymentRunItem.objects.create_payment_run_item(obj, _instalment())
    PaymentRunItem.objects.create_payment_run_item(obj, _instalment())
    assert 2 == obj.item_count()


@pytest.mark.django_db
def test_item_count_zero():
    obj = PaymentRun.objects.create_payment_run()
    assert 0 == obj.item_count()


@pytest.mark.django_db
def test_manager():
    obj = PaymentRun.objects.create_payment_run()
    assert date.today() == obj.created.date()


@pytest.mark.django_db
def test_notify_does_not_exist(caplog):
    payment_run = PaymentRun.objects.create_payment_run()
    PaymentRun.objects.notify(payment_run, 1)
    assert (
        "Cannot send email notification of payment transactions.  "
        "No email addresses set-up in 'enquiry.models.Notify'"
    ) in str(caplog.records)


@pytest.mark.django_db
def test_process_payments():
    """Process payments."""
    UserFactory(username=Checkout.SYSTEM_GENERATED_USER_NAME)
    today = date.today()
    install = ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-1),
        amount=Decimal("2"),
        count=2,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
    )
    CustomerFactory(
        email=install.object_payment_plan.content_object.checkout_email
    )
    NotifyFactory()
    with mock.patch("stripe.Customer.create"), mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="323", status="succeeded"
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        assert (1, 0) == PaymentRun.objects.process_payments()
    install.refresh_from_db()
    assert install.state.is_success() is True
    assert 1 == install.retry_count
    # payment run
    assert 1 == PaymentRun.objects.count()
    payment_run = PaymentRun.objects.first()
    assert 1 == payment_run.paymentrunitem_set.count()
    payment_run_item = payment_run.paymentrunitem_set.first()
    assert install.pk == payment_run_item.instalment.pk
    assert payment_run_item.checkout is not None
    assert payment_run_item.checkout.state.is_success() is True
    # mail
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert "Payment plan activity update" in message.subject
    assert "Processed 1 payment transaction" in message.description
    assert "failed" not in message.description


@pytest.mark.django_db
def test_process_payments_retry():
    """Process various payments."""
    today = date.today()
    UserFactory(username=Checkout.SYSTEM_GENERATED_USER_NAME)
    install_1 = ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=1),
        amount=Decimal("1"),
        count=2,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    install_2 = ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-1),
        amount=Decimal("2"),
        count=2,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.FAIL),
    )
    install_3 = ObjectPaymentPlanInstalmentFactory(
        due=today + relativedelta(days=-2),
        amount=Decimal("3"),
        count=2,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
        retry_count=1,
    )
    CustomerFactory(
        email=install_2.object_payment_plan.content_object.checkout_email
    )
    CustomerFactory(
        email=install_3.object_payment_plan.content_object.checkout_email
    )
    NotifyFactory()
    with mock.patch("stripe.Customer.create"), mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="212", status="succeeded"
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        assert (2, 0) == PaymentRun.objects.process_payments()
    # check
    install_1.refresh_from_db()
    assert install_1.state.is_pending() is True
    assert install_1.retry_count is None
    install_2.refresh_from_db()
    assert install_2.state.is_success() is True
    assert 1 == install_2.retry_count
    install_3.refresh_from_db()
    assert install_3.state.is_success() is True
    assert 2 == install_3.retry_count
    # payment run
    assert 1 == PaymentRun.objects.count()
    payment_run = PaymentRun.objects.first()
    assert 2 == payment_run.paymentrunitem_set.count()
    # mail
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert "Payment plan activity update" in message.subject
    assert "Processed 2 payment transaction" in message.description
    assert "failed" not in message.description


@pytest.mark.django_db
def test_process_payments_fail():
    """Process payments."""
    UserFactory(username=Checkout.SYSTEM_GENERATED_USER_NAME)
    with mock.patch("stripe.Customer.create") as mock_customer, mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.side_effect = CheckoutError("Mock")
        mock_payment_intent.return_value = MockIntent(
            intent_id="323", status="failed"
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        today = date.today()
        install = ObjectPaymentPlanInstalmentFactory(
            amount=Decimal("1"),
            count=3,
            due=today + relativedelta(days=-1),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=SalesLedgerFactory()
            ),
        )
        CustomerFactory(
            email=install.object_payment_plan.content_object.checkout_email
        )
        NotifyFactory()
        assert (1, 1) == PaymentRun.objects.process_payments()
        # check
        install.refresh_from_db()
        assert install.state.is_fail() is True
        assert 1 == Message.objects.count()
        message = Message.objects.first()
        assert "Payment plan activity update" in message.subject
        assert "Processed 1 payment transaction" in message.description
        assert "1 failed transaction" in message.description


@pytest.mark.django_db
def test_process_payments_fail_on_session():
    """Process payments.

    TODO Make sure we send an email to tell the user to login and authenticate
    the on-session transaction.

    """
    call_command("init_app_checkout")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    UserFactory(username=Checkout.SYSTEM_GENERATED_USER_NAME)
    with mock.patch("stripe.Customer.create") as mock_customer, mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.side_effect = CheckoutError("Mock")
        mock_payment_intent.return_value = MockIntent(
            intent_id="545",
            decline_code=Checkout.AUTHENTICATION_REQUIRED,
            status="requires_payment_method",
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        today = date.today()
        install = ObjectPaymentPlanInstalmentFactory(
            amount=Decimal("1"),
            count=3,
            due=today + relativedelta(days=-1),
            object_payment_plan=ObjectPaymentPlanFactory(
                content_object=SalesLedgerFactory(contact=contact)
            ),
        )
        CustomerFactory(
            email=install.object_payment_plan.content_object.checkout_email
        )
        NotifyFactory()
        assert (1, 1) == PaymentRun.objects.process_payments()
        # check
        install.refresh_from_db()
        assert install.state.is_pay_on_session() is True
        assert 1 == Message.objects.count()
        message = Message.objects.first()
        assert "Payment plan activity update" in message.subject
        assert "Processed 1 payment transaction" in message.description
        assert "1 failed transaction" in message.description
        # 27/09/2019, duplicate code.
        # See ``MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH``
        # message_2 = Message.objects.last()
        # assert "Login and authorise your payment" in message_2.subject
        # assert "" == message_2.description
        # assert 1 == message_2.mail_set.count()
        # mail = message_2.mail_set.first()
        # assert {"name": "Patrick", "description": "2 of 1 instalments"} == {
        #    x.key: x.value for x in mail.mailfield_set.all()
        # }


@pytest.mark.django_db
def test_str():
    obj = PaymentRun.objects.create_payment_run()
    value = str(obj)
    assert "Payment Run" in value
    assert str(obj.pk) in value
    assert "created" in value
