# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.core.management import call_command
from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus
from unittest import mock

from checkout.models import Checkout, CheckoutAction, CheckoutState, Customer
from checkout.tests.factories import (
    CustomerFactory,
    ObjectPaymentPlanFactory,
    ObjectPaymentPlanInstalmentFactory,
)
from checkout.tests.helper import MockIntent
from contact.tests.factories import ContactFactory, ContactEmailFactory
from example_checkout.tests.factories import SalesLedgerFactory
from login.tests.fixture import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_checkout_refresh_card(client):
    call_command("init_app_checkout")
    # CheckoutSettingsFactory()
    assert 0 == Checkout.objects.count()
    assert 0 == Customer.objects.count()
    # contact
    user = UserFactory(first_name="Patrick")
    contact = ContactFactory(user=user)
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    # payment plan
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory(contact=contact)
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        count=1,
        deposit=True,
        due=date.today() + relativedelta(days=-7),
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
        amount=Decimal("20"),
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        count=2,
        deposit=False,
        due=date.today() + relativedelta(days=-6),
        state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
        amount=Decimal("10"),
    )
    # test
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with mock.patch(
        "stripe.SetupIntent.create"
    ) as mock_setup_intent_create, mock.patch(
        "stripe.SetupIntent.retrieve"
    ) as mock_setup_intent_retrieve:
        mock_setup_intent_create.return_value = {"id": "654"}
        mock_setup_intent_retrieve.return_value = MockIntent(status="succeeded")
        response = client.get(reverse("web.checkout.refresh.card"))
    assert HTTPStatus.OK == response.status_code
    assert 1 == Checkout.objects.count()
    checkout = Checkout.objects.first()
    # copied from 'test_create_checkout_setup_intent_card_refresh'
    assert "" == checkout.payment_intent_id
    assert "654" == checkout.setup_intent_id
    assert CheckoutAction.CARD_REFRESH == checkout.action.slug
    assert CheckoutState.PENDING == checkout.state.slug
    assert date.today() == checkout.checkout_date.date()
    assert object_payment_plan == checkout.content_object
    assert "1 payments remaining" == checkout.description
    assert Decimal("10") == checkout.total
    assert user == checkout.user


@pytest.mark.django_db
def test_object_payment_plan_delete(client):
    user = UserFactory(is_staff=True)
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with freeze_time(datetime(2020, 8, 17, 15, 23, tzinfo=pytz.utc)):
        response = client.post(
            reverse(
                "checkout.object.payment.plan.delete",
                args=[object_payment_plan.pk],
            )
        )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("checkout.object.payment.plan.list") == response.url
    object_payment_plan.refresh_from_db()
    assert object_payment_plan.is_deleted is True
    assert user == object_payment_plan.user_deleted
    assert (
        datetime(2020, 8, 17, 15, 23, tzinfo=pytz.utc)
        == object_payment_plan.date_deleted
    )
