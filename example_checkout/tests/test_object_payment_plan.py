# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.core.management import call_command
from django.db import transaction
from unittest import mock

from checkout.models import (
    CheckoutError,
    CheckoutState,
    Customer,
    ObjectPaymentPlan,
)
from checkout.tests.factories import (
    CustomerFactory,
    ObjectPaymentPlanFactory,
    ObjectPaymentPlanInstalmentFactory,
    PaymentPlanFactory,
)
from checkout.tests.helper import MockIntent
from contact.tests.factories import ContactEmailFactory, ContactFactory
from login.tests.factories import UserFactory
from mail.models import Message
from mail.tests.factories import MailTemplateFactory
from stock.tests.factories import ProductFactory
from .factories import ExampleContentObjectFactory, SalesLedgerFactory


@pytest.mark.django_db
def test_charge_deposit():
    CustomerFactory(email="code@pkimber.net")
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("12.34")),
    )
    x = ObjectPaymentPlanFactory(content_object=content_object)
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=x,
        deposit=True,
        due=today + relativedelta(days=1),
        count=1,
    )
    with mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="987", status="succeeded"
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        assert x.charge_deposit(UserFactory()) is True


@pytest.mark.django_db
def test_checkout_description():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        deposit=True,
        due=date.today() + relativedelta(days=-1),
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
        count=1,
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        deposit=False,
        due=date.today(),
        count=2,
    )
    assert ["1 payments remaining"] == object_payment_plan.checkout_description


@pytest.mark.django_db
def test_checkout_email():
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory(contact=contact),
    )
    assert "code@pkimber.net" == object_payment_plan.checkout_email


@pytest.mark.django_db
def test_checkout_name():
    contact = ContactFactory(
        user=UserFactory(first_name="P", last_name="Kimber")
    )
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory(contact=contact),
    )
    assert "P Kimber" == object_payment_plan.checkout_name


@pytest.mark.django_db
def test_create_object_payment_plan():
    sl = SalesLedgerFactory()
    payment_plan = PaymentPlanFactory(deposit=20, count=2, interval=1)
    # create the contact plan with the deposit
    with transaction.atomic():
        # this must be run within a transaction
        ObjectPaymentPlan.objects.create_object_payment_plan(
            sl, payment_plan, Decimal("100")
        )
    object_payment_plan = ObjectPaymentPlan.objects.for_content_object(sl)
    # check deposit - count should be '1' and the 'due' date ``today``
    result = [
        (p.count, p.amount, p.due) for p in object_payment_plan.payments()
    ]
    assert [(1, Decimal("20"), date.today())] == result
    # create the instalments
    with transaction.atomic():
        # this must be run within a transaction
        object_payment_plan.create_instalments()
    result = [
        (p.count, p.amount, p.due) for p in object_payment_plan.payments()
    ]
    offset = 0
    # instalments start a month later if after the 15th of the month
    if date.today().day > 15:
        offset = 1
    assert [
        (1, Decimal("20"), date.today()),
        (
            2,
            Decimal("40"),
            date.today() + relativedelta(months=+(1 + offset), day=1),
        ),
        (
            3,
            Decimal("40"),
            date.today() + relativedelta(months=+(2 + offset), day=1),
        ),
    ] == result


@pytest.mark.django_db
def test_create_instalments_once_only():
    sl = SalesLedgerFactory()
    payment_plan = PaymentPlanFactory(deposit=20, count=2, interval=1)
    # create the contact plan with the deposit
    with transaction.atomic():
        # this must be run within a transaction
        pp = ObjectPaymentPlan.objects.create_object_payment_plan(
            sl, payment_plan, Decimal("100")
        )
    # create the instalments
    with transaction.atomic():
        # this must be run within a transaction
        pp.create_instalments()
    with pytest.raises(CheckoutError) as e:
        pp.create_instalments()
    assert "instalments already created" in str(e.value)


@pytest.mark.django_db
def test_create_instalments_no_deposit():
    obj = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    with pytest.raises(CheckoutError) as e:
        obj.create_instalments()
    assert "no deposit/instalment record" in str(e.value)


@pytest.mark.django_db
def test_create_instalments_corrupt():
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    obj = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan, count=2
    )
    with pytest.raises(CheckoutError) as e:
        obj.object_payment_plan.create_instalments()
    assert "no deposit record" in str(e.value)


@pytest.mark.django_db
def test_factory():
    ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())


@pytest.mark.django_db
def test_fail_or_request():
    """Payment plans which have an instalment in the fail or request state."""
    p1 = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=p1,
        due=date.today() + relativedelta(months=-2),
        state=CheckoutState.objects.get(slug=CheckoutState.FAIL),
    )
    p2 = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=p2,
        due=date.today() + relativedelta(months=-1),
        state=CheckoutState.objects.get(slug=CheckoutState.REQUEST),
    )
    p3 = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=p3,
        due=date.today() + relativedelta(months=-1),
        state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
    )
    result = ObjectPaymentPlan.objects.fail_or_request()
    assert [p1.pk, p2.pk] == [obj.pk for obj in result]


@pytest.mark.django_db
def test_fail_or_request_duplicate():
    """Payment plans which have an instalment in the fail or request state."""
    obj = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=obj,
        due=date.today() + relativedelta(months=-2),
        state=CheckoutState.objects.get(slug=CheckoutState.FAIL),
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=obj,
        due=date.today() + relativedelta(months=-1),
        state=CheckoutState.objects.get(slug=CheckoutState.REQUEST),
    )
    result = ObjectPaymentPlan.objects.fail_or_request()
    assert [obj.pk] == [obj.pk for obj in result]


# @pytest.mark.django_db
# def test_for_emails_version_1(django_assert_num_queries):
#    for name in ("one", "two", "three"):
#        contact = ContactFactory(user=UserFactory())
#        ContactEmailFactory(
#            contact=contact, email="{}@pkimber.net".format(name)
#        )
#        ObjectPaymentPlanFactory(
#            content_object=SalesLedgerFactory(contact=contact),
#            payment_plan=PaymentPlanFactory(name=name),
#        )
#    with django_assert_num_queries(8):
#        qs = ObjectPaymentPlan.objects.for_emails_version_1(
#            ["one@pkimber.net", "three@pkimber.net"]
#        )
#    assert set(["one", "three"]) == set([x.payment_plan.name for x in qs])


@pytest.mark.django_db
def test_for_email(django_assert_num_queries):
    # one, two and three
    for name in ("one", "two"):
        contact = ContactFactory(user=UserFactory())
        ContactEmailFactory(
            contact=contact, email="{}@pkimber.net".format(name)
        )
        ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory(contact=contact),
            payment_plan=PaymentPlanFactory(name="{}-sales".format(name)),
        )
    # four, five and six
    for name in ("one", "three"):
        ObjectPaymentPlanFactory(
            content_object=ExampleContentObjectFactory(
                description=name, email="{}@pkimber.net".format(name)
            ),
            payment_plan=PaymentPlanFactory(name="{}-example".format(name)),
        )
    # nine
    ExampleContentObjectFactory(description="four", email="one@pkimber.net")
    # ten
    contact = ContactFactory(user=UserFactory())
    ContactEmailFactory(contact=contact, email="one@pkimber.net")
    SalesLedgerFactory(contact=contact)
    #
    # object_pks = [x.pk for x in PaymentPlan.objects.filter(name="one")]
    # test (nine and ten are not linked to payment plans)
    with django_assert_num_queries(8):
        qs = ObjectPaymentPlan.objects.for_email(
            "one@pkimber.net"
        )  # , object_pks)
        #    [
        #        "one@pkimber.net",
        #        "three@pkimber.net",
        #        "five@pkimber.net",
        #        "nine@pkimber.net",
        #        "ten@pkimber.net",
        #    ]
        # )
    # assert set(["one", "three", "five"]) == set(
    assert set(["one-example", "one-sales"]) == set(
        [x.payment_plan.name for x in qs]
    )


@pytest.mark.django_db
def test_instalment_count():
    """See ``payment_count``."""
    obj = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=obj,
        deposit=True,
        due=today + relativedelta(days=1),
        count=1,
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=obj, due=today + relativedelta(days=2), count=2
    )
    assert 1 == obj.instalment_count()


@pytest.mark.parametrize(
    "state", [CheckoutState.FAIL, CheckoutState.PENDING, CheckoutState.REQUEST]
)
@pytest.mark.django_db
def test_instalments_due(state):
    today = date.today()
    checkout_state = CheckoutState.objects.get(slug=state)
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        deposit=True,
        due=today + relativedelta(days=-30),
        count=1,
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        due=today + relativedelta(days=-15),
        state=checkout_state,
        count=2,
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan, due=today, count=3
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        due=today + relativedelta(days=+30),
        count=4,
    )
    assert set([1, 2]) == set(
        [x.count for x in object_payment_plan.instalments_due()]
    )


@pytest.mark.parametrize(
    "state", [CheckoutState.FAIL, CheckoutState.PENDING, CheckoutState.REQUEST]
)
@pytest.mark.django_db
def test_instalments_due_exclude_by_due_date(state):
    today = date.today()
    checkout_state = CheckoutState.objects.get(slug=state)
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory()
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        deposit=True,
        due=today + relativedelta(days=-30),
        count=1,
    )
    obj = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        due=today + relativedelta(days=-15),
        state=checkout_state,
        count=2,
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan, due=today, count=3
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan,
        due=today + relativedelta(days=+30),
        count=4,
    )
    qs = object_payment_plan.instalments_due(obj.due)
    assert [1] == [x.count for x in qs]


@pytest.mark.django_db
def test_is_deposit_paid():
    x = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=x,
        deposit=True,
        due=today + relativedelta(days=1),
        count=1,
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=x, due=today + relativedelta(days=2), count=2
    )
    assert x.is_deposit_paid() is True


@pytest.mark.django_db
def test_is_deposit_paid_no_instalments():
    x = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    today = date.today()
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=x,
        deposit=True,
        due=today + relativedelta(days=1),
        count=1,
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
    )
    with pytest.raises(CheckoutError) as e:
        x.is_deposit_paid()
    assert (
        "The deposit for payment plan '{}' has been paid, but the "
        "instalments were not created".format(x.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_is_deposit_paid_not():
    today = date.today()
    x = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=x,
        deposit=True,
        due=today + relativedelta(days=1),
        count=1,
        state=CheckoutState.objects.get(slug=CheckoutState.PENDING),
    )
    assert x.is_deposit_paid() is False


@pytest.mark.django_db
def test_outstanding_payment_plans():
    assert 0 == ObjectPaymentPlan.objects.outstanding_payment_plans().count()


@pytest.mark.django_db
def test_outstanding_payment_plans_exclude_deleted():
    obj = ObjectPaymentPlanFactory(
        content_object=SalesLedgerFactory(), deleted=True
    )
    ObjectPaymentPlanInstalmentFactory(object_payment_plan=obj)
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    assert 1 == ObjectPaymentPlan.objects.outstanding_payment_plans().count()


@pytest.mark.django_db
def test_outstanding_payment_plans_exclude_success():
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    ObjectPaymentPlanInstalmentFactory(
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        ),
    )
    assert 1 == ObjectPaymentPlan.objects.outstanding_payment_plans().count()


@pytest.mark.django_db
def test_outstanding_payment_plans_filter_two():
    obj = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    ObjectPaymentPlanInstalmentFactory(object_payment_plan=obj)
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=obj, due=date.today() + relativedelta(months=+1)
    )
    assert 1 == ObjectPaymentPlan.objects.outstanding_payment_plans().count()


@pytest.mark.django_db
def test_payment_count():
    """See ``instalment_count``."""
    obj = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=obj,
        deposit=True,
        due=date.today() + relativedelta(days=1),
        count=1,
    )
    ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=obj,
        due=date.today() + relativedelta(days=2),
        count=2,
    )
    assert 2 == obj.payment_count()


@pytest.mark.django_db
def test_pks_with_email():
    # one, two and three
    pks = []
    for name in ("one", "two"):
        contact = ContactFactory(user=UserFactory())
        ContactEmailFactory(
            contact=contact, email="{}@pkimber.net".format(name)
        )
        x = ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory(contact=contact),
            payment_plan=PaymentPlanFactory(name="{}-sales".format(name)),
        )
        pks.append(x.pk)
    # four, five and six
    for name in ("one", "three"):
        x = ObjectPaymentPlanFactory(
            content_object=ExampleContentObjectFactory(
                description=name, email="{}@pkimber.net".format(name)
            ),
            payment_plan=PaymentPlanFactory(name="{}-example".format(name)),
        )
        pks.append(x.pk)
    # nine
    ExampleContentObjectFactory(description="four", email="one@pkimber.net")
    # ten
    contact = ContactFactory(user=UserFactory())
    ContactEmailFactory(contact=contact, email="one@pkimber.net")
    SalesLedgerFactory(contact=contact)
    # test (nine and ten are not linked to payment plans)
    result = ObjectPaymentPlan.objects.pks_with_email("one@pkimber.net", pks)
    assert 2 == len(result)
    assert set(["one-sales", "one-example"]) == set(
        [
            x.payment_plan.name
            for x in ObjectPaymentPlan.objects.filter(pk__in=result)
        ]
    )


@pytest.mark.django_db
def test_refresh_card_expiry_dates():
    MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_CARD_EXPIRY)
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = []
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 8, "exp_year": 1986},
                    "created": 123,
                }
            ]
        }
        contact = ContactFactory(user=UserFactory(first_name="Pat"))
        object_payment_plan = ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory(contact=contact)
        )
        ObjectPaymentPlanInstalmentFactory(
            object_payment_plan=object_payment_plan
        )
        ObjectPaymentPlanInstalmentFactory(
            object_payment_plan=object_payment_plan,
            due=date.today() + relativedelta(months=+1),
        )
        customer = CustomerFactory(
            email=object_payment_plan.content_object.checkout_email
        )
        ObjectPaymentPlan.objects.refresh_card_expiry_dates()
        customer.refresh_from_db()
        assert customer.refresh is True
        # check email template context
        assert 1 == Message.objects.count()
        message = Message.objects.first()
        assert 1 == message.mail_set.count()
        mail = message.mail_set.first()
        assert 1 == mail.mailfield_set.count()
        # the email of the customer matches the email of the contact,
        # so use the first name of the user
        assert {"name": "Pat"} == {
            f.key: f.value for f in mail.mailfield_set.all()
        }


@pytest.mark.django_db
def test_refresh_card_expiry_dates_future():
    MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_CARD_EXPIRY)
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = []
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2050},
                    "created": 123,
                }
            ]
        }
        obj = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
        ObjectPaymentPlanInstalmentFactory(object_payment_plan=obj)
        ObjectPaymentPlanInstalmentFactory(
            object_payment_plan=obj, due=date.today() + relativedelta(months=+1)
        )
        customer = CustomerFactory(email=obj.content_object.checkout_email)
        ObjectPaymentPlan.objects.refresh_card_expiry_dates()
        customer.refresh_from_db()
        assert customer.refresh is False
        # check we didn't send a notification email
        assert 0 == Message.objects.count()


@pytest.mark.django_db
def test_refresh_card_expiry_dates_refreshed():
    MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_CARD_EXPIRY)
    """Customer card already marked for 'refresh', so don't send an email."""
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = []
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 8, "exp_year": 1986},
                    "created": 123,
                }
            ]
        }
        obj = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
        ObjectPaymentPlanInstalmentFactory(object_payment_plan=obj)
        ObjectPaymentPlanInstalmentFactory(
            object_payment_plan=obj, due=date.today() + relativedelta(months=+1)
        )
        customer = CustomerFactory(
            email=obj.content_object.checkout_email, refresh=True
        )
        ObjectPaymentPlan.objects.refresh_card_expiry_dates()
        customer.refresh_from_db()
        assert customer.refresh is True
        # check we didn't send a notification email
        assert 0 == Message.objects.count()


# @pytest.mark.django_db
# def test_report_card_expiry_dates():
#    object_payment_plan = ObjectPaymentPlanFactory(
#        content_object=SalesLedgerFactory()
#    )
#    ObjectPaymentPlanInstalmentFactory(object_payment_plan=object_payment_plan)
#    obj = ObjectPaymentPlanInstalmentFactory(
#        object_payment_plan=ObjectPaymentPlanFactory(
#            content_object=SalesLedgerFactory()
#        )
#    )
#    CustomerFactory(email=obj.object_payment_plan.content_object.checkout_email)
#    ObjectPaymentPlan.objects.report_card_expiry_dates


@pytest.mark.django_db
def test_set_deleted():
    x = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    assert x.deleted is False
    x.set_deleted(UserFactory())
    x.refresh_from_db()
    assert x.deleted is True
    assert x.is_deleted is True


@pytest.mark.django_db
def test_str():
    str(ObjectPaymentPlanFactory(content_object=SalesLedgerFactory()))


@pytest.mark.django_db
def test_total_outstanding():
    call_command("init_app_checkout")
    x = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    deposit_date = date.today() + relativedelta(months=-1)
    # make x instalments... (not in the normal order etc)
    # all but ``CheckoutState.SUCCESS`` should be ignored
    for count, state in enumerate(CheckoutState.objects.all(), 1):
        ObjectPaymentPlanInstalmentFactory(
            object_payment_plan=x,
            count=count,
            deposit=True if count == 1 else False,
            due=deposit_date + relativedelta(days=count),
            state=state,
            amount=Decimal("10"),
        )
        print(count, state)
    expect = Decimal(count - 1) * Decimal("10")
    assert expect == x.total_outstanding()


@pytest.mark.django_db
def test_total_outstanding_nothing():
    x = ObjectPaymentPlanFactory(content_object=SalesLedgerFactory())
    assert Decimal() == x.total_outstanding()
