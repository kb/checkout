# -*- encoding: utf-8 -*-
import pytest

from example_checkout.tasks import send_on_session_payment_emails


@pytest.mark.django_db
def test_send_on_session_payment_emails():
    send_on_session_payment_emails()
