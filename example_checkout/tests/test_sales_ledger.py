# -*- encoding: utf-8 -*-
import pytest

from checkout.tests.helper import check_checkout, check_object_payment_plan
from example_checkout.tests.factories import SalesLedgerFactory
from finance.tests.factories import VatSettingsFactory


@pytest.mark.django_db
def test_check_checkout():
    VatSettingsFactory()
    obj = SalesLedgerFactory()
    check_checkout(obj)


@pytest.mark.django_db
def test_check_object_payment_plan():
    obj = SalesLedgerFactory()
    check_object_payment_plan(obj)
