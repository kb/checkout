# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal

from checkout.models import CheckoutAction, CheckoutError
from checkout.tests.factories import CheckoutFactory, PaymentPlanFactory
from example_checkout.tests.factories import SalesLedgerFactory
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_payment_plan_example():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
        content_object=SalesLedgerFactory(
            product=ProductFactory(price=Decimal("50")), quantity=2
        ),
        payment_plan=PaymentPlanFactory(deposit=50, count=2, interval=1),
    )
    result = []
    for instalment_date, value in checkout.payment_plan_example():
        result.append(value)
    assert [Decimal("50"), Decimal("25"), Decimal("25")] == result


@pytest.mark.django_db
def test_payment_plan_example_not_payment_plan():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.INVOICE),
        content_object=SalesLedgerFactory(),
    )
    with pytest.raises(CheckoutError) as e:
        checkout.payment_plan_example()
    assert (
        "Cannot get an example payment plan "
        "for the 'invoice' action: {}".format(checkout.pk)
    ) in str(e.value)
