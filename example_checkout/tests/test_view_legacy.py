# -*- encoding: utf-8 -*-
import json
import pytest
import pytz

from datetime import datetime
from decimal import Decimal
from django.core.management import call_command
from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus
from unittest import mock

from checkout.models import Checkout, CheckoutAction, CheckoutState
from checkout.tests.factories import CustomerFactory
from checkout.tests.helper import MockIntent
from contact.tests.factories import ContactFactory
from example_checkout.tests.factories import SalesLedgerFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.models import Message
from mail.tests.factories import NotifyFactory
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_example_checkout_paynow_or_invoice(client):
    call_command("init_app_checkout")
    user = UserFactory(email="code@pkimber.net", first_name="Patrick")
    NotifyFactory(email="notify@pkimber.net")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    CustomerFactory(email="code@pkimber.net"),
    with mock.patch("stripe.PaymentIntent.create") as mock_create, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_list, mock.patch(
        "stripe.PaymentIntent.retrieve"
    ) as mock_retrieve:
        mock_create.return_value = {"id": "543"}
        mock_list.return_value = {"data": [{"id": "876"}]}
        mock_retrieve.return_value = MockIntent(status="succeeded")
        with freeze_time(datetime(2019, 8, 20, 6, 1, 0, tzinfo=pytz.utc)):
            checkout = Checkout.objects.create_checkout_payment_intent(
                SalesLedgerFactory(
                    contact=ContactFactory(user=user),
                    product=ProductFactory(name="Apple", price=Decimal("22")),
                ),
                user,
            )
        response = client.post(
            reverse("example.checkout.paynow.or.invoice", args=[checkout.uuid]),
            data={
                "action_slug": CheckoutAction.INVOICE,
                "company_name": "KB",
                "address_1": "130 High Street",
                "town": "Crediton",
                "county": "Devon",
                "postcode": "EX17 3LF",
                "country": "UK",
                "contact_name": "Patrick",
                "email": "testing@pkimber.net",
                "phone": "01837",
            },
        )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("project.home") == response.url
    checkout.refresh_from_db()
    assert CheckoutAction.INVOICE == checkout.action.slug
    assert [
        "Patrick",
        "KB",
        "130 High Street",
        "Crediton",
        "Devon",
        "EX17 3LF",
        "UK",
        "testing@pkimber.net",
        "01837",
    ] == list(checkout.invoice_data())
    assert CheckoutState.SUCCESS == checkout.state.slug
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert "SUCCESS - Invoice from Patrick" == message.subject
    assert (
        "20/08/2019 06:01 - SUCCESS - Invoice from Patrick, "
        "code@pkimber.net:"
        "\n\nApple x 1 @ 22\n\n"
        "\n\nInvoice: Patrick, KB, 130 High Street, "
        "Crediton, Devon, EX17 3LF, UK, testing@pkimber.net, 01837"
    ) == message.description


@pytest.mark.django_db
def test_example_checkout_paynow_or_invoice_get(client):
    call_command("init_app_checkout")
    user = UserFactory(email="code@pkimber.net")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    CustomerFactory(email="code@pkimber.net"),
    with mock.patch("stripe.PaymentIntent.create") as mock_create, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_list, mock.patch(
        "stripe.PaymentIntent.retrieve"
    ) as mock_retrieve:
        mock_create.return_value = {"id": "543"}
        mock_list.return_value = {"data": [{"id": "876"}]}
        mock_retrieve.return_value = MockIntent(status="succeeded")
        checkout = Checkout.objects.create_checkout_payment_intent(
            SalesLedgerFactory(
                contact=ContactFactory(user=user),
                product=ProductFactory(name="Apple", price=Decimal("22")),
            ),
            user,
        )
        response = client.get(
            reverse("example.checkout.paynow.or.invoice", args=[checkout.uuid])
        )
    assert HTTPStatus.OK == response.status_code
    for key in [
        "action_data",
        "allow_pay_by_invoice",
        "checkout_email",
        "checkout_total",
        "client_secret",
        "description",
        "success_url",
    ]:
        assert key in response.context
    assert {
        "payment": {"name": "Payment", "payment": True},
        "invoice": {"name": "Invoice", "payment": False},
    } == json.loads(response.context["action_data"])
    assert response.context["allow_pay_by_invoice"] is True
    assert "shhhh" == response.context["client_secret"]
    assert ["Apple x 1 @ 22.00"] == response.context["description"]
    assert "code@pkimber.net" == response.context["checkout_email"]
    assert reverse("project.home") == response.context["success_url"]
    assert Decimal("2200") == response.context["checkout_total"]


@pytest.mark.django_db
def test_example_checkout_paynow_or_invoice_post_no_company_name(client):
    call_command("init_app_checkout")
    user = UserFactory(email="code@pkimber.net")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    CustomerFactory(email="code@pkimber.net"),
    with mock.patch("stripe.PaymentIntent.create") as mock_create, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_list, mock.patch(
        "stripe.PaymentIntent.retrieve"
    ) as mock_retrieve:
        mock_create.return_value = {"id": "543"}
        mock_list.return_value = {"data": [{"id": "876"}]}
        mock_retrieve.return_value = MockIntent(status="succeeded")
        checkout = Checkout.objects.create_checkout_payment_intent(
            SalesLedgerFactory(
                contact=ContactFactory(user=user),
                product=ProductFactory(name="Apple", price=Decimal("22")),
            ),
            user,
        )
        response = client.post(
            reverse("example.checkout.paynow.or.invoice", args=[checkout.uuid]),
            data={
                "action_slug": CheckoutAction.INVOICE,
                "address_1": "130 High Street",
                "town": "Crediton",
                "county": "Devon",
                "postcode": "EX17 3LF",
                "country": "UK",
                "contact_name": "Patrick",
                "email": "testing@pkimber.net",
                "phone": "01837",
            },
        )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert {
        "__all__": [
            (
                "Please enter an invoice company name, address, "
                "town, county, postcode, country, contact name and "
                "an email address or phone number."
            )
        ]
    } == form.errors


@pytest.mark.django_db
def test_example_checkout_paynow_or_invoice_post_no_email_or_phone(client):
    call_command("init_app_checkout")
    user = UserFactory(email="code@pkimber.net")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    CustomerFactory(email="code@pkimber.net"),
    with mock.patch("stripe.PaymentIntent.create") as mock_create, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_list, mock.patch(
        "stripe.PaymentIntent.retrieve"
    ) as mock_retrieve:
        mock_create.return_value = {"id": "543"}
        mock_list.return_value = {"data": [{"id": "876"}]}
        mock_retrieve.return_value = MockIntent(status="succeeded")
        checkout = Checkout.objects.create_checkout_payment_intent(
            SalesLedgerFactory(
                contact=ContactFactory(user=user),
                product=ProductFactory(name="Apple", price=Decimal("22")),
            ),
            user,
        )
        response = client.post(
            reverse("example.checkout.paynow.or.invoice", args=[checkout.uuid]),
            data={
                "action_slug": CheckoutAction.INVOICE,
                "company_name": "KB",
                "address_1": "130 High Street",
                "town": "Crediton",
                "county": "Devon",
                "postcode": "EX17 3LF",
                "country": "UK",
                "contact_name": "Patrick",
            },
        )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert {
        "__all__": ["Please enter an email address or phone number."]
    } == form.errors
