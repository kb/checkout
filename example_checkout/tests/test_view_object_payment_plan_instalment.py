# -*- encoding: utf-8 -*-
import logging
import pytest
import urllib.parse

from datetime import date
from decimal import Decimal
from django.conf import settings
from django.core.management import call_command
from django.urls import reverse
from http import HTTPStatus
from unittest import mock

from checkout.models import (
    Checkout,
    CheckoutAction,
    CheckoutError,
    CheckoutState,
)
from checkout.tests.factories import (
    CheckoutFactory,
    CheckoutStateFactory,
    CustomerFactory,
    ObjectPaymentPlanFactory,
    ObjectPaymentPlanInstalmentFactory,
)
from checkout.tests.helper import MockIntent
from contact.tests.factories import ContactFactory, ContactEmailFactory
from example_checkout.tests.factories import SalesLedgerFactory
from login.tests.fixture import TEST_PASSWORD, UserFactory
from mail.models import Message, Notify
from mail.tests.factories import NotifyFactory


@pytest.mark.django_db
def test_charge_stripe(client):
    sales_ledger = SalesLedgerFactory()
    CustomerFactory(email=sales_ledger.contact.user.email)
    obj = ObjectPaymentPlanInstalmentFactory(
        amount=Decimal("12.34"),
        count=2,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.REQUEST),
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse(
        "checkout.object.payment.plan.instalment.charge", args=[obj.pk]
    )
    with mock.patch("stripe.Customer.create"), mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="323", status="succeeded"
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    # assert "/checkout/object/payment/plan/" in response.url
    assert (
        reverse(
            "checkout.object.payment.plan",
            args=[obj.object_payment_plan.pk],
        )
        == response.url
    )
    assert Message.objects.first() is None
    qs = Checkout.objects.for_content_object(obj)
    assert 1 == qs.count()
    checkout = qs.first()
    assert Decimal("12.34") == checkout.total
    assert CheckoutAction.CHARGE == checkout.action.slug
    assert CheckoutState.SUCCESS == checkout.state.slug
    # check the message
    response = client.get(response.url)
    assert "messages" in response.context
    messages = response.context["messages"]
    assert 1 == len(messages)
    for msg in messages:
        # only one message
        assert "Charged" in msg.message
        assert "a total of 12.34 ref 1 of 1 instalments" in msg.message


@pytest.mark.django_db
def test_charge_stripe_already_paid(client):
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    sales_ledger = SalesLedgerFactory(contact=contact)
    CustomerFactory(email=sales_ledger.contact.user.email)
    object_payment_plan_instalment = ObjectPaymentPlanInstalmentFactory(
        amount=Decimal("12.34"),
        count=2,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.REQUEST),
    )
    action_charge = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    state_success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    CheckoutFactory(
        action=action_charge,
        content_object=object_payment_plan_instalment,
        state=state_success,
    )
    assert 1 == Checkout.objects.count()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse(
        "checkout.object.payment.plan.instalment.charge",
        args=[object_payment_plan_instalment.pk],
    )
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert (
        reverse(
            "checkout.object.payment.plan",
            args=[object_payment_plan_instalment.object_payment_plan.pk],
        )
        == response.url
    )
    assert Message.objects.first() is None
    assert 1 == Checkout.objects.count()
    qs = Checkout.objects.for_content_object(object_payment_plan_instalment)
    assert 1 == qs.count()
    response = client.get(response.url)
    assert "messages" in response.context
    messages = response.context["messages"]
    assert 1 == len(messages)
    for msg in messages:
        # only one message
        assert (
            "Instalment {} for code@pkimber.net has already "
            "been paid".format(object_payment_plan_instalment.pk)
        ) == msg.message


@pytest.mark.django_db
def test_charge_stripe_error(client):
    """The Stripe call will raise an exception - notify the user."""
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    sales_ledger = SalesLedgerFactory(contact=contact)
    CustomerFactory(email=sales_ledger.contact.email())
    object_payment_plan = ObjectPaymentPlanFactory(content_object=sales_ledger)
    obj = ObjectPaymentPlanInstalmentFactory(
        amount=Decimal("12.34"),
        count=2,
        object_payment_plan=object_payment_plan,
        state=CheckoutState.objects.get(slug=CheckoutState.REQUEST),
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse(
        "checkout.object.payment.plan.instalment.charge", args=[obj.pk]
    )
    with mock.patch("stripe.Customer.create"), mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="323", status="it-will-fail"
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert "/checkout/object/payment/plan/" in response.url
    assert (
        reverse(
            "checkout.object.payment.plan",
            args=[object_payment_plan.pk],
        )
        in response.url
    )

    assert Message.objects.first() is None
    qs = Checkout.objects.for_content_object(obj)
    assert 1 == qs.count()
    checkout = qs.first()
    assert Decimal("12.34") == checkout.total
    assert CheckoutAction.CHARGE == checkout.action.slug
    assert CheckoutState.FAIL == checkout.state.slug
    # check the message
    response = client.get(response.url)
    assert "messages" in response.context
    messages = response.context["messages"]
    assert 1 == len(messages)
    for msg in messages:
        # only one message
        assert "Could not charge code@pkimber.net." in msg.message


@pytest.mark.django_db
def test_charge_stripe_exception(client):
    """Instalment has already been paid - so don't allow payment again!"""
    sales_ledger = SalesLedgerFactory()
    CustomerFactory(email=sales_ledger.contact.user.email)
    x = ObjectPaymentPlanInstalmentFactory(
        amount=Decimal("12.34"),
        count=2,
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=sales_ledger
        ),
        state=CheckoutState.objects.get(slug=CheckoutState.SUCCESS),
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("checkout.object.payment.plan.instalment.charge", args=[x.pk])
    with pytest.raises(CheckoutError) as e:
        client.post(url)
    assert (
        "Cannot take payment from Stripe for instalment"
        "{} ('checkout_can_charge')".format(x.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_object_payment_plan_instalment_email(client):
    NotifyFactory(email="patrick@kbsoftware.co.uk")
    call_command("init_app_checkout")
    contact = ContactFactory(
        user=UserFactory(first_name="Patrick", last_name="Kimber")
    )
    contact_email = ContactEmailFactory(
        contact=contact, email="code@pkimber.net"
    )
    CustomerFactory(email=contact_email.email)
    content_object = SalesLedgerFactory(contact=contact)
    object_payment_plan = ObjectPaymentPlanFactory(
        content_object=content_object
    )
    x = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=object_payment_plan, deposit=True
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = {"id": "543"}
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        response = client.post(
            reverse(
                "checkout.object.payment.plan.instalment.email", args=[x.pk]
            )
        )
    assert HTTPStatus.FOUND == response.status_code
    assert (
        reverse("checkout.object.payment.plan", args=[object_payment_plan.pk])
        == response.url
    )
    # checkout
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    checkout = Checkout.objects.get(action=action)
    # email to customer
    qs = Message.objects.for_content_object(x)
    assert 1 == qs.count()
    message = qs.first()
    assert "On session payment" == message.subject
    assert "" == message.description
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert "code@pkimber.net" == mail.email
    url = urllib.parse.urljoin(
        settings.HOST_NAME,
        reverse("web.checkout.pay.on.session", args=[checkout.uuid]),
    )
    assert {
        "amount": "99.99",
        "description": "deposit",
        "name": "Patrick",
        "url": '<a href="{}" mc:disable-tracking>{}</a>'.format(url, url),
    } == {x.key: x.value for x in mail.mailfield_set.all()}


@pytest.mark.django_db
def test_object_payment_plan_instalment_paid(client):
    x = ObjectPaymentPlanInstalmentFactory(
        object_payment_plan=ObjectPaymentPlanFactory(
            content_object=SalesLedgerFactory()
        )
    )
    assert 0 == Checkout.objects.count()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("checkout.object.payment.plan.instalment.paid", args=[x.pk])
    )
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == Checkout.objects.count()
    checkout = Checkout.objects.first()
    assert checkout.state.is_success() is True
    assert user == checkout.user
    assert x == checkout.content_object


@pytest.mark.django_db
def test_pay_on_session_redirect(client):
    """Customer has an O/S payment due via pay on session.

    1. Check the customer is logged in.
    2. ? Check the instalment has not been paid.
       This may already be done by the ``web.checkout.pay.on.session`` view.
    3. Create a ``Checkout`` object for the instalment
    4. Redirect to ``web.checkout.pay.on.session``

    """
    call_command("init_app_checkout")
    contact = ContactFactory(
        user=UserFactory(username="Aaaa", email="a@pkimber.net")
    )
    contact_email = ContactEmailFactory(
        contact=contact, email="aaa@pkimber.net"
    )
    CustomerFactory(email=contact_email.email)
    sales_ledger = SalesLedgerFactory(contact=contact)
    object_payment_plan = ObjectPaymentPlanFactory(content_object=sales_ledger)
    instalment = ObjectPaymentPlanInstalmentFactory(
        count=2,
        due=date(2019, 5, 30),
        amount=Decimal("1"),
        object_payment_plan=object_payment_plan,
        state=CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION),
    )
    assert 0 == Checkout.objects.count()
    assert (
        client.login(username=contact.user.username, password=TEST_PASSWORD)
        is True
    )
    with mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="787", status="succeeded"
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        response = client.get(
            reverse(
                "checkout.object.payment.plan.instalment.pay.on.session",
                args=[instalment.pk],
            )
        )
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == Checkout.objects.count()
    checkout = Checkout.objects.first()
    assert (
        reverse("web.checkout.pay.on.session", args=[checkout.uuid])
        == response.url
    )


@pytest.mark.django_db
def test_pay_on_session_redirect_invalid_email(client, caplog):
    """Customer has an O/S payment due via pay on session.

    This test checks the email address of the logged in user matches the email
    address of the ``content_object``.

    https://www.kbsoftware.co.uk/crm/ticket/7108/

    """
    call_command("init_app_checkout")
    contact = ContactFactory(
        user=UserFactory(username="Aaaa", email="a@pkimber.net")
    )
    contact_email = ContactEmailFactory(
        contact=contact, email="aaa@pkimber.net"
    )
    CustomerFactory(email=contact_email.email)
    sales_ledger = SalesLedgerFactory(contact=contact)
    object_payment_plan = ObjectPaymentPlanFactory(content_object=sales_ledger)
    instalment = ObjectPaymentPlanInstalmentFactory(
        due=date(2019, 5, 30),
        amount=Decimal("1"),
        object_payment_plan=object_payment_plan,
        state=CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION),
    )
    assert 0 == Checkout.objects.count()
    user_b = UserFactory(username="Bbbb", email="b@pkimber.net")
    contact_b = ContactFactory(user=user_b)
    ContactEmailFactory(contact=contact_b, email="bbb@pkimber.net")
    assert (
        client.login(username=user_b.username, password=TEST_PASSWORD) is True
    )
    with mock.patch(
        "stripe.PaymentIntent.create"
    ) as mock_payment_intent, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_payment_intent.return_value = MockIntent(
            intent_id="787", status="succeeded"
        )
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
    with pytest.raises(CheckoutError) as e:
        client.get(
            reverse(
                "checkout.object.payment.plan.instalment.pay.on.session",
                args=[instalment.pk],
            )
        )
    expect = (
        "email address for logged in user (Bbbb) does not "
        "match email address for instalment (aaa@pkimber.net): "
        "['b@pkimber.net', 'bbb@pkimber.net']"
    )
    assert expect in str(e.value)
    assert 0 == Checkout.objects.count()
    assert expect in str(
        [str(x.msg) for x in caplog.records if x.levelno == logging.ERROR]
    )
