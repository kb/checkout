# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime
from decimal import Decimal
from django.contrib.auth.models import AnonymousUser
from freezegun import freeze_time

from checkout.models import Checkout, CheckoutAction
from checkout.tests.factories import CheckoutFactory, CheckoutAdditionalFactory
from contact.tests.factories import ContactEmailFactory, ContactFactory
from example_checkout.tests.factories import SalesLedgerFactory
from login.tests.fixture import UserFactory
from mail.models import Message
from mail.tests.factories import NotifyFactory
from stock.tests.factories import ProductFactory


@pytest.mark.parametrize("username", [None, "patrick"])
@pytest.mark.django_db
def test_create_checkout_invoice(username):
    if username is None:
        user = AnonymousUser()
    else:
        user = UserFactory(username=username, first_name="Orange")
    NotifyFactory(email="code@pkimber.net")
    action = CheckoutAction.objects.get(slug=CheckoutAction.INVOICE)
    # content_object
    contact = ContactFactory(user=UserFactory(first_name="Patrick"))
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    content_object = SalesLedgerFactory(
        contact=contact,
        product=ProductFactory(name="Apple", price=Decimal("5")),
        quantity=2,
    )
    with freeze_time(datetime(2019, 8, 20, 6, 1, 0, tzinfo=pytz.utc)):
        checkout = Checkout.objects.create_checkout_invoice(
            action,
            content_object,
            user,
            {
                "company_name": "KB",
                "address_1": "2 High Street",
                "email": "test@pkimber.net",
                "date_of_birth": date(1972, 3, 22),
            },
        )
    assert action == checkout.action
    assert checkout.state.is_success() is True
    if username is None:
        assert checkout.user is None
    else:
        assert user == checkout.user
    assert content_object == checkout.content_object
    assert Decimal("10") == checkout.total
    assert "Apple x 2 @ 5" == checkout.description
    assert ["KB", "2 High Street", "test@pkimber.net"] == list(
        checkout.invoice_data()
    )
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert "SUCCESS - Invoice from Patrick" == message.subject
    assert (
        "20/08/2019 06:01 - SUCCESS - Invoice from Patrick, "
        "code@pkimber.net:"
        "\n\nApple x 2 @ 5\n\n"
        "\n\nInvoice: KB, 2 High Street, test@pkimber.net"
        "\n\nDate of birth: 22/03/1972"
    ) == message.description


@pytest.mark.django_db
def test_invoice_data():
    action = CheckoutAction.objects.get(slug=CheckoutAction.INVOICE)
    checkout = CheckoutFactory(
        action=action, content_object=SalesLedgerFactory()
    )
    CheckoutAdditionalFactory(
        checkout=checkout, company_name="KB", email="test@pkimber.net"
    )
    assert ("KB", "test@pkimber.net") == tuple(checkout.invoice_data())


@pytest.mark.django_db
def test_invoice_data_does_not_exist():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.INVOICE),
        content_object=SalesLedgerFactory(),
    )
    assert [] == checkout.invoice_data()


@pytest.mark.django_db
def test_invoice_data_none():
    action = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    checkout = CheckoutFactory(
        action=action, content_object=SalesLedgerFactory()
    )
    assert () == tuple(checkout.invoice_data())


@pytest.mark.django_db
def test_previous_address_data_does_not_exist():
    checkout = CheckoutFactory(
        action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN),
        content_object=SalesLedgerFactory(),
    )
    assert [] == checkout.previous_address_data()
