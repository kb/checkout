# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command
from django.test import TestCase


@pytest.mark.django_db
def test_demo_data():
    call_command("demo-data-login")
    call_command("init_app_checkout")
    call_command("demo_data_checkout")


@pytest.mark.django_db
def test_send_on_session_payment_emails():
    call_command("send_on_session_payment_emails")
