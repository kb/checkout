# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings

from checkout.models import ObjectPaymentPlanInstalment


logger = logging.getLogger(__name__)


def can_login_and_pay(contact):
    return True


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def send_on_session_payment_emails():
    """Send on-session emails.

    .. note:: This is an example task.
              Your project needs to implement a copy of this code.

    For details, see 'send_on_session_payment_emails' in our docs:
    https://www.kbsoftware.co.uk/docs/app-checkout.html#tasks

    """
    logger.info("send_on_session_payment_emails")
    count = ObjectPaymentPlanInstalment.objects.send_on_session_payment_emails(
        can_login_and_pay
    )
    logger.info(
        "send_on_session_payment_emails - {} records- complete".format(count)
    )
    return count


def schedule_send_on_session_payment_emails():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    send_on_session_payment_emails.send()
