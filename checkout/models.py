# -*- encoding: utf-8 -*-
import logging
import urllib.parse
import uuid

from datetime import date
from dateutil.relativedelta import relativedelta
from dateutil.rrule import MONTHLY, rrule
from decimal import Decimal
from django.apps import apps
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from reversion import revisions as reversion

from base.model_utils import TimedCreateModifyDeleteModel, TimeStampedModel
from base.singleton import SingletonModel
from base.url_utils import url_with_querystring
from mail.models import Mail, Message, Notify
from mail.service import queue_mail_message, queue_mail_template
from mail.tasks import process_mail
from .service import StripeCheckout


CURRENCY = "GBP"
logger = logging.getLogger(__name__)


def default_checkout_state():
    return CheckoutState.objects.get(slug=CheckoutState.PENDING).pk


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


def system_generated_user():
    user_model = get_user_model()
    try:
        return user_model.objects.get(
            username=Checkout.SYSTEM_GENERATED_USER_NAME
        )
    except user_model.DoesNotExist:
        raise CheckoutError(
            "Cannot find '{}' user".format(Checkout.SYSTEM_GENERATED_USER_NAME)
        )


class CheckoutError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class CustomerDoesNotExistError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class CheckoutState(TimeStampedModel):
    CANCEL = "cancel"
    FAIL = "fail"
    # used for payment plans only.
    PAY_ON_SESSION = "pay_on_session"
    PENDING = "pending"
    # used for payment plans only.
    REQUEST = "request"
    SUCCESS = "success"

    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)

    class Meta:
        ordering = ("name",)
        verbose_name = "Checkout state"
        verbose_name_plural = "Checkout states"

    def __str__(self):
        return "{}".format(self.name)

    def is_cancel(self):
        return self.slug == self.CANCEL

    def is_fail(self):
        return self.slug == self.FAIL

    def is_pay_on_session(self):
        return self.slug == self.PAY_ON_SESSION

    def is_pending(self):
        return self.slug == self.PENDING

    def is_request(self):
        return self.slug == self.REQUEST

    def is_success(self):
        return self.slug == self.SUCCESS


reversion.register(CheckoutState)


class CheckoutAction(TimeStampedModel):
    CARD_REFRESH = "card_refresh"
    CHARGE = "charge"
    INVOICE = "invoice"
    MANUAL = "manual"
    PAY_ON_SESSION = "pay_on_session"
    PAYMENT = "payment"
    PAYMENT_PLAN = "payment_plan"
    PAYMENT_PLAN_OPTION_2 = "payment_plan_option_2"

    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    # What is this field used for?  Remove for now so we can find out.
    # payment = models.BooleanField()

    class Meta:
        ordering = ("name",)
        verbose_name = "Checkout action"
        verbose_name_plural = "Checkout action"

    def __str__(self):
        return "{}".format(self.name)

    def is_card_refresh(self):
        return self.slug == self.CARD_REFRESH

    def is_charge(self):
        return self.slug == self.CHARGE

    def is_invoice(self):
        return self.slug == self.INVOICE

    def is_manual(self):
        return self.slug == self.MANUAL

    def is_pay_on_session(self):
        return self.slug == self.PAY_ON_SESSION

    def is_payment(self):
        return self.slug == self.PAYMENT

    def is_payment_plan(self):
        return (
            self.slug == self.PAYMENT_PLAN
            or self.slug == self.PAYMENT_PLAN_OPTION_2
        )

    def is_payment_plan_option_2(self):
        return self.slug == self.PAYMENT_PLAN_OPTION_2


reversion.register(CheckoutAction)


class CustomerManager(models.Manager):
    def _create_customer(self, name, email, customer_id):
        x = self.model(name=name, email=email, customer_id=customer_id)
        x.save()
        return x

    def for_email(self, email):
        return self.model.objects.filter(email__iexact=email).order_by("-pk")

    def get_customer(self, email):
        """Get the Stripe customer from the email address.

        Note: email addresses are case-sensitive.  For more info, see
        https://code.djangoproject.com/ticket/17561.

        """
        if not email:
            raise CheckoutError(
                "Cannot get a customer without an email address"
            )
        result = self.for_email(email).first()
        if not result:
            raise CustomerDoesNotExistError(
                "Cannot find a customer record for '{}'".format(email)
            )
        return result

    def init_customer(self, name, email):
        """Initialise Stripe customer using name and email.

        1. Lookup existing customer record in the database.

           - Retrieve customer from Stripe and update the name.

        2. If the customer does not exist:

          - Create Stripe customer with name and email.
          - Create a customer record in the database.

        Return the customer and clear the card ``refresh`` flag.

        """
        try:
            customer = self.get_customer(email)
            customer.name = name
            customer.refresh = False
            customer.save()
            StripeCheckout().update_customer(customer.customer_id, name)
        except CustomerDoesNotExistError:
            customer_id = StripeCheckout().create_customer(name, email)
            customer = self._create_customer(name, email, customer_id)
        return customer

    def pks_with_email(self, email, object_pks):
        """Find the IDs of customers matching the email address.

        Keyword arguments:
        email -- find customer matching this email address
        object_pks -- only include customers from ``object_pks``

        """
        return (
            self.for_email(email)
            .filter(pk__in=object_pks)
            .values_list("pk", flat=True)
        )

    def update_card_expiry(self, email):
        """Find the customer, get the expiry date from Stripe and update."""
        try:
            customer = Customer.objects.get_customer(email)
            customer.update_card_expiry()
        except CustomerDoesNotExistError:
            logger.warning(
                "Cannot find a customer with email: {}".format(email)
            )


class Customer(TimeStampedModel):
    """Stripe Customer.

    Link the Stripe customer to an email address (and name).

    Note: It is expected that multiple users in our databases could have the
    same email address.  If they have different names, then this table looks
    very confusing.  Try checking the 'content_object' of the 'Checkout' model
    if you need to diagnose an issue.

    """

    MAIL_TEMPLATE_CARD_EXPIRY = "customer_card_expiry"
    MAIL_TEMPLATE_CARD_REFRESH_REQUEST = "contact_card_refresh_request"
    # 27/09/2019, Duplicate code...
    # see ``MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH``
    # MAIL_TEMPLATE_PAY_ON_SESSION = "customer_pay_on_session"
    MAIL_TEMPLATE_PAYMENT_FAILED = "customer_payment_failed"
    MAIL_TEMPLATE_REMINDER = "customer_instalment_reminder"

    name = models.TextField()
    email = models.EmailField(unique=True)
    customer_id = models.TextField()
    expiry_date = models.DateField(blank=True, null=True)
    refresh = models.BooleanField(
        default=False,
        help_text="Should the customer refresh their card details?",
    )
    objects = CustomerManager()

    class Meta:
        ordering = ("pk",)
        verbose_name = "Customer"
        verbose_name_plural = "Customers"

    def __str__(self):
        return "{} {}".format(self.email, self.customer_id)

    # @property
    # def checkout_can_charge(self):
    #    """We can always take a payment for this object!"""
    #    return True

    # @property
    # def checkout_description(self):
    #    result = "{}".format(self.name)
    #    return [result]

    # @property
    # def checkout_email(self):
    #    return self.email

    # def checkout_fail(self, checkout):
    #    pass

    # @property
    # def checkout_name(self):
    #    return "{}".format(self.name)

    # @property
    # def checkout_total(self):
    #    return Decimal()

    def _default_payment_method(self):
        result = (None, None)
        payment_methods = StripeCheckout().payment_methods(self.customer_id)
        if payment_methods:
            default_payment_method = None
            if len(payment_methods) > 1:
                stripe_customer = StripeCheckout().customer(self.customer_id)
                invoice_settings = stripe_customer["invoice_settings"]
                default_payment_method = invoice_settings[
                    "default_payment_method"
                ]
            created = 0
            for payment_method_id, card_detail in payment_methods.items():
                # if we find the default, this is the one we are looking for!
                if default_payment_method == payment_method_id:
                    result = (payment_method_id, card_detail)
                    break
                # find the most recent card
                if card_detail.created > created:
                    result = (payment_method_id, card_detail)
                    created = card_detail.created
        else:
            raise CheckoutError(
                "No payment methods found for customer {}, '{}'".format(
                    self.pk, self.customer_id
                )
            )
        return result

    def default_payment_method_id(self):
        payment_method_id, card_detail = self._default_payment_method()
        return payment_method_id

    def get_absolute_url(self):
        return url_with_querystring(
            reverse("checkout.customer"), email=self.email
        )

    def is_expiring(self):
        """Is the card expiring within the next month?

        If the ``expiry_date`` is ``None``, then it has *not* expired.

        The expiry date is set to the last day of the month e.g. for September
        2015, the ``expiry_date`` will be 30/09/2015.

        """
        result = False
        one_month = date.today() + relativedelta(months=+1)
        if self.expiry_date and self.expiry_date <= one_month:
            return True
        return result

    def name_or_first_name_of_user(self):
        """Get the first name of the customer (if it can be found)."""
        first_name = None
        try:
            user = get_user_model().objects.get(email=self.email)
            first_name = user.first_name
        except (
            get_user_model().DoesNotExist,
            get_user_model().MultipleObjectsReturned,
        ):
            pass
        return first_name or self.name

    def update_card_expiry(self):
        """Get the expiry date from Stripe and update the customer record.

        Reviewed for Stripe SCA, 31/03/2023

          If the client wants to put a URL in the template, then they should
          use a fully qualified URL, followed by ``/card/refresh/``.

        For more information, see the ticket:
        https://www.kbsoftware.co.uk/crm/ticket/6684/
        And the documentation in the site at ``docs-kb/source/checkout.rst``.

        """
        payment_method_id = None
        if self.customer_id:
            try:
                payment_method_id, card_detail = self._default_payment_method()
            except CheckoutError:
                pass
        if payment_method_id:
            if card_detail.expiry_year and card_detail.expiry_month:
                # last day of the month
                self.expiry_date = date(
                    card_detail.expiry_year,
                    card_detail.expiry_month,
                    1,
                ) + relativedelta(months=+1, day=1, days=-1)
                # is the card expiring soon?
                is_expiring = self.is_expiring()
                # has the customer already been asked to refresh their card?
                if self.refresh == is_expiring:
                    pass
                else:
                    name = self.name_or_first_name_of_user().title()
                    with transaction.atomic():
                        self.refresh = is_expiring
                        # save the details
                        self.save()
                        # email the customer
                        queue_mail_template(
                            self,
                            self.MAIL_TEMPLATE_CARD_EXPIRY,
                            {self.email: dict(name=name)},
                        )


reversion.register(Customer)


class CustomerPaymentManager(models.Manager):
    def pks_with_email(self, email, object_pks):
        """Find the IDs of customer payments matching the email address.

        Keyword arguments:
        email -- find customer payments matching this email address
        object_pks -- only include customer payments from ``object_pks``

        """
        return (
            self.model.objects.filter(customer__email__iexact=email)
            .filter(pk__in=object_pks)
            .values_list("pk", flat=True)
        )


class CustomerPayment(TimeStampedModel):
    """Take a payment from a customer.

    .. note:: We are not using this with SCA (yet).
              For details, see:
              https://www.kbsoftware.co.uk/docs/app-checkout.html#changelog

    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    description = models.CharField(max_length=100)
    total = models.DecimalField(max_digits=8, decimal_places=2)
    objects = CustomerPaymentManager()

    class Meta:
        ordering = ("pk",)
        verbose_name = "Customer payment"
        verbose_name_plural = "Customer payments"

    def __str__(self):
        return "{} {} {}".format(
            self.customer.email, self.description, self.total
        )

    def get_absolute_url(self):
        return url_with_querystring(
            reverse("checkout.customer"), email=self.customer.email
        )

    # def checkout_actions(self):
    #    return [CheckoutAction.PAYMENT]

    # @property
    # def checkout_can_charge(self):
    #    return bool(self.total)

    @property
    def checkout_description(self):
        result = "{}".format(self.description)
        return [result]

    @property
    def checkout_email(self):
        return self.customer.email

    def checkout_fail(self, checkout):
        pass

    # def checkout_fail_url(self, checkout_pk):
    #    return self.get_absolute_url()

    @property
    def checkout_first_name(self):
        return self.customer.name_or_first_name_of_user()

    @property
    def checkout_name(self):
        return "{}".format(self.customer.name)

    def checkout_success(self, checkout):
        pass

    # def checkout_success_url(self, checkout_pk):
    #    return self.get_absolute_url()

    @property
    def checkout_total(self):
        return self.total


reversion.register(CustomerPayment)


class PaymentPlanManager(models.Manager):
    def create_payment_plan(self, slug, name, deposit, count, interval):
        x = self.model(
            slug=slug,
            name=name,
            deposit=deposit,
            count=count,
            interval=interval,
        )
        x.save()
        return x

    def current(self):
        """List of payment plan headers excluding 'deleted'."""
        return self.model.objects.exclude(deleted=True)


class PaymentPlan(TimedCreateModifyDeleteModel):
    """Definition of a payment plan."""

    name = models.TextField()
    slug = models.SlugField(unique=True)
    deposit = models.IntegerField(help_text="Initial deposit as a percentage")
    count = models.IntegerField(help_text="Number of instalments")
    interval = models.IntegerField(help_text="Instalment interval in months")
    # deleted = models.BooleanField(default=False)
    objects = PaymentPlanManager()

    class Meta:
        ordering = ("slug",)
        verbose_name = "Payment plan"
        verbose_name_plural = "Payment plan"

    def __str__(self):
        return "{}".format(self.slug)

    def clean(self):
        if not self.count:
            raise ValidationError("Set at least one instalment.")

        if not self.deposit:
            raise ValidationError("Set an initial deposit.")

        if not self.interval:
            raise ValidationError(
                "Set the number of months between instalments."
            )

    def save(self, *args, **kwargs):
        if self.can_update():
            super().save(*args, **kwargs)
        else:
            raise CheckoutError("Payment plan in use.  Cannot be updated.")

    def can_update(self):
        """Only allow updates if there are no objects attached."""
        result = True
        if self._state.adding:
            pass
        else:
            count = ObjectPaymentPlan.objects.filter(payment_plan=self).count()
            if count:
                result = False
        return result

    def deposit_amount(self, total):
        return (total * (self.deposit / Decimal("100"))).quantize(
            Decimal(".01")
        )

    def instalments(self, deposit_date, total):
        """Calculate the instalment dates and values."""
        # deposit
        deposit = self.deposit_amount(total)
        # list of dates
        first_interval = self.interval
        if deposit_date.day > 15:
            first_interval = first_interval + 1
        start_date = deposit_date + relativedelta(months=+first_interval, day=1)
        instalment_dates = [
            d.date()
            for d in rrule(
                MONTHLY,
                count=self.count,
                dtstart=start_date,
                interval=self.interval,
            )
        ]
        # instalments
        instalment = ((total - deposit) / self.count).quantize(Decimal(".01"))
        # list of payment amounts
        values = []
        check = deposit
        for d in instalment_dates:
            value = instalment
            values.append(value)
            check = check + value
        # make the total match
        values[-1] = values[-1] + (total - check)
        return list(zip(instalment_dates, values))

    def example(self, deposit_date, total):
        result = [(deposit_date, self.deposit_amount(total))]
        return result + self.instalments(deposit_date, total)


reversion.register(PaymentPlan)


class CheckoutManager(models.Manager):
    def audit(self):
        return self.model.objects.all().order_by("-pk")

    def audit_content_type(self, content_type):
        return self.model.objects.filter(content_type=content_type).order_by(
            "-pk"
        )

    def create_checkout_invoice(self, action, content_object, user, add_data):
        """Create a checkout object for an invoice."""
        x = self.model(
            checkout_date=timezone.now(),
            action=action,
            content_object=content_object,
            description=", ".join(content_object.checkout_description),
            total=content_object.checkout_total,
        )
        # Allow for anonymous user
        if user.is_authenticated:
            x.user = user
        x.save()
        CheckoutAdditional.objects.create_checkout_additional(x, **add_data)
        # mark as successful
        x.state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
        x.save()
        content_object.checkout_success(x)
        x.notify()
        transaction.on_commit(lambda: process_mail.send())
        return x

    def create_checkout_manual(self, content_object, user):
        """Mark a transaction as paid (manual).

        You must be a member of staff to use this method.

        """
        content_object.refresh_from_db()
        if not user.is_staff:
            raise CheckoutError(
                "Only a member of staff can mark this transaction as paid: "
                "{}".format(str(content_object))
            )
        valid_state = (
            CheckoutState.FAIL,
            CheckoutState.PENDING,
            CheckoutState.REQUEST,
        )
        if content_object.state.slug not in valid_state:
            raise CheckoutError(
                "Cannot mark this transaction as paid. The current state "
                "is invalid ('{}')".format(content_object.state.slug)
            )
        action = CheckoutAction.objects.get(slug=CheckoutAction.MANUAL)
        checkout = self.model(
            checkout_date=timezone.now(),
            action=action,
            content_object=content_object,
            description=", ".join(content_object.checkout_description),
            total=content_object.checkout_total,
            user=user,
        )
        checkout.save()
        try:
            with transaction.atomic():
                checkout.payment_success()
        except CheckoutError:
            with transaction.atomic():
                checkout.payment_cancel()

    def create_checkout_payment_intent(self, content_object, user):
        """Create a checkout payment request.

        .. note:: This will **not** be used by a payment plan.

        """
        checkout = self.model(
            checkout_date=timezone.now(),
            action=CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT),
            content_object=content_object,
            description=", ".join(content_object.checkout_description),
            total=content_object.checkout_total,
        )
        # an anonymous user can create a checkout
        if user.is_authenticated:
            checkout.user = user
        checkout.save()
        checkout.create_and_save_payment_intent()
        return checkout

    def create_checkout_payment_intent_on_session(self, content_object, user):
        """Create a checkout payment request - on session.

        .. note:: This **will** be used by payment plans.

        """
        action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
        checkout = self.model(
            action=action,
            checkout_date=timezone.now(),
            content_object=content_object,
            description=", ".join(content_object.checkout_description),
            total=content_object.checkout_total,
        )
        if user.is_authenticated:
            checkout.user = user
        checkout.save()
        checkout.create_and_save_payment_intent_on_session()
        return checkout

    def create_checkout_setup_intent(
        self, content_object, user, payment_plan, action_slug=None
    ):
        """Create a checkout setup intent (payment plan) request."""
        if action_slug is None:
            action_slug = CheckoutAction.PAYMENT_PLAN
        try:
            action = CheckoutAction.objects.get(slug=action_slug)
        except CheckoutAction.DoesNotExist:
            raise CheckoutError(
                "'CheckoutAction' '{}' does not exist".format(action_slug)
            )
        checkout = self.model(
            action=action,
            checkout_date=timezone.now(),
            content_object=content_object,
            description=", ".join(content_object.checkout_description),
            payment_plan=payment_plan,
            total=content_object.checkout_total,
        )
        # an anonymous user can create a checkout
        if user.is_authenticated:
            checkout.user = user
        checkout.save()
        checkout.create_and_save_setup_intent()
        return checkout

    def decline_message(self, content_object, decline_filter=None):
        """Format the decline message for a content object.

        Keyword arguments:
        content_object -- most recent 'Checkout' object for the 'content_object'
        decline_filter -- only 'Checkout' objects which have been declined.

        """
        result = ""
        # checkout for the 'content_object'
        qs = self.model.objects.for_content_object(content_object)
        if decline_filter:
            q_decline = Q(decline_code__isnull=True) | Q(decline_code__exact="")
            q_message = Q(decline_message__isnull=True) | Q(
                decline_message__exact=""
            )
            qs = qs.exclude(q_decline & q_message)
        qs = qs.order_by("-pk")
        checkout = qs.first()
        if checkout and (checkout.decline_message or checkout.decline_code):
            result = "('{}' at {}) {}".format(
                checkout.decline_code,
                timezone.localtime(checkout.created).strftime("%d/%m/%Y %H:%M"),
                checkout.decline_message,
            )
        return result

    def for_content_object(self, content_object):
        content_type = ContentType.objects.get_for_model(content_object)
        return self.model.objects.filter(
            content_type=content_type, object_id=content_object.pk
        )

    def for_email(self, email):
        """Find all the checkout objects for an email address.

        This is complicated because we have to lookup content objects which may
        be in different database tables (Django models).

        .. tip:: Also see ``ObjectPaymentPlanManager.for_email``

        """
        # find the model classes which are linked to payment plans
        qs = (
            self.model.objects.all()
            .order_by("content_type")
            .distinct("content_type")
        )
        result = []
        # iterate through the model classes
        for x in qs:
            # find the pks of the models linked to a checkout
            object_pks = self.model.objects.filter(
                content_type=x.content_type
            ).values_list("object_id", flat=True)
            # get the model class
            model_class = x.content_type.model_class()
            # I hope this only happens when the 'content_type' no longer exists
            if model_class:
                # ask the model class for a list of pks matching by email
                object_pks = model_class.objects.pks_with_email(
                    email, object_pks
                )
                # find the checkout transactions linked to the content objects
                pks = self.model.objects.filter(
                    content_type=x.content_type, object_id__in=object_pks
                ).values_list("pk", flat=True)
                result.extend([x for x in pks])
        # retrieve the object payment plans for the matching content objects
        return self.model.objects.filter(pk__in=set(result)).order_by("-pk")

    def payment_intent_fulfillment(self):
        """Check the pending checkouts to see if they were successful.

        We allow 1440 minutes (24 hours) for the payment to complete.
        (for details, see https://www.kbsoftware.co.uk/crm/ticket/5521/)
        If the payment does not complete, then we *cancel* it.

        .. note:: This method is very similar to ``setup_intent_fulfillment``
                  and ``payment_intent_on_session_fulfillment`` (see below).

        """
        count = 0
        qs = self.pending(CheckoutAction.PAYMENT)
        for x in qs:
            count = count + 1
            intent = StripeCheckout().payment_intent(x.payment_intent_id)
            status = intent["status"]
            logger.info(
                "{}. {} ({}) - {}".format(
                    x.pk, x.action.slug, x.payment_intent_id, status
                )
            )
            if status == "succeeded":
                with transaction.atomic():
                    x.payment_success()
                    x.notify()
                    transaction.on_commit(lambda: process_mail.send())
            elif x.age_in_minutes() > 1440:
                with transaction.atomic():
                    x.payment_cancel()
        return count

    def payment_intent_on_session_fulfillment(self):
        """Check the pending checkouts to see if they were successful.

        We don't have a time limit for an on session payment because we are
        waiting for the customer to make the payment.

        1. If the 'content_object' has been paid then we can mark the checkout
           as failed ('success_for_content_object').
        2. Check the status of the payment intent.  Has it been canceled?
           https://stripe.com/docs/api/setup_intents/object#setup_intent_object-status

        .. note:: This method is very similar to ``payment_intent_fulfillment``
                  and ``setup_intent_fulfillment``.

        """
        count = 0
        qs = self.pending(CheckoutAction.PAY_ON_SESSION)
        for x in qs:
            count = count + 1
            cancel = False
            intent = StripeCheckout().payment_intent(x.payment_intent_id)
            status = intent["status"]
            logger.info(
                "{}. {} ({}) - {}".format(
                    x.pk, x.action.slug, x.payment_intent_id, status
                )
            )
            if status == "succeeded":
                with transaction.atomic():
                    # do not try and 'attach_payment_method_to_customer'
                    x.payment_success()
                    x.notify()
                    transaction.on_commit(lambda: process_mail.send())
            elif status == "canceled":
                cancel = True
            else:
                # 'status' is something else e.g. 'requires_payment_method'
                qs = self.success_for_content_object(x.content_object)
                if qs.exists():
                    # 'content_object' has been paid, so cancel this checkout
                    cancel = True
            if cancel:
                with transaction.atomic():
                    x.payment_cancel()
        return count

    def pending(self, action_slug=None):
        qs = self.model.objects.filter(state__slug=CheckoutState.PENDING)
        if action_slug:
            if isinstance(action_slug, (list, tuple)):
                qs = qs.filter(action__slug__in=action_slug)
            else:
                qs = qs.filter(action__slug=action_slug)
        return qs

    def setup_intent_fulfillment(self):
        """Check the pending checkouts to see if they were successful.

        We allow 1440 minutes (24 hours) for the setup intent to complete.
        (for details, see https://www.kbsoftware.co.uk/crm/ticket/5521/
        and https://www.kbsoftware.co.uk/crm/ticket/6351/)

        .. note:: This method is very similar to ``payment_intent_fulfillment``
                  and ``payment_intent_on_session_fulfillment`` (see above).

        """
        count = 0
        qs = self.pending(
            [
                CheckoutAction.PAYMENT_PLAN,
                CheckoutAction.PAYMENT_PLAN_OPTION_2,
                CheckoutAction.CARD_REFRESH,
            ]
        )
        for x in qs:
            count = count + 1
            intent = StripeCheckout().setup_intent(x.setup_intent_id)
            status = intent["status"]
            logger.info(
                "{}. {} ({}) {}".format(
                    x.pk, x.action.slug, x.setup_intent_id, status
                )
            )
            if status == "succeeded":
                with transaction.atomic():
                    x.attach_payment_method_to_customer(intent)
                    x.payment_success()
                    x.notify()
                    transaction.on_commit(lambda: process_mail.send())
            elif x.age_in_minutes() > 1440:
                with transaction.atomic():
                    x.payment_cancel()
        return count

    def success(self):
        return self.audit().filter(state__slug=CheckoutState.SUCCESS)

    def success_for_content_object(self, content_object):
        """Has there been a successful payment for the content object?"""
        qs = self.model.objects.for_content_object(content_object)
        success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
        return qs.filter(state=success)


class Checkout(TimeStampedModel):
    """Checkout.

    Create a 'Checkout' instance when you want to interact with Stripe e.g.
    take a payment, get card details to set-up a payment plan or refresh the
    details of an expired card.

    The ``payment_intent_id`` is for use with the Stripe Payment Intents API:
    https://stripe.com/docs/payments/payment-intents

    We use a *universally unique identifier* for the ``id``.  For details,
    https://docs.djangoproject.com/en/2.2/ref/models/fields/#uuidfield

    """

    AUTHENTICATION_REQUIRED = "authentication_required"
    EXPIRED_CARD = "expired_card"
    SYSTEM_GENERATED_USER_NAME = "system.generated"

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    checkout_date = models.DateTimeField()
    action = models.ForeignKey(CheckoutAction, on_delete=models.CASCADE)
    customer = models.ForeignKey(
        Customer, blank=True, null=True, on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text=(
            "User who created the checkout request "
            "(or blank if the the user is not logged in)"
        ),
    )
    payment_intent_id = models.TextField(blank=True)
    setup_intent_id = models.TextField(blank=True)
    state = models.ForeignKey(
        CheckoutState, default=default_checkout_state, on_delete=models.CASCADE
    )
    state_updated = models.DateTimeField(blank=True, null=True)
    description = models.TextField()
    total = models.DecimalField(
        max_digits=8, decimal_places=2, blank=True, null=True
    )
    payment_plan = models.ForeignKey(
        PaymentPlan, blank=True, null=True, on_delete=models.CASCADE
    )
    # status
    decline_code = models.TextField(blank=True)
    decline_message = models.TextField(blank=True)
    # link to the object in the system which requested the checkout
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
    objects = CheckoutManager()

    class Meta:
        ordering = ("pk",)
        verbose_name = "Checkout"
        verbose_name_plural = "Checkouts"

    def __str__(self):
        try:
            txt = self.content_object.checkout_email
        except AttributeError:
            txt = self.pk
        return "{}".format(txt)

    def age_in_minutes(self):
        return int((timezone.now() - self.created).total_seconds() / 60)

    def attach_payment_method_to_customer(self, setup_intent):
        if self.customer:
            raise CheckoutError(
                "Checkout {} already has a customer".format(self.pk)
            )
        if self.action.is_payment_plan() or self.action.is_card_refresh():
            self.customer = Customer.objects.init_customer(
                self.content_object.checkout_name,
                self.content_object.checkout_email,
            )
            stripe_checkout = StripeCheckout()
            payment_method_id = (
                stripe_checkout.attach_payment_method_to_customer(
                    setup_intent, self.customer.customer_id
                )
            )
            stripe_checkout.update_customer_default_payment_method(
                self.customer.customer_id, payment_method_id
            )
        else:
            raise CheckoutError(
                "Payment methods can only be attached to a payment "
                "plan: {} ('{}')".format(self.pk, self.action.slug)
            )

    def client_secret_payment_intent(self):
        if self.action.is_payment() or self.action.is_pay_on_session():
            intent = StripeCheckout().payment_intent(self.payment_intent_id)
            return intent.client_secret
        else:
            raise CheckoutError(
                "Can only get secrets for payments (checkout {}, "
                "action '{}')".format(self.pk, self.action.slug)
            )

    def client_secret_setup_intent(self):
        if self.action.is_payment_plan() or self.action.is_card_refresh():
            intent = StripeCheckout().setup_intent(self.setup_intent_id)
            return intent.client_secret
        else:
            raise CheckoutError(
                "Can only get secrets for payment plans and card refresh "
                "(checkout {}, action '{}')".format(self.pk, self.action.slug)
            )

    def content_object_url(self):
        return self.content_object.get_absolute_url()

    def create_and_save_payment_intent_on_session(self):
        customer = Customer.objects.get_customer(
            self.content_object.checkout_email
        )
        payment_method_id = customer.default_payment_method_id()
        if self.action.is_pay_on_session():
            intent_id = StripeCheckout().create_payment_intent_on_session(
                customer.customer_id, payment_method_id, self.total
            )
            self.payment_intent_id = intent_id
            self.customer = customer
            self.save()
        else:
            raise CheckoutError(
                "Can only create an on-session payment intent for on-session "
                "payments. Checkout {}, action '{}'".format(
                    self.pk, self.action.slug
                )
            )

    def create_and_save_payment_intent(self):
        if self.action.is_payment():
            self.payment_intent_id = StripeCheckout().create_payment_intent(
                self.total
            )
            self.save()
        else:
            raise CheckoutError(
                "Can only create a payment intent for payments. "
                "Checkout {}, action '{}'".format(self.pk, self.action.slug)
            )

    def create_and_save_setup_intent(self):
        if self.action.is_payment_plan() or self.action.is_card_refresh():
            self.setup_intent_id = StripeCheckout().create_setup_intent()
            self.save()
        else:
            raise CheckoutError(
                "Can only create a setup intent for payment plans "
                "(or card refresh). Checkout {}, action '{}'".format(
                    self.pk, self.action.slug
                )
            )

    def date_of_birth(self):
        try:
            data = self.checkoutadditional
            return data.date_of_birth
        except CheckoutAdditional.DoesNotExist:
            pass
        return None

    def invoice_data(self):
        try:
            if self.action.is_invoice():
                data = self.checkoutadditional
                return filter(
                    None,
                    (
                        data.contact_name,
                        data.company_name,
                        data.address_1,
                        data.address_2,
                        data.address_3,
                        data.town,
                        data.county,
                        data.postcode,
                        data.country,
                        data.email,
                        data.phone,
                    ),
                )
        except CheckoutAdditional.DoesNotExist:
            pass
        return []

    def notify(self, request=None):
        """Send notification of checkout status.

        Pass in a 'request' if you want the email to contain the URL of the
        checkout transaction.

        """
        email_addresses = [n.email for n in Notify.objects.all()]
        if email_addresses:
            caption = self.action.name
            state = self.state.name
            subject = "{} - {} from {}".format(
                state.upper(),
                caption.capitalize(),
                self.content_object.checkout_name,
            )
            message = "{} - {} - {} from {}, {}:".format(
                self.created.strftime("%d/%m/%Y %H:%M"),
                state.upper(),
                caption,
                self.content_object.checkout_name,
                self.content_object.checkout_email,
            )
            message = message + "\n\n{}\n\n{}".format(
                self.description,
                (
                    request.build_absolute_uri(self.content_object_url())
                    if request
                    else ""
                ),
            )
            if self.invoice_data():
                message = message + "\n\nInvoice: {}".format(
                    ", ".join(self.invoice_data())
                )
            if self.previous_address_data():
                message = message + "\n\nPrevious address: {}".format(
                    ", ".join(self.previous_address_data())
                )
            if self.date_of_birth():
                dob = self.date_of_birth().strftime("%d/%m/%Y")
                message = message + "\n\nDate of birth: {}".format(dob)
            queue_mail_message(self, email_addresses, subject, message)
        else:
            logger.error(
                "Cannot send email notification of checkout transaction.  "
                "No email addresses set-up in 'enquiry.models.Notify'"
            )

    def payment_cancel(self):
        """Checkout cancelled.

        Cancel does not update the ``content_object`` it just makes the content
        object go away.

        06/10/2020, I am not sure what I mean by the sentence above!

        """
        self.state = CheckoutState.objects.get(slug=CheckoutState.CANCEL)
        self.state_updated = timezone.now()
        self.save()

    def payment_fail(self):
        """Checkout failed - so update and notify admin."""
        self.state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
        self.state_updated = timezone.now()
        self.save()
        return self.content_object.checkout_fail(self)

    def payment_plan_example(self):
        if self.action.is_payment_plan():
            return self.payment_plan.example(
                date.today(), self.content_object.checkout_total
            )
        else:
            raise CheckoutError(
                "Cannot get an example payment plan for the '{}' "
                "action: {}".format(self.action.slug, self.pk)
            )

    def payment_success(self):
        """Checkout successful.

        This method is called from within a transaction.
        See ``checkout.success`` in the model manager.

        """
        self.state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
        self.state_updated = timezone.now()
        self.save()
        return self.content_object.checkout_success(self)

    def previous_address_data(self):
        try:
            if self.action.is_payment_plan():
                data = self.checkoutadditional
                return filter(
                    None,
                    (
                        data.address_1,
                        data.address_2,
                        data.address_3,
                        data.town,
                        data.county,
                        data.postcode,
                        data.country,
                    ),
                )
        except CheckoutAdditional.DoesNotExist:
            pass
        return []


reversion.register(Checkout)


class CheckoutAdditionalManager(models.Manager):
    def create_checkout_additional(self, checkout, **kwargs):
        obj = self.model(checkout=checkout, **kwargs)
        obj.save()
        return obj


class CheckoutAdditional(TimeStampedModel):
    """If a user decides to pay by invoice, there are the details.

    Links with the ``CheckoutForm`` in ``checkout/forms.py``.
    Probably easier to put validation in the form if required.

    """

    checkout = models.OneToOneField(Checkout, on_delete=models.CASCADE)
    # company
    company_name = models.CharField(max_length=100, blank=True)
    address_1 = models.CharField("Address", max_length=100, blank=True)
    address_2 = models.CharField("", max_length=100, blank=True)
    address_3 = models.CharField("", max_length=100, blank=True)
    town = models.CharField(max_length=100, blank=True)
    county = models.CharField(max_length=100, blank=True)
    postcode = models.CharField(max_length=20, blank=True)
    country = models.CharField(max_length=100, blank=True)
    # contact
    contact_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(blank=False)
    phone = models.CharField(max_length=50, blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    objects = CheckoutAdditionalManager()

    class Meta:
        ordering = ("email",)
        verbose_name = "Checkout Additional Information"
        verbose_name_plural = "Checkout Additional Information"

    def __str__(self):
        return "{}".format(self.email)


reversion.register(CheckoutAdditional)


class ObjectPaymentPlanManager(models.Manager):
    def _payment_plans_by_instalment_state(self, states):
        values = ObjectPaymentPlanInstalment.objects.filter(
            state__slug__in=states
        ).values_list("object_payment_plan__pk", flat=True)
        # 'set' will remove duplicate 'values'
        return self.model.objects.filter(pk__in=(set(values))).exclude(
            deleted=True
        )

    def create_object_payment_plan(self, content_object, payment_plan, total):
        """Create a payment plan for an object with the initial deposit record.

        This method must be called from within a transaction.

        """
        x = self.model(
            content_object=content_object,
            payment_plan=payment_plan,
            total=total,
        )
        x.save()
        ObjectPaymentPlanInstalment.objects.create_object_payment_plan_instalment(
            x, 1, True, payment_plan.deposit_amount(total), date.today()
        )
        return x

    def fail_or_request(self):
        return self._payment_plans_by_instalment_state(
            (CheckoutState.FAIL, CheckoutState.REQUEST)
        )

    def for_content_object(self, obj):
        """Return the ``ObjectPaymentPlan`` instance for ``obj``.

        .. note:: This method does a ``get``... not a ``filter``!

        """
        return self.model.objects.get(
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.pk,
        )

    def for_email(self, email):
        """Find all the payment plans for an email address.

        This is complicated because we have to lookup content objects which may
        be in different database tables (Django models).

        .. tip:: Also see ``CheckoutManager.for_email``

        """
        # find the model classes which are linked to payment plans
        qs = (
            self.model.objects.all()
            .order_by("content_type")
            .distinct("content_type")
        )
        result = []
        # iterate through the model classes
        for x in qs:
            # find the pks of the models linked to a payment plan
            object_pks = self.model.objects.filter(
                content_type=x.content_type
            ).values_list("object_id", flat=True)
            # print()
            # print("1. {}".format(object_pks))
            # get the model class
            model_class = x.content_type.model_class()
            # print("2. {}".format(model_class))
            # ask the model class for a list of pks matching by email
            object_pks = model_class.objects.pks_with_email(email, object_pks)
            # find the payment plans linked to the content objects
            pks = self.model.objects.filter(
                content_type=x.content_type, object_id__in=object_pks
            ).values_list("pk", flat=True)
            result.extend([x for x in pks])
        # retrieve the object payment plans for the matching content objects
        return self.model.objects.filter(pk__in=set(result))

    def outstanding_payment_plans(self):
        """List of outstanding payment plans.

        Used to refresh card expiry dates.

        """
        return self._payment_plans_by_instalment_state(
            (CheckoutState.FAIL, CheckoutState.PENDING, CheckoutState.REQUEST)
        )

    def pks_with_email(self, email, object_pks):
        """Find the IDs of payment plans matching the email address.

        Keyword arguments:
        email -- find payment plans matching this email address
        object_pks -- only include payment plans from ``object_pks``

        .. tip:: This class is ``ObjectPaymentPlanManager``.

        """
        # payment plans matching the email address
        payment_plan_pk_qs = self.for_email(email).values_list("pk", flat=True)
        # intersection of matching payment plans and the ``object_pks``
        pks = set(object_pks) & set([x for x in payment_plan_pk_qs])
        return self.model.objects.filter(pk__in=pks).values_list(
            "pk", flat=True
        )

    def refresh_card_expiry_dates(self):
        """Refresh the card expiry dates for outstanding payment plans."""
        for plan in self.outstanding_payment_plans():
            Customer.objects.update_card_expiry(
                plan.content_object.checkout_email
            )


class ObjectPaymentPlan(TimedCreateModifyDeleteModel):
    """Payment plan for an object."""

    # link to the object in the system which requested the payment plan
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
    payment_plan = models.ForeignKey(PaymentPlan, on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=8, decimal_places=2)
    # is this object deleted?
    # deleted = models.BooleanField(default=False)
    objects = ObjectPaymentPlanManager()

    class Meta:
        ordering = ("created",)
        unique_together = ("object_id", "content_type")
        verbose_name = "Object payment plan"
        verbose_name_plural = "Object payment plans"

    def __str__(self):
        return "{} created {}".format(self.payment_plan.name, self.created)

    def _check_create_instalments(self):
        """Check the current records to make sure we can create instalments."""
        instalments = self.objectpaymentplaninstalment_set.all()
        count = instalments.count()
        if not count:
            # a payment plan should always have a deposit record
            raise CheckoutError(
                "no deposit/instalment record set-up for "
                "payment plan: '{}'".format(self.pk)
            )

        if count == 1:
            # check the first payment is the deposit
            first_instalment = instalments.first()
            if not first_instalment.deposit:
                raise CheckoutError(
                    "no deposit record for "
                    "payment plan: '{}'".format(self.pk)
                )

        else:
            # cannot create instalments if already created!
            raise CheckoutError(
                "instalments already created for this "
                "payment plan: '{}'".format(self.pk)
            )

    def _outstanding_instalments(self):
        """Outstanding instalments for this payment plan."""
        success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
        return self.objectpaymentplaninstalment_set.exclude(state=success)

    def charge_deposit(self, user):
        """Charge the deposit."""
        self._check_create_instalments()
        deposit = self.objectpaymentplaninstalment_set.first()
        return deposit.charge_stripe(user)

    @property
    def checkout_description(self):
        """Used for card refresh."""
        result = []
        result.append(
            "{} payments remaining".format(
                self._outstanding_instalments().count(),
            )
        )
        return result

    @property
    def checkout_email(self):
        return self.content_object.checkout_email

    def checkout_fail(self, checkout):
        """This method is used to refresh the card - so no action required."""
        pass

    def checkout_success(self, checkout):
        """This method is used to refresh the card - so no action required."""
        pass

    @property
    def checkout_name(self):
        return self.content_object.checkout_name

    @property
    def checkout_total(self):
        return self.total_outstanding()

    def create_instalments(self):
        """The deposit has been paid, so create the instalments."""
        self._check_create_instalments()
        instalments = self.payment_plan.instalments(
            timezone.now().date(), self.total
        )
        count = 1
        for due, amount in instalments:
            count = count + 1
            ObjectPaymentPlanInstalment.objects.create_object_payment_plan_instalment(
                self, count, False, amount, due
            )

    def deposit_instalment(self):
        return self.objectpaymentplaninstalment_set.get(deposit=True)

    def get_absolute_url(self):
        return reverse("checkout.object.payment.plan", args=[self.pk])

    def instalment_count(self):
        """Number of instalments left to pay (excluding the deposit)."""
        return self.objectpaymentplaninstalment_set.exclude(
            deposit=True
        ).count()

    def instalments_due(self, due_date=None):
        """Check if previous instalments are due.

        .. note:: The ``due_date`` parameter will exclude instalments due
                  *before* this date.

        """
        if due_date is None:
            due_date = date.today()
        qs = self.objectpaymentplaninstalment_set.filter(
            due__lt=due_date
        ).exclude(state__slug=CheckoutState.SUCCESS)
        return qs

    def is_deposit_paid(self):
        deposit_instalment = self.deposit_instalment()
        result = deposit_instalment.state.is_success()
        if result:
            if self.instalment_count():
                # if the deposit is paid, then the instalments should have been
                # created...
                pass
            else:
                raise CheckoutError(
                    "The deposit for payment plan '{}' has been paid, but the "
                    "instalments were not created".format(self.pk)
                )
        return result

    def payment_count(self):
        return self.objectpaymentplaninstalment_set.count()

    def payments(self):
        return self.objectpaymentplaninstalment_set.all().order_by("count")

    def total_outstanding(self):
        """Total amount outstanding for this payment plan."""
        success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
        result = self._outstanding_instalments().aggregate(
            total=models.Sum("amount")
        )
        return result["total"] or Decimal()


reversion.register(ObjectPaymentPlan)


class ObjectPaymentPlanInstalmentManager(models.Manager):
    def _due(self, date_gt, date_lt):
        return self._due_ignore_retry_count(date_gt, date_lt).exclude(
            retry_count__gt=self.model.RETRY_COUNT
        )

    def _due_ignore_retry_count(self, date_gt, date_lt):
        return (
            self.model.objects.filter(
                due__gte=date_gt,
                due__lte=date_lt,
                state__slug__in=(CheckoutState.FAIL, CheckoutState.PENDING),
            )
            .exclude(deposit=True)
            .exclude(object_payment_plan__deleted=True)
        )

    def _user_has_active_account(self, email, can_login_and_pay):
        """Does the user have an account and did they log in within 90 days?

        Keyword arguments:
        email -- email address of the user
        can_login_and_pay -- a function which can be used to check if the user
                             can login and get access to a payment screen.

        """

        result = False
        qs = get_contact_model().objects.matching_email(email)
        for contact in qs:
            is_active = (
                contact.user.is_active
                and contact.user.last_login
                and can_login_and_pay(contact)
            )
            if is_active:
                diff = timezone.now() - contact.user.last_login
                if diff.days < 90:
                    result = True
                    break
        return result

    def create_object_payment_plan_instalment(
        self, object_payment_plan, count, deposit, amount, due
    ):
        x = self.model(
            object_payment_plan=object_payment_plan,
            count=count,
            deposit=deposit,
            amount=amount,
            due=due,
        )
        x.save()
        return x

    def due(self):
        """Lock the records while we try and take the payment.

        TODO Do we need to check that a payment is not already linked to this
        record?

        """
        date_lt = date.today()
        date_gt = date_lt + relativedelta(
            days=-ObjectPaymentPlanInstalment.RETRY_COUNT
        )
        return self._due(date_gt, date_lt)

    def failed(self):
        """List of failed instalments.

        Send an email to students whose monthly instalments fail:
        https://www.kbsoftware.co.uk/crm/ticket/4401/

        """
        date_lt = date.today()
        # look for failed payments for 10 days
        days_to_check = ObjectPaymentPlanInstalment.RETRY_COUNT + 10
        date_gt = date_lt + relativedelta(days=-days_to_check)
        return self._due_ignore_retry_count(date_gt, date_lt)

    def failure_emails(self):
        """

        1. Which failure emails are due.
        2. Remove email addresses which have already been sent a reminder.

        """
        result = {}
        for instalment in self.failed().order_by("-due"):
            result[instalment.checkout_email] = instalment.pk
        # check to see if we already sent a reminder for this email address
        date_lt = timezone.now()
        date_gt = date_lt + relativedelta(
            days=-ObjectPaymentPlanInstalment.REMINDER_CHECK
        )
        previous_reminders = Mail.objects.emails_for_template(
            date_gt, date_lt, Customer.MAIL_TEMPLATE_PAYMENT_FAILED
        )
        # remove email addresses which have already been sent a reminder
        for email in previous_reminders:
            result.pop(email, None)
        return result

    def pay_on_session(self):
        """List of instalments which are *pay-on-session*.

        .. note:: We are using ``CheckoutState``, but this is the ``state`` of
                  the instalment.

        """
        return self.model.objects.filter(
            state__slug=CheckoutState.PAY_ON_SESSION
        ).exclude(object_payment_plan__deleted=True)

    def pay_on_session_emails(self):
        result = {}
        for instalment in self.pay_on_session().order_by("-due"):
            result[instalment.checkout_email] = instalment.pk
        # check to see if we already sent a reminder for this email address
        date_lt = timezone.now()
        date_gt = date_lt + relativedelta(months=-1)
        # previous_emails_auth = Mail.objects.emails_for_template(
        #    date_gt, date_lt, self.model.MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH
        # )
        previous_emails_anon = Mail.objects.emails_for_template(
            date_gt, date_lt, self.model.MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON
        )
        # remove email addresses which have already been sent a notification
        # for email in previous_emails_auth.union(previous_emails_anon):
        for email in previous_emails_anon:
            result.pop(email, None)
        return result

    def pay_on_session_for_email(self, email):
        """On-session instalment payments due for an email address.

        This method is used on customer dashboards to display outstanding
        on-session payments.

        .. note:: We are using ``CheckoutState``, but this is the ``state`` of
                  the instalment.

        """
        object_payment_plan_pks = [
            x.pk for x in ObjectPaymentPlan.objects.for_email(email)
        ]
        return ObjectPaymentPlanInstalment.objects.filter(
            object_payment_plan__pk__in=object_payment_plan_pks
        ).filter(state__slug=CheckoutState.PAY_ON_SESSION)

    def pks_with_email(self, email, object_pks):
        """Find the IDs of instalments matching the email address.

        Keyword arguments:
        email -- find instalments matching this email address
        object_pks -- only include instalments from ``object_pks``

        .. tip:: This class is ``ObjectPaymentPlanInstalmentManager``.

        """
        # payment plans matching the email address
        payment_plan_pk_qs = ObjectPaymentPlan.objects.for_email(
            email
        ).values_list("pk", flat=True)
        # object payment plans matching the email address
        qs = self.model.objects.filter(
            object_payment_plan__pk__in=[x for x in payment_plan_pk_qs]
        ).values_list("pk", flat=True)
        # intersection of matching payment plans and the ``object_pks``
        pks = set(object_pks) & set([x for x in qs])
        return self.model.objects.filter(pk__in=pks).values_list(
            "pk", flat=True
        )

    def reminder(self):
        """List of instalments which will be due soon.

        This information will be used to send a reminder email to the customer
        so they can make sure they have money in their account.

        """
        date_gt = date.today()
        date_lt = date_gt + relativedelta(
            days=+ObjectPaymentPlanInstalment.REMINDER_DAYS
        )
        return self._due(date_gt, date_lt)

    def reminder_emails(self):
        """

        1. Which reminder emails are due.
        2. Remove email addresses which have already been sent a reminder.

        """
        result = {}
        for instalment in self.reminder().order_by("-due"):
            result[instalment.checkout_email] = instalment.pk
        # check to see if we already sent a reminder for this email address
        date_lt = timezone.now()
        date_gt = date_lt + relativedelta(
            days=-ObjectPaymentPlanInstalment.REMINDER_CHECK
        )
        previous_reminders = Mail.objects.emails_for_template(
            date_gt, date_lt, Customer.MAIL_TEMPLATE_REMINDER
        )
        # remove email addresses which have already been sent a reminder
        for email in previous_reminders:
            result.pop(email, None)
        return result

    def send_failure_emails(self):
        count = 0
        for email, instalment_pk in self.failure_emails().items():
            instalment = self.model.objects.get(pk=instalment_pk)
            context = {email: {"name": instalment.checkout_first_name.title()}}
            queue_mail_template(
                instalment, Customer.MAIL_TEMPLATE_PAYMENT_FAILED, context
            )
            count = count + 1
        # email status to notify list
        email_addresses = [n.email for n in Notify.objects.all()]
        if email_addresses:
            if count:
                # Use the first notify email as the content object
                queue_mail_message(
                    Notify.objects.first(),
                    email_addresses,
                    "Payment run - failure emails",
                    "Sent {} payment run failure emails".format(count),
                )
        else:
            logger.error(
                "Cannot send email notification of failure emails.  "
                "No email addresses set-up in 'enquiry.models.Notify'"
            )
        if count:
            transaction.on_commit(lambda: process_mail.send())
        return count

    def send_on_session_payment_emails(self, can_login_and_pay):
        """Email the user if Stripe requests an on on-session payment.

        Keyword arguments:
        can_login_and_pay -- a function which can be used to check if the user
                             can login and get access to a payment screen.

        - If the instalment is a deposit, then the user may not be
          authenticated, so send a URL.
        - If this is an instalment following the deposit, then use an email
          template (which should instruct the user to log into their account
          and pay).

        .. tip:: Look out for user accounts with duplicate email addresses.
                 The payment system works on email addresses, but your user may
                 have more than one user account.

        Just to note... in September 2019, I added (and then removed) similar
        code to ``checkout_fail`` in ``ObjectPaymentPlanInstalment``.

        .. tip:: This is in the ``ObjectPaymentPlanInstalmentManager`` class.

        """
        count = 0
        with transaction.atomic():
            for email, instalment_pk in self.pay_on_session_emails().items():
                instalment = self.model.objects.get(pk=instalment_pk)
                is_auth = self._user_has_active_account(
                    email, can_login_and_pay
                )
                if instalment.create_pay_on_session_email(
                    AnonymousUser(), is_auth
                ):
                    count = count + 1
            # email status to notify list
            email_addresses = [n.email for n in Notify.objects.all()]
            if email_addresses:
                if count:
                    # Use the first notify email as the content object
                    queue_mail_message(
                        Notify.objects.first(),
                        email_addresses,
                        "On session payment emails",
                        "Sent {} on session payment emails".format(count),
                    )
            else:
                logger.error(
                    "Cannot send email notification of on session payments. "
                    "No email addresses set-up in 'enquiry.models.Notify'"
                )
            if count:
                transaction.on_commit(lambda: process_mail.send())
        return count

    def send_payment_reminder_emails(self):
        count = 0
        for email, instalment_pk in self.reminder_emails().items():
            instalment = self.model.objects.get(pk=instalment_pk)
            context = {
                email: {
                    "due": instalment.due.strftime("%a %d %b %Y"),
                    "name": instalment.checkout_first_name.title(),
                }
            }
            queue_mail_template(
                instalment, Customer.MAIL_TEMPLATE_REMINDER, context
            )
            count = count + 1
        process_mail.send()
        return count


class ObjectPaymentPlanInstalment(TimeStampedModel):
    """Payments due for an object.

    The deposit record gets created first.  It has the ``deposit`` field set to
    ``True``.

    The instalment records are created after the deposit has been collected.
    Instalment records have the ``deposit`` field set to ``False``.

    """

    MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON = (
        "payment_plan_on_session_authorise_anon"
    )
    # MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH = (
    #     "payment_plan_on_session_authorise_auth"
    # )
    #
    # 18/12/2020, Deleted template from Mandrill (as not used)
    # https://www.kbsoftware.co.uk/crm/ticket/5415/
    #
    # We would like to inform you that this month's instalment in respect of
    # your course fees has been declined.
    # In order for us to receive this payment, please can you login to our
    # website and follow the instructions.
    # You can also update your details if you have received a new card.
    # This is a European regulatory requirement to reduce fraud and make online
    # payments more secure.
    # We look forward to receiving your payment as soon as possible.

    REMINDER_CHECK = 21
    REMINDER_DAYS = 7
    RETRY_COUNT = 4

    object_payment_plan = models.ForeignKey(
        ObjectPaymentPlan, on_delete=models.CASCADE
    )
    count = models.IntegerField()
    state = models.ForeignKey(
        CheckoutState, default=default_checkout_state, on_delete=models.CASCADE
    )
    deposit = models.BooleanField(help_text="Is this the initial payment")
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    due = models.DateField()
    # error details
    retry_count = models.IntegerField(blank=True, null=True)
    objects = ObjectPaymentPlanInstalmentManager()

    class Meta:
        unique_together = (
            ("object_payment_plan", "due"),
            ("object_payment_plan", "count"),
        )
        verbose_name = "Payments for an object"
        verbose_name_plural = "Payments for an object"

    def __str__(self):
        return "{} {} {}".format(
            self.object_payment_plan.payment_plan.name, self.due, self.amount
        )

    def _create_pay_on_session_email(self, user):  # is_auth_template):
        """email the user requesting an on-session payment.

        Keyword arguments:
        user -- who is requesting the payment
        is_auth_template -- are we asking the user to log into their account?

        .. note:: I can't decide whether to keep creating ``Checkout``
                  instances, but I think we will.
                  Our ``ObjectPaymentPlanInstalmentPayOnSessionRedirectView``
                  class creates a new instance each time it is run.
                  Each instance will act as an audit of who and when an on
                  session payment was requested.

        .. tip:: This method is in the ``ObjectPaymentPlanInstalment`` class.

        """
        data = {
            "name": self.checkout_first_name.title(),
            "description": "".join(self.checkout_description),
            "amount": self.amount,
        }
        # if is_auth_template:
        #    template_name = self.MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH
        # else:
        template_name = self.MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON
        checkout = Checkout.objects.create_checkout_payment_intent_on_session(
            self, user
        )
        url = urllib.parse.urljoin(
            settings.HOST_NAME,
            reverse("web.checkout.pay.on.session", args=[checkout.uuid]),
        )
        # https://www.kbsoftware.co.uk/docs/app-mail.html?highlight=mailchimp#mandrill-template
        data.update(
            {"url": '<a href="{}" mc:disable-tracking>{}</a>'.format(url, url)}
        )
        queue_mail_template(self, template_name, {self.checkout_email: data})

    def _raise_exception_if_success(self, checkout):
        if self.state.slug == CheckoutState.SUCCESS:
            raise CheckoutError(
                "Instalment {} has already been paid successfully "
                "(checkout {})".format(self.pk, checkout.pk)
            )

    def can_mark_paid(self):
        result = False
        if not self.object_payment_plan.deleted:
            if not self.state.slug == CheckoutState.SUCCESS:
                result = True
        return result

    def charge_stripe(self, user, payment_run_item=None):
        """Charge an instalment (could be a deposit).

        .. tip:: ``self`` is ``ObjectPaymentPlanInstalment``.

        What am I going to do here?

        1. Lookup the customer...
        2. Get the Stripe customer
        3. Check the customer has a payment method ('SetupIntent')
        4. Charge the card and see what happens!

        Used to be::

          Checkout.objects.charge(deposit, user, self.payment_method_id)

        To test the ``decline_code`` of  ``authentication_required``, use the
        card number ``4000002760003184``.

        For more information, see:
        https://stripe.com/docs/payments/cards/charging-saved-cards#notify

        """
        result = False
        customer = Customer.objects.get_customer(self.checkout_email)
        payment_method_id = customer.default_payment_method_id()
        if (
            self.state.is_fail()
            or self.state.is_pending()
            or self.state.is_request()
        ):
            pass
        else:
            raise CheckoutError(
                "Instalment '{}' must be in the failed, pending or "
                "request state: '{}'".format(self.pk, self.state.slug)
            )
        # We set the status to 'request' before asking for the money.
        # This is because we can't put the payment intent into a
        # transaction.
        #
        # If we were not careful, we could have a situation where the
        # payment succeeds and we don't manage to set the state to
        # 'success'.
        #
        # In the code below, if the payment fails the record will be left in
        # the 'request' state and so we won't ask for the money again.
        self.state = CheckoutState.objects.get(slug=CheckoutState.REQUEST)
        self.save()
        # create a checkout object to store the payment intent
        checkout = Checkout(
            action=CheckoutAction.objects.get(slug=CheckoutAction.CHARGE),
            checkout_date=timezone.now(),
            content_object=self,
            customer=customer,
            description=", ".join(self.checkout_description),
            total=self.amount,
            user=user,
        )
        checkout.save()
        if payment_run_item:
            payment_run_item.checkout = checkout
            payment_run_item.save()
        # request and confirm payment in one step (``off_session``)
        intent = StripeCheckout().create_payment_intent_for_customer(
            customer.customer_id, payment_method_id, self.amount
        )
        checkout.payment_intent_id = intent["id"]
        checkout.save()
        status = intent["status"]
        if status == "succeeded":
            with transaction.atomic():
                checkout.payment_success()
                if not payment_run_item:
                    checkout.notify()
                result = True
        elif status == "requires_payment_method":
            last_payment_error = intent["last_payment_error"]
            # import json
            # print(json.dumps(intent, indent=4))
            # print(json.dumps(last_payment_error, indent=4))
            checkout.decline_code = last_payment_error.get(
                "decline_code"
            ) or last_payment_error.get("code")
            checkout.decline_message = last_payment_error["message"]
            checkout.save()
            with transaction.atomic():
                # If the 'decline_code' is
                # 'AUTHENTICATION_REQUIRED' or
                # 'EXPIRED_CARD' then the instalment state will be set to
                # 'Checkout.PAY_ON_SESSION'
                checkout.payment_fail()
                if not payment_run_item:
                    checkout.notify()
        else:
            with transaction.atomic():
                # state will be set to 'CheckoutError.FAIL'
                checkout.payment_fail()
                if not payment_run_item:
                    checkout.notify()
            # raise CheckoutError(
            logger.error(
                "Payment intent failed (status '{}') for "
                "customer {} ({}) (checkout {})".format(
                    status,
                    customer.customer_id,
                    self.checkout_email,
                    checkout.pk,
                )
            )
        return result

    @property
    def checkout_can_charge(self):
        """Check we can take payment for the ``ObjectPaymentPlanInstalment``."""
        result = False
        slug = self.state.slug
        if self.deposit:
            check = slug in (CheckoutState.FAIL, CheckoutState.PENDING)
        else:
            check = slug in (
                CheckoutState.FAIL,
                CheckoutState.PAY_ON_SESSION,
                CheckoutState.PENDING,
                CheckoutState.REQUEST,
            )
        if check and not self.object_payment_plan.deleted:
            result = True
        return result

    @property
    def checkout_description(self):
        result = []
        if self.deposit:
            result.append("deposit")
        else:
            # instalments to display as '3 of 6' rather than '4 of 7' #1389
            result.append(
                "{} of {} instalments".format(
                    self.current_instalment(),
                    self.object_payment_plan.instalment_count(),
                )
            )
        return result

    @property
    def checkout_email(self):
        return self.object_payment_plan.content_object.checkout_email

    def checkout_fail(self, checkout):
        """Update this instalment to record the payment failure.

        .. tip:: ``self`` is ``ObjectPaymentPlanInstalment``.

        Called from within a transaction so you can update the model.

        If the checkout has already been successfully paid, then raise an
        exception.

        01/10/2020, 'EXPIRED_CARD' will ask for an on-session payment.
        This hasn't been requested, but makes sense to me (at 5am)!
        https://www.kbsoftware.co.uk/crm/ticket/5240/

        """
        self._raise_exception_if_success(checkout)
        if checkout.decline_code in (
            Checkout.AUTHENTICATION_REQUIRED,
            Checkout.EXPIRED_CARD,
        ):
            state = CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION)
            # 27/09/2019, See ``send_on_session_payment_emails``
            # send an email asking the customer to login and pay on-session
            # context = {
            #    self.checkout_email: {
            #        "description": "".join(self.checkout_description),
            #        "name": self.checkout_first_name,
            #    }
            # }
            # queue_mail_template(
            #    self, Customer.MAIL_TEMPLATE_PAY_ON_SESSION, context
            # )
        else:
            state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
        self.state = state
        self.save()
        self.object_payment_plan.content_object.instalment_fail(
            checkout, self.due
        )

    @property
    def checkout_first_name(self):
        return self.object_payment_plan.content_object.checkout_first_name

    @property
    def checkout_name(self):
        return self.object_payment_plan.content_object.checkout_name

    def checkout_success(self, checkout):
        """Record a successful payment for the 'ObjectPaymentPlanInstalment'.

        Called from within a transaction and you can update the model.

        If the checkout has already been successfully paid, then raise an
        exception.

        """
        self._raise_exception_if_success(checkout)
        if self.state.slug == CheckoutState.SUCCESS:
            raise CheckoutError(
                "Instalment {} has already been paid successfully "
                "(checkout {})".format(self.pk, checkout.pk)
            )
        self.state = checkout.state
        self.save()
        if self.deposit:
            self.object_payment_plan.create_instalments()
        self.object_payment_plan.content_object.instalment_success(checkout)

    # def checkout_success_url(self, checkout_pk):
    #    return self.get_absolute_url()

    @property
    def checkout_total(self):
        return self.amount

    def create_pay_on_session_email(self, user, is_auth_template):
        """email the user requesting an on-session payment.

        Keyword arguments:
        user -- who is requesting the payment
        is_auth_template -- are we asking the user to log into their account?

        .. tip:: This method is in the ``ObjectPaymentPlanInstalment`` class.

        """
        result = False
        checkout = Checkout.objects.success_for_content_object(self).first()
        if checkout:
            # we already have a successful checkout for this instalment
            logger.info(
                "'create_pay_on_session_email', already has a successful "
                "checkout (id {} created {}) for {} id {} ({})".format(
                    checkout.pk,
                    checkout.created.strftime("%d/%m/%Y %H:%M"),
                    self.__class__.__name__,
                    self.pk,
                    str(self),
                )
            )
        else:
            self._create_pay_on_session_email(user)  # , is_auth_template)
            result = True
        return result

    def current_instalment(self):
        if self.deposit:
            raise CheckoutError(
                "Cannot ask the deposit record for it's instalment count"
            )
        if self.count > 1:
            return self.count - 1
        else:
            raise CheckoutError(
                "The 'count' value for an instalment should be greater than 1"
            )

    def get_absolute_url(self):
        """TODO Update this to display the payment plan."""
        return reverse(
            "checkout.object.payment.plan", args=[self.object_payment_plan.pk]
        )

    def mail(self):
        # pks = [x.pk for x in Message.objects.for_content_object(self)]
        messages = Message.objects.for_content_object(self)
        return Mail.objects.filter(
            # email=self.object.email,
            # message__pk__in=pks,
            message__in=messages,
            # message__template__slug__in=(
            # ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH,
            # ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON,
            # ),
        )


reversion.register(ObjectPaymentPlanInstalment)


class PaymentRunManager(models.Manager):
    def create_payment_run(self):
        obj = self.model(created=timezone.now())
        obj.save()
        return obj

    def notify(self, payment_run, error_count):
        """Send notification of payment run"""
        email_addresses = [n.email for n in Notify.objects.all()]
        if email_addresses:
            subject = "Payment plan activity update - {}".format(
                timezone.now().strftime("%d/%m/%Y %H:%M")
            )
            message = "Processed {} payment transactions.\n\n".format(
                payment_run.item_count()
            )
            if error_count:
                message = message + (
                    "{} failed transaction(s).\n\n".format(error_count)
                )
            message = message + (
                "To view the transactions, log into your dashboard "
                "and click:\nSettings, Checkout - Payment Plans - Audit"
            )
            queue_mail_message(payment_run, email_addresses, subject, message)
            # will also trigger sending of emails to customers
            transaction.on_commit(lambda: process_mail.send())
        else:
            logger.error(
                "Cannot send email notification of payment transactions.  "
                "No email addresses set-up in 'enquiry.models.Notify'"
            )

    def process_payments(self):
        """Process pending payments.

        We set the status to 'request' before asking for the money.  This is
        because we can't put the payment request into a transaction.  If we are
        not careful, we could have a situation where the payment succeeds and
        we don't manage to set the state to 'success'.  In the code below, if
        the payment fails the record will be left in the 'request' state and
        so we won't ask for the money again.

        """
        error_count = 0
        payment_run = None
        system_user = system_generated_user()
        pks = []
        qs = ObjectPaymentPlanInstalment.objects.due()
        if qs.count():
            with transaction.atomic():
                payment_run = self.create_payment_run()
                for instalment in qs:
                    item = PaymentRunItem.objects.create_payment_run_item(
                        payment_run, instalment
                    )
                    pks.append(item.pk)
        for pk in pks:
            item = PaymentRunItem.objects.get(pk=pk)
            with transaction.atomic():
                # make sure the payment is still pending
                instalment = (
                    ObjectPaymentPlanInstalment.objects.select_for_update(
                        nowait=True
                    ).get(pk=item.instalment.pk)
                )
                # Query only returns instalments which are pending or failed.
                # Difficult to test, but we are just double checking to make
                # sure the state hasn't changed since.
                if instalment.state.is_fail() or instalment.state.is_pending():
                    pass
                else:
                    raise CheckoutError(
                        "Instalment '{}' must be in the failed or pending "
                        "state: '{}'".format(
                            instalment.pk, instalment.state.slug
                        )
                    )
                # we are ready to request payment
                state = CheckoutState.objects.get(slug=CheckoutState.REQUEST)
                instalment.state = state
                retry_count = instalment.retry_count or 0
                instalment.retry_count = retry_count + 1
                instalment.save()
            # request payment
            try:
                # if Stripe asks for an on-session payment, then increment count
                if not instalment.charge_stripe(system_user, item):
                    error_count = error_count + 1
            except (CheckoutError, CustomerDoesNotExistError) as e:
                logger.error(e)
                error_count = error_count + 1
        if payment_run:
            self.notify(payment_run, error_count)
        return len(pks), error_count


class PaymentRun(models.Model):
    created = models.DateTimeField()
    objects = PaymentRunManager()

    class Meta:
        ordering = ("created",)
        verbose_name = "Payment Run"
        verbose_name_plural = "Payment Runs"

    def __str__(self):
        return "Payment Run {} created {}".format(
            self.pk, self.created.strftime("%d/%m/%Y %H:%M")
        )

    def item_count(self):
        return self.paymentrunitem_set.count()


class PaymentRunItemManager(models.Manager):
    def create_payment_run_item(self, payment_run, instalment):
        obj = self.model(
            created=timezone.now(),
            payment_run=payment_run,
            instalment=instalment,
        )
        obj.save()
        return obj


class PaymentRunItem(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField(auto_now=True)
    payment_run = models.ForeignKey(PaymentRun, on_delete=models.CASCADE)
    instalment = models.ForeignKey(
        ObjectPaymentPlanInstalment, on_delete=models.CASCADE
    )
    checkout = models.ForeignKey(
        Checkout, blank=True, null=True, on_delete=models.CASCADE
    )
    objects = PaymentRunItemManager()

    class Meta:
        ordering = ("payment_run__pk", "created")
        unique_together = ("payment_run", "instalment")
        verbose_name = "Payment Run Item"
        verbose_name_plural = "Payment Run Items"

    def __str__(self):
        return "{} created {} for instalment {}".format(
            self.payment_run.pk,
            self.payment_run.created.strftime("%d/%m/%Y %H:%M"),
            self.instalment.pk,
        )


class CheckoutSettingsManager(models.Manager):
    def settings(self):
        try:
            return self.model.objects.get()
        except self.model.DoesNotExist:
            raise CheckoutError(
                "Checkout settings have not been set-up in admin"
            )


class CheckoutSettings(SingletonModel):
    default_payment_plan = models.ForeignKey(
        PaymentPlan,
        related_name="+",
        on_delete=models.CASCADE,
    )
    default_payment_plan_option_2 = models.ForeignKey(
        PaymentPlan, related_name="+", on_delete=models.CASCADE
    )
    objects = CheckoutSettingsManager()

    class Meta:
        verbose_name = "Checkout Settings"

    def __str__(self):
        return "Default Payment Plan: {}".format(self.default_payment_plan.name)


reversion.register(CheckoutSettings)
