# -*- encoding: utf-8 -*-
from django import forms

from .models import (
    Checkout,
    CheckoutAction,
    CheckoutSettings,
    Customer,
    ObjectPaymentPlan,
    ObjectPaymentPlanInstalment,
    PaymentPlan,
)


class ActionAddressForm(forms.ModelForm):
    """Checkout address / action form.

    .. note:: This is a legacy style form which we use to support old-style
              checkout flows i.e. where the user chooses the payment method on
              the checkout page.

    .. note:: This is a model form, but the model is not defined at this stage.
              This form is designed to be inherited by other forms.
              The model will need to be defined by those forms.

    .. tip:: If extending this to a form that has contact address fields then
             set ``additional_prefix`` to make the field names unique.

    """

    action_slug = forms.ChoiceField(widget=forms.RadioSelect)
    additional_prefix = ""

    def _action_choices(self, actions):
        result = []
        for item in actions:
            x = CheckoutAction.objects.get(slug=item)
            result.append((item, x.name))
        return result

    def _create_invoice_fields(self):
        # actions = kwargs.pop("actions")
        self.fields[self.additional_prefix + "company_name"] = forms.CharField(
            required=False, label="Company Name"
        )
        self.fields[self.additional_prefix + "address_1"] = forms.CharField(
            label="Address", required=False
        )
        self.fields[self.additional_prefix + "address_2"] = forms.CharField(
            label="", required=False
        )
        self.fields[self.additional_prefix + "address_3"] = forms.CharField(
            label="", required=False
        )
        self.fields[self.additional_prefix + "town"] = forms.CharField(
            required=False, label="Town"
        )
        self.fields[self.additional_prefix + "county"] = forms.CharField(
            required=False, label="County"
        )
        self.fields[self.additional_prefix + "postcode"] = forms.CharField(
            required=False, label="Postcode"
        )
        self.fields[self.additional_prefix + "country"] = forms.CharField(
            required=False, label="Country"
        )
        # contact
        self.fields[self.additional_prefix + "contact_name"] = forms.CharField(
            required=False, label="Contact Name"
        )
        self.fields[self.additional_prefix + "email"] = forms.EmailField(
            required=False, label="Email"
        )
        self.fields[self.additional_prefix + "phone"] = forms.CharField(
            required=False, label="Phone"
        )
        hidden_fields = []
        # hide invoice fields if not used
        hidden_fields = [
            self.additional_prefix + "company_name",
            self.additional_prefix + "address_1",
            self.additional_prefix + "address_2",
            self.additional_prefix + "address_3",
            self.additional_prefix + "town",
            self.additional_prefix + "county",
            self.additional_prefix + "postcode",
            self.additional_prefix + "country",
            self.additional_prefix + "contact_name",
            self.additional_prefix + "email",
            self.additional_prefix + "phone",
        ]
        for name in hidden_fields:
            self.fields[name].widget = forms.HiddenInput()
        for field_name in (
            self.additional_prefix + "company_name",
            self.additional_prefix + "address_1",
            self.additional_prefix + "town",
            self.additional_prefix + "county",
            self.additional_prefix + "postcode",
            self.additional_prefix + "contact_name",
            self.additional_prefix + "email",
            self.additional_prefix + "phone",
        ):
            if "class" in self.fields[field_name].widget.attrs:
                css_class = (
                    self.fields[field_name].widget.attrs["class"]
                    + " required-when-invoice"
                )
            else:
                css_class = "required-when-invoice"
            self.fields[field_name].widget.attrs.update({"class": css_class})

    def __init__(self, *args, **kwargs):
        valid_action_slugs = kwargs.pop("action_slugs", None)
        super().__init__(*args, **kwargs)
        self._create_invoice_fields()
        if valid_action_slugs:
            choices = self._action_choices(valid_action_slugs)
            f = self.fields["action_slug"]
            f.choices = choices
            f.required = True
            if len(choices) == 1:
                f.widget = forms.HiddenInput()
                self.initial["action_slug"] = choices[0][0]
            elif CheckoutAction.PAYMENT in valid_action_slugs:
                self.initial["action_slug"] = CheckoutAction.PAYMENT
            # show invoice fields if used
            if CheckoutAction.INVOICE in valid_action_slugs:
                restore_fields = [
                    self.additional_prefix + "company_name",
                    self.additional_prefix + "address_1",
                    self.additional_prefix + "address_2",
                    self.additional_prefix + "address_3",
                    self.additional_prefix + "town",
                    self.additional_prefix + "county",
                    self.additional_prefix + "postcode",
                    self.additional_prefix + "country",
                    self.additional_prefix + "contact_name",
                    self.additional_prefix + "phone",
                ]
                for name in restore_fields:
                    self.fields[name].widget = forms.TextInput()
                name = self.additional_prefix + "email"
                self.fields[name].widget = forms.EmailInput()

    def clean(self):
        cleaned_data = super().clean()
        # pop to avoid validating the action
        action = cleaned_data.get("action_slug")
        if action == CheckoutAction.INVOICE:
            valid = True
            for name in (
                self.additional_prefix + "company_name",
                self.additional_prefix + "address_1",
                self.additional_prefix + "town",
                self.additional_prefix + "county",
                self.additional_prefix + "postcode",
                self.additional_prefix + "country",
                self.additional_prefix + "contact_name",
            ):
                cleaned_field_data = cleaned_data.get(name)
                if cleaned_field_data:
                    valid = valid and cleaned_field_data.strip()
                else:
                    valid = False
            if valid:
                cleaned_email_data = cleaned_data.get(
                    self.additional_prefix + "email"
                )
                cleaned_phone_data = cleaned_data.get(
                    self.additional_prefix + "phone"
                )
                valid = (cleaned_email_data and cleaned_email_data.strip()) or (
                    cleaned_phone_data and cleaned_phone_data.strip()
                )
                if not valid:
                    raise forms.ValidationError(
                        "Please enter an email address or phone number."
                    )
            else:
                raise forms.ValidationError(
                    "Please enter an invoice company name, address, town, "
                    "county, postcode, country, contact name and an email "
                    "address or phone number."
                )
        return cleaned_data

    def additional_data(self):
        return dict(
            company_name=self.cleaned_data[
                self.additional_prefix + "company_name"
            ],
            address_1=self.cleaned_data[self.additional_prefix + "address_1"],
            address_2=self.cleaned_data[self.additional_prefix + "address_2"],
            address_3=self.cleaned_data[self.additional_prefix + "address_3"],
            town=self.cleaned_data[self.additional_prefix + "town"],
            county=self.cleaned_data[self.additional_prefix + "county"],
            postcode=self.cleaned_data[self.additional_prefix + "postcode"],
            country=self.cleaned_data[self.additional_prefix + "country"],
            contact_name=self.cleaned_data[
                self.additional_prefix + "contact_name"
            ],
            email=self.cleaned_data[self.additional_prefix + "email"],
            phone=self.cleaned_data[self.additional_prefix + "phone"],
        )


class ActionAddressCheckoutForm(ActionAddressForm):
    class Meta:
        model = Checkout
        fields = ("action_slug",)


class CheckoutEmptyForm(forms.ModelForm):
    class Meta:
        model = Checkout
        fields = ()


class CheckoutSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("default_payment_plan", "default_payment_plan_option_2"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = CheckoutSettings
        fields = ("default_payment_plan", "default_payment_plan_option_2")


# class CustomerCheckoutAddressForm(CheckoutAddressForm):
#    class Meta:
#        model = Customer
#        fields = ()
#        # fields = ("action",)


class CustomerEmptyForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ()


class ObjectPaymentPlanEmptyForm(forms.ModelForm):
    class Meta:
        model = ObjectPaymentPlan
        fields = ()


class ObjectPaymentPlanInstalmentEmptyForm(forms.ModelForm):
    class Meta:
        model = ObjectPaymentPlanInstalment
        fields = ()


# class ObjectPaymentPlanInstalmentCheckoutForm(CheckoutAddressForm):
#    class Meta:
#        model = ObjectPaymentPlanInstalment
#        fields = ()
#        # fields = ("action",)


class PaymentPlanEmptyForm(forms.ModelForm):
    class Meta:
        model = PaymentPlan
        fields = ()


class PaymentPlanForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update(
            {"class": "pure-input-1-2", "rows": 4}
        )

    class Meta:
        model = PaymentPlan
        fields = ("slug", "name", "deposit", "count", "interval")
