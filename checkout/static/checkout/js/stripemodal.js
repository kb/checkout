var stripeModal = (function(){
  var method = {};
  var $createContent = false;
  var $overlay = $('<div id="stripe-modal-overlay"></div>');
  var $modal = $('<div id="stripe-modal"></div>');
  var $close = $('<button id="stripe-modal-close" type="button"><span>&plus;</span></button>');

  $modal.hide();
  $overlay.hide();
  $modal.append($close);
  $(document).ready(function(){
    $('body').append($overlay, $modal);
  });

  // Center the modal in the viewport
  method.center = function () {
    var top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
    var left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;
    $modal.css({
      top:top + $(window).scrollTop(),
      left:left + $(window).scrollLeft()
    });
  };

  // Open the modal
  method.open = function (settings) {
    $createContent = false;
    if (settings.content) {
        var $content = $('<div id="stripe-modal-content"></div>');
        $modal.empty().append($content);
        $content.empty().append(settings.content);
        $content.show();
        $createContent = true;
    }
    else if (settings.contentId) {
        var $content = $("#" + settings.contentId);
        $content.show();
        $modal.empty().append($content);
    }
    else {
        if (console) console.log("ERROR: Cannot create modal as neither content nor contentId is defined");
        return;
    }

      $("body").addClass("modal-open");

    if (settings.css) {
        $modal.css(settings.css);
    }

    // Use jQuery event namespace
    $(window).bind('resize.modal', method.center);
    $modal.show();
    $overlay.show();
    method.center();

    if (settings.clickModalToClose !== false)
      $modal.on("click", doClose);

    if (settings.clickOverlayToClose !== false)
      $overlay.on("click", doClose);

    if (settings.hasCloseButton !== false) {
      $modal.append($close);
      $close.on("click", doClose);
    }
  };

  // Close the modal
  method.close = function () {
    $("body").removeClass("modal-open");
    $modal.hide();
    $overlay.hide();
    if ($createContent)
        $content.empty();

    $modal.off("click");
    $overlay.off("click");

    // Remove the binding using jQuery event namespace
    $(window).unbind('resize.stripeModal');
  };

  function doClose(e) {
    e.preventDefault();
    method.close();
  }

  return method;
}());

