# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings

from checkout.models import (
    Checkout,
    ObjectPaymentPlan,
    ObjectPaymentPlanInstalment,
    PaymentRun,
)


logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def process_payments():
    logger.info("process_payments")
    return PaymentRun.objects.process_payments()


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def refresh_card_expiry_dates():
    logger.info("refresh_card_expiry_dates")
    ObjectPaymentPlan.objects.refresh_card_expiry_dates()


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def send_failure_emails():
    logger.info("send_failure_emails")
    count = ObjectPaymentPlanInstalment.objects.send_failure_emails()
    logger.info("send_failure_emails - {} records- complete".format(count))
    return count


# The 'send_on_session_payment_emails' needs to be implemented in your project
# because the 'can_login_and_pay' function is specific to your project.
#
# For details, see 'send_on_session_payment_emails' in our docs:
# https://www.kbsoftware.co.uk/docs/app-checkout.html#tasks
#
# @task()
# def send_on_session_payment_emails():
#     logger.info("send_on_session_payment_emails")
#     count = ObjectPaymentPlanInstalment.objects.send_on_session_payment_emails(
#         can_login_and_pay
#     )
#     logger.info(
#         "send_on_session_payment_emails - {} records- complete".format(count)
#     )
#     return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def send_payment_reminder_emails():
    logger.info("send_payment_reminder_emails")
    return ObjectPaymentPlanInstalment.objects.send_payment_reminder_emails()


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def payment_intent_fulfillment():
    caption = "payment_intent_fulfillment"
    logger.info("{}".format(caption))
    count = Checkout.objects.payment_intent_fulfillment()
    logger.info("{} - {} records - complete".format(caption, count))
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def payment_intent_on_session_fulfillment():
    caption = "payment_intent_on_session_fulfillment"
    logger.info("{}".format(caption))
    count = Checkout.objects.payment_intent_on_session_fulfillment()
    logger.info("{} - {} records - complete".format(caption, count))
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def setup_intent_fulfillment():
    caption = "setup_intent_fulfillment"
    logger.info("{}".format(caption))
    count = Checkout.objects.setup_intent_fulfillment()
    logger.info("{} - {} records - complete".format(caption, count))
    return count


def schedule_payment_intent_fulfillment():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    payment_intent_fulfillment.send()


def schedule_payment_intent_on_session_fulfillment():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    payment_intent_on_session_fulfillment.send()


def schedule_process_payments():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    process_payments.send()


def schedule_refresh_card_expiry_dates():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    refresh_card_expiry_dates.send()


def schedule_send_failure_emails():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    send_failure_emails.send()


def schedule_send_payment_reminder_emails():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    send_payment_reminder_emails.send()


def schedule_setup_intent_fulfillment():
    """APScheduler would like to run the task.

    .. note:: The scheduler doesn't have access to the Dramatiq queue, so send
              the message from here.

    """
    setup_intent_fulfillment.send()
