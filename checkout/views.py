# -*- encoding: utf-8 -*-
import json
import logging
import stripe

from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    RedirectView,
    TemplateView,
    UpdateView,
)

from base.url_utils import url_with_querystring
from base.view_utils import BaseMixin
from mail.service import queue_mail_template
from mail.tasks import process_mail
from .forms import (
    ActionAddressCheckoutForm,
    CheckoutEmptyForm,
    CheckoutSettingsForm,
    CustomerEmptyForm,
    ObjectPaymentPlanEmptyForm,
    ObjectPaymentPlanInstalmentEmptyForm,
    PaymentPlanEmptyForm,
    PaymentPlanForm,
)
from .models import (
    Checkout,
    CheckoutAction,
    CheckoutAdditional,
    CheckoutError,
    CheckoutSettings,
    CheckoutState,
    Customer,
    get_contact_model,
    ObjectPaymentPlan,
    ObjectPaymentPlanInstalment,
    PaymentPlan,
    PaymentRun,
    PaymentRunItem,
)


CONTENT_OBJECT_PK = "content_object_pk"
logger = logging.getLogger(__name__)


class CheckoutAuditListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(audit=True))
        return context

    def get_queryset(self):
        return Checkout.objects.audit()


class CheckoutCardRefreshListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    """Customers with expiring cards."""

    paginate_by = 10
    template_name = "checkout/card_refresh_list.html"

    def get_queryset(self):
        return Customer.objects.refresh


class CheckoutDashTemplateView(
    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, TemplateView
):
    template_name = "checkout/dash.html"


class CheckoutListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(audit=False))
        return context

    def get_queryset(self):
        return Checkout.objects.success()


class CheckoutBaseMixin:
    def _checkout(self):
        uuid = self._uuid()
        checkout = Checkout.objects.get(uuid=uuid)
        return checkout

    def _check_perm_success(self):
        """Check the permissions for the checkout object.

        We check the user (who might be anonymous) is not viewing an old
        transaction.

        """
        one_month = timezone.now() + relativedelta(months=-1)
        if self.object.created < one_month:
            raise CheckoutError(
                "Cannot view this checkout transaction.  It is too old "
                "(or has travelled in time, {} {} {} days).".format(
                    self.object.created.strftime("%d/%m/%Y"),
                    timezone.now().strftime("%d/%m/%Y"),
                    (one_month - self.object.created).days,
                )
            )

    def _uuid(self):
        uuid = self.kwargs["uuid"]
        return uuid

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self._check_perm_success()
        context.update(dict(stripe_publish_key=settings.STRIPE_PUBLISH_KEY))
        return context

    def get_object(self):
        return self._checkout()


class CheckoutMixin(CheckoutBaseMixin):
    form_class = CheckoutEmptyForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(action=self.object.action))
        return context


class CheckoutPayNowOrInvoiceMixin(CheckoutBaseMixin):
    """Checkout form where the user can choose between pay-now and invoice.

    .. note:: This is a legacy style view which we use to support old-style
              checkout flows i.e. where the user chooses the payment method on
              the checkout page.

    .. note:: This view does not handle payment plans because a Stripe payment
              intent will already have been created for this checkout object.

    """

    form_class = ActionAddressCheckoutForm

    def _action_data(self):
        """the action data for the javascript on the page."""
        actions = self._checkout_actions()
        result = {}
        for slug in actions:
            x = CheckoutAction.objects.get(slug=slug)
            result[slug] = {
                "name": x.name,
                "payment": True if x.is_payment() or x.is_charge() else False,
            }
        return json.dumps(result)

    def _checkout_actions(self):
        """A list of valid 'CheckoutAction' constants.

        .. note:: An ``action`` is a payment method e.g. invoice.

        """
        return [CheckoutAction.PAYMENT, CheckoutAction.INVOICE]

    def form_valid(self, form):
        action_slug = form.cleaned_data.pop("action_slug")
        action = CheckoutAction.objects.get(slug=action_slug)
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.action = action
            if action.slug == CheckoutAction.INVOICE:
                self.object.state = CheckoutState.objects.get(
                    slug=CheckoutState.SUCCESS
                )
            self.object = form.save()
            CheckoutAdditional.objects.create_checkout_additional(
                self.object, **form.additional_data()
            )
            self.object.content_object.checkout_success(self.object)
            self.object.notify()
            transaction.on_commit(lambda: process_mail.send())
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        total = self.object.content_object.checkout_total * Decimal("100")
        action_data = self._action_data()
        context.update(
            {
                "action_data": action_data,
                "allow_pay_by_invoice": True,
                "checkout_email": self.object.content_object.checkout_email,
                "checkout_name": self.object.content_object.checkout_name,
                "checkout_total": total,
                "client_secret": self.object.client_secret_payment_intent(),
                "description": self.object.content_object.checkout_description,
                "success_url": self.get_success_url(),
            }
        )
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(action_slugs=self._checkout_actions()))
        return kwargs


class CheckoutPaymentIntentMixin(CheckoutMixin):
    """Collect the data required for Stripe to hande a payment intent.

    This mixin is used for all on-session payments.

    The ``client_secret_payment_intent`` method will get the secret for a
    payment intent generated either with ``create_payment_intent``
    or ``create_pay_on_session_intent``.

    You need to create a ``get_success_url`` method in your derived class.
    This could simply refer *up* to the ``content_object`` e.g::

      def get_success_url(self):
          return self.object.content_object.checkout_success_url(
              self.object.pk
          )

    """

    def get(self, request, *args, **kwargs):
        """Check to see if a successful payment has already been made.

        If a payment has already been made, then redirect to the success view.

        """
        self.object = self.get_object()
        qs = Checkout.objects.success_for_content_object(
            self.object.content_object
        )
        if qs.exists():
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                checkout_email=self.object.content_object.checkout_email,
                checkout_name=self.object.content_object.checkout_name,
                checkout_total=self.object.content_object.checkout_total,
                client_secret=self.object.client_secret_payment_intent(),
                success_url=self.get_success_url(),
            )
        )
        return context

    def get_success_url(self):
        raise NotImplementedError(
            "{0} is missing implementation of the "
            "'get_success_url' method. You should write "
            "one.".format(self.__class__.__name__)
        )


class CheckoutPayOnSessionListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 10

    def get_queryset(self):
        return ObjectPaymentPlanInstalment.objects.pay_on_session().order_by(
            "-pk"
        )


class CheckoutPaymentIntentListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):
    template_name = "checkout/payment_intent_list.html"

    def get_queryset(self):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        return stripe.PaymentIntent.list(limit=10)


class ObjectPaymentPlanSetupIntentCardRefreshMixin:
    form_class = CheckoutEmptyForm

    def _contact(self):
        contact_model = apps.get_model(settings.CONTACT_MODEL)
        try:
            return contact_model.objects.get(user=self.request.user)
        except contact_model.DoesNotExist:
            raise CheckoutError(
                "Cannot find a contact record for '{}'".format(
                    self.request.user.username
                )
            )

    def _object_payment_plan(self):
        result = None
        contact = self._contact()
        email = contact.email()
        qs = ObjectPaymentPlan.objects.for_email(email)
        max_total = Decimal()
        for object_payment_plan in qs:
            total = object_payment_plan.total_outstanding()
            if total > max_total:
                max_total = total
                result = object_payment_plan
        if result is None:
            raise CheckoutError(
                "Cannot find a payment plan for '{}'".format(email)
            )
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        object_payment_plan = self._object_payment_plan()
        context.update(
            dict(
                action=self.object.action,
                checkout_email=self.request.user.email,
                checkout_name=object_payment_plan.checkout_name,
                checkout_total=self.object.total,
                client_secret=self.object.client_secret_setup_intent(),
                stripe_publish_key=settings.STRIPE_PUBLISH_KEY,
                success_url=self.get_success_url(),
            ),
        )
        return context

    def get_object(self):
        with transaction.atomic():
            object_payment_plan = self._object_payment_plan()
            checkout = Checkout.objects.create_checkout_setup_intent(
                object_payment_plan,
                self.request.user,
                object_payment_plan.payment_plan,
                action_slug=CheckoutAction.CARD_REFRESH,
            )
        return checkout


class CheckoutSettingsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = CheckoutSettingsForm

    def get_object(self):
        return CheckoutSettings.load()

    def get_success_url(self):
        return reverse("project.settings")


class CheckoutSetupIntentListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):
    template_name = "checkout/setup_intent_list.html"

    def get_queryset(self):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        return stripe.SetupIntent.list(limit=10)


class CheckoutSetupIntentMixin(CheckoutMixin):
    """Collect the data required for Stripe to hande a set-up intent.

    We use setup intents for initialising payment plans.

    We use a setup intent to collect card details, but not make a charge.

    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                client_secret=self.object.client_secret_setup_intent(),
                payment_plan_example=self.object.payment_plan_example(),
            )
        )
        return context


class CheckoutSuccessMixin(CheckoutBaseMixin):
    """Thank you for your payment (etc).

    Use with a ``DetailView`` e.g::

      class SalesLedgerCheckoutSuccessView(
          CheckoutSuccessMixin, BaseMixin, DetailView):

    """

    model = Checkout

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object.action.is_payment_plan():
            context.update(example=self.object.payment_plan_example())
        return context


class CustomerCardRefreshRequestFormView(
    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, UpdateView
):
    """Send an email to a user asking them to refresh their card details.

    The email will contain a link to the page on the site where the user can
    enter their card details.

    The URL is hard-coded to be ``web.checkout.refresh.card``.

    """

    form_class = CustomerEmptyForm
    model = Customer
    template_name = "checkout/customer_card_refresh_form.html"

    def _get_customer(self):
        pk = int(self.kwargs.get("pk"))
        return Customer.objects.get(pk=pk)

    def _queue_email(self):
        customer = self._get_customer()
        name = customer.name_or_first_name_of_user().title()
        url = self.request.build_absolute_uri(
            reverse("web.checkout.refresh.card")
        )
        context = dict(name=name, url=url)
        queue_mail_template(
            customer,
            Customer.MAIL_TEMPLATE_CARD_REFRESH_REQUEST,
            {customer.email: context},
        )
        transaction.on_commit(lambda: process_mail.send())
        messages.info(
            self.request,
            "email sent to {} asking them to update their card details".format(
                customer.email
            ),
        )

    def form_valid(self, form):
        self._queue_email()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(customer=self._get_customer()))
        return context

    def get_success_url(self):
        customer = self._get_customer()
        return url_with_querystring(
            reverse("checkout.customer"), email=customer.email
        )


# .. note:: We are not using this with SCA (yet).
#           For details, see:
#           https://www.kbsoftware.co.uk/docs/app-checkout.html#changelog
#
# class CustomerCheckoutRefreshUpdateView(
#    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, UpdateView
# ):
#
#    form_class = CustomerCheckoutForm
#    model = Customer
#    template_name = "checkout/customer_checkout_refresh.html"
#
#    def get_context_data(self, **kwargs):
#        context = super().get_context_data(**kwargs)
#        context.update(dict(key=settings.STRIPE_PUBLISH_KEY))
#        return context


class CustomerTemplateView(
    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, TemplateView
):
    template_name = "checkout/customer.html"

    def _contacts(self, email):
        contact_model = apps.get_model(settings.CONTACT_MODEL)
        return contact_model.objects.filter(user__email__iexact=email)

    def _email(self):
        email = self.request.GET.get("email")
        if not email:
            raise Http404("Customer view needs an email address parameter.")
        return email

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        email = self._email()
        context.update(
            dict(
                audit=Checkout.objects.for_email(email),
                contacts=self._contacts(email),
                customers=Customer.objects.for_email(email),
                email=email,
                payment_plans=ObjectPaymentPlan.objects.for_email(email),
            )
        )
        return context


class ObjectPaymentPlanDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ObjectPaymentPlanEmptyForm
    model = ObjectPaymentPlan
    template_name = "checkout/object_paymentplan_delete.html"

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("checkout.object.payment.plan.list")


class ObjectPaymentPlanListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 10

    def get_queryset(self):
        qs = ObjectPaymentPlan.objects.outstanding_payment_plans()
        return qs.order_by("-pk")


class ObjectPaymentPlanCardFailListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    """"""

    paginate_by = 10

    def get_queryset(self):
        return ObjectPaymentPlan.objects.fail_or_request()


class ObjectPaymentPlanInstalmentAuditListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 15

    def get_queryset(self):
        return Checkout.objects.for_content_object(
            ContentType.objects.get_for_model(ObjectPaymentPlanInstalment)
        ).order_by("-pk")


class ObjectPaymentPlanDetailView(
    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, DetailView
):
    model = ObjectPaymentPlan


class ObjectPaymentPlanInstalmentDetailView(
    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, DetailView
):
    model = ObjectPaymentPlanInstalment


class ObjectPaymentPlanInstalmentChargeUpdateView(
    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, UpdateView
):
    """Charge the customers card for this instalment.

    .. warning:: Some projects do extra processing when taking payment for a
                 deposit, so you may want to control access to this view in
                 those cases.

    """

    model = ObjectPaymentPlanInstalment
    form_class = ObjectPaymentPlanInstalmentEmptyForm
    template_name = "checkout/objectpaymentplaninstalment_charge_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        qs = Checkout.objects.success_for_content_object(self.object)
        if qs.exists():
            messages.info(
                self.request,
                "Instalment {} for {} has already been paid".format(
                    self.object.pk,
                    self.object.checkout_email,
                ),
            )
        else:
            if not self.object.checkout_can_charge:
                raise CheckoutError(
                    "Cannot take payment from Stripe for instalment"
                    "{} ('checkout_can_charge')".format(self.object.pk)
                )
            self.object.charge_stripe(self.request.user)
            if self.object.state.is_success():
                messages.info(
                    self.request,
                    "Charged '{}' a total of {} ref {}".format(
                        self.object.checkout_email,
                        self.object.checkout_total,
                        ", ".join(self.object.checkout_description),
                    ),
                )
            else:
                decline_message = Checkout.objects.decline_message(self.object)
                messages.error(
                    self.request,
                    "Could not charge {}.{}".format(
                        self.object.checkout_email,
                        (
                            " {}".format(decline_message)
                            if decline_message
                            else ""
                        ),
                    ),
                )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            "checkout.object.payment.plan",
            args=[self.object.object_payment_plan.pk],
        )


class ObjectPaymentPlanInstalmentEmailUpdateView(
    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, UpdateView
):
    """Charge the customers card for this instalment."""

    model = ObjectPaymentPlanInstalment
    form_class = ObjectPaymentPlanInstalmentEmptyForm
    template_name = "checkout/objectpaymentplaninstalment_email_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.create_pay_on_session_email(
            self.request.user, is_auth_template=False
        )
        messages.info(
            self.request,
            (
                "Sent an email to {} asking them to pay {} as an on-session "
                "payment".format(self.object.checkout_email, self.object.amount)
            ),
        )
        transaction.on_commit(lambda: process_mail.send())
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            "checkout.object.payment.plan",
            args=[self.object.object_payment_plan.pk],
        )


class ObjectPaymentPlanInstalmentPaidUpdateView(
    StaffuserRequiredMixin, LoginRequiredMixin, BaseMixin, UpdateView
):
    """Mark this instalment as paid ('CheckoutAction.MANUAL')."""

    model = ObjectPaymentPlanInstalment
    form_class = ObjectPaymentPlanInstalmentEmptyForm
    template_name = "checkout/objectpaymentplaninstalment_paid_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        Checkout.objects.create_checkout_manual(self.object, self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            "checkout.object.payment.plan",
            args=[self.object.object_payment_plan.pk],
        )


class ObjectPaymentPlanInstalmentPayOnSessionRedirectView(
    LoginRequiredMixin, BaseMixin, RedirectView
):
    permanent = False
    query_string = False

    def _user_emails(self):
        result = []
        if self.request.user.email:
            result.append(self.request.user.email)
        contact_model = get_contact_model()
        try:
            contact = contact_model.objects.get(user=self.request.user)
            result.append(contact.email())
        except contact_model.DoesNotExist:
            pass
        return result

    def get_redirect_url(self, *args, **kwargs):
        """

        .. tip:: For notes ref *creating ``Checkout`` instances*, see
                 ``ObjectPaymentPlanInstalment._create_pay_on_session_email``.

        .. note:: I thought about checking the user has the same email address
                  as the instalment, but I don't think I have a reliable way
                  of finding the email address for a user...

        """
        pk = kwargs["pk"]
        instalment = ObjectPaymentPlanInstalment.objects.get(pk=pk)
        instalment_email = (
            instalment.object_payment_plan.content_object.checkout_email
        )
        user_emails = self._user_emails()
        if instalment_email in user_emails:
            pass
        else:
            message = (
                "email address for logged in user "
                f"({self.request.user.username}) "
                "does not match email address for instalment "
                f"({instalment_email}): {user_emails}"
            )
            logger.error(message)
            raise CheckoutError(message)
        checkout = Checkout.objects.success_for_content_object(
            instalment
        ).first()
        if checkout:
            # we already have a successful checkout for this instalment
            pass
        else:
            # create a new checkout for this instalment
            checkout = (
                Checkout.objects.create_checkout_payment_intent_on_session(
                    instalment, self.request.user
                )
            )
        return reverse("web.checkout.pay.on.session", args=[checkout.uuid])


class PaymentPlanCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = PaymentPlanForm
    model = PaymentPlan

    def get_success_url(self):
        return reverse("checkout.payment.plan.list")


class PaymentPlanDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = PaymentPlanEmptyForm
    model = PaymentPlan
    template_name = "checkout/paymentplan_delete.html"

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.deleted = True
            self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("checkout.payment.plan.list")


class PaymentPlanListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 5

    def get_queryset(self):
        return PaymentPlan.objects.current()


class PaymentPlanUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = PaymentPlanForm
    model = PaymentPlan

    def get_success_url(self):
        return reverse("checkout.payment.plan.list")


class PaymentRunItemListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20

    def _payment_run(self):
        pk = int(self.kwargs.get("pk"))
        return PaymentRun.objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(payment_run=self._payment_run()))
        return context

    def get_queryset(self):
        return PaymentRunItem.objects.filter(
            payment_run=self._payment_run()
        ).order_by("created")


class PaymentRunListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20

    def get_queryset(self):
        return PaymentRun.objects.all().order_by("-created")
