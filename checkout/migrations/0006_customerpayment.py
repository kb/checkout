# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("checkout", "0005_auto_20151002_2010"),
    ]

    operations = [
        migrations.CreateModel(
            name="CustomerPayment",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("description", models.CharField(max_length=100)),
                ("total", models.DecimalField(decimal_places=2, max_digits=8)),
                (
                    "customer",
                    models.ForeignKey(
                        to="checkout.Customer", on_delete=models.CASCADE
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        related_name="+",
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "Customer payment",
                "verbose_name_plural": "Customer payments",
                "ordering": ("pk",),
            },
        )
    ]
