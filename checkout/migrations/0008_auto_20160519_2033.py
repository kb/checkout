# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("checkout", "0007_objectpaymentplaninstalment_retry_count")
    ]

    operations = [
        migrations.CreateModel(
            name="PaymentRun",
            fields=[
                (
                    "id",
                    models.AutoField(
                        primary_key=True,
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                    ),
                ),
                ("created", models.DateTimeField()),
            ],
            options={
                "ordering": ("created",),
                "verbose_name": "Payment Run",
                "verbose_name_plural": "Payment Runs",
            },
        ),
        migrations.CreateModel(
            name="PaymentRunItem",
            fields=[
                (
                    "id",
                    models.AutoField(
                        primary_key=True,
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                    ),
                ),
                ("created", models.DateTimeField()),
                ("modified", models.DateTimeField(auto_now=True)),
                (
                    "checkout",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        to="checkout.Checkout",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "instalment",
                    models.ForeignKey(
                        to="checkout.ObjectPaymentPlanInstalment",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "payment_run",
                    models.ForeignKey(
                        to="checkout.PaymentRun", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "ordering": ("payment_run__pk", "created"),
                "verbose_name": "Payment Run Item",
                "verbose_name_plural": "Payment Run Items",
            },
        ),
        migrations.AlterUniqueTogether(
            name="paymentrunitem",
            unique_together=set([("payment_run", "instalment")]),
        ),
    ]
