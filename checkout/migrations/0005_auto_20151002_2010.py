# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("checkout", "0004_auto_20150907_1607")]

    operations = [
        migrations.CreateModel(
            name="CheckoutAdditional",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        primary_key=True,
                        serialize=False,
                        auto_created=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("company_name", models.CharField(max_length=100, blank=True)),
                (
                    "address_1",
                    models.CharField(
                        verbose_name="Address", max_length=100, blank=True
                    ),
                ),
                (
                    "address_2",
                    models.CharField(
                        verbose_name="", max_length=100, blank=True
                    ),
                ),
                (
                    "address_3",
                    models.CharField(
                        verbose_name="", max_length=100, blank=True
                    ),
                ),
                ("town", models.CharField(max_length=100, blank=True)),
                ("county", models.CharField(max_length=100, blank=True)),
                ("postcode", models.CharField(max_length=20, blank=True)),
                ("country", models.CharField(max_length=100, blank=True)),
                ("contact_name", models.CharField(max_length=100, blank=True)),
                ("email", models.EmailField(max_length=254)),
                ("phone", models.CharField(max_length=50, blank=True)),
                ("date_of_birth", models.DateField(blank=True, null=True)),
                (
                    "checkout",
                    models.OneToOneField(
                        to="checkout.Checkout", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "Checkout Additional Information",
                "verbose_name_plural": "Checkout Additional Information",
                "ordering": ("email",),
            },
        ),
        migrations.RemoveField(model_name="checkoutinvoice", name="checkout"),
        migrations.DeleteModel(name="CheckoutInvoice"),
    ]
