# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("checkout", "0006_customerpayment")]

    operations = [
        migrations.AddField(
            model_name="objectpaymentplaninstalment",
            name="retry_count",
            field=models.IntegerField(blank=True, null=True),
        )
    ]
