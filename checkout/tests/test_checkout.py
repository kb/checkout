# -*- encoding: utf-8 -*-
#
# These tests use the ``SalesLedger`` model (it has the required methods e.g.
# ``checkout_email``).
#
# You can find the ``Checkout`` tests in::
#   example_checkout/tests/test_checkout.py
