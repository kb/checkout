# -*- encoding: utf-8 -*-
import pytest

from datetime import date, datetime
from freezegun import freeze_time
from unittest import mock

from checkout.models import CheckoutError, Customer, CustomerDoesNotExistError
from checkout.tests.factories import CustomerFactory
from login.tests.factories import UserFactory
from mail.models import Message
from mail.tests.factories import MailTemplateFactory
from .helper import MockCustomer


# @pytest.mark.django_db
# def test_check_checkout():
#    customer = CustomerFactory()
#    check_checkout(customer)


@pytest.mark.django_db
def test_customer_refresh():
    customer = CustomerFactory(refresh=True)
    assert True == customer.refresh


@pytest.mark.django_db
def test_customer_refresh_default():
    customer = CustomerFactory()
    assert False == customer.refresh


@pytest.mark.django_db
def test_default_payment_method_id():
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = {
            "invoice_settings": {"default_payment_method": "876"}
        }
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "543",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                },
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                },
            ]
        }
        assert "876" == CustomerFactory().default_payment_method_id()


@pytest.mark.django_db
def test_default_payment_method_id_does_not_exist():
    customer = CustomerFactory(customer_id="abc123")
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = []
        mock_payment_method.return_value = {"data": []}
        with pytest.raises(CheckoutError) as e:
            customer.default_payment_method_id()
    assert "No payment methods found for customer {}, 'abc123'".format(
        customer.id
    ) in str(e.value)


@pytest.mark.django_db
def test_default_payment_method_id_only_one():
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = {
            "invoice_settings": {"default_payment_method": None}
        }
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "876",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                }
            ]
        }
        assert "876" == CustomerFactory().default_payment_method_id()


@pytest.mark.django_db
def test_default_payment_method_id_select_first():
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = {
            "invoice_settings": {"default_payment_method": None}
        }
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "203",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                },
                {
                    "id": "456",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                },
                {
                    "id": "321",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                },
            ]
        }
        assert "203" == CustomerFactory().default_payment_method_id()


@pytest.mark.django_db
def test_default_payment_method_id_select_most_recent():
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = {
            "invoice_settings": {"default_payment_method": None}
        }
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "123",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                },
                {
                    "id": "456",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 456,
                },
                {
                    "id": "321",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 321,
                },
            ]
        }
        assert "456" == CustomerFactory().default_payment_method_id()


@pytest.mark.django_db
def test_name_or_first_name_of_user():
    """No user with this email address."""
    customer = CustomerFactory(email="code@pkimber.net", name="Patrick Kimber")
    assert "Patrick Kimber" == customer.name_or_first_name_of_user()


@pytest.mark.django_db
def test_name_or_first_name_of_user_matching():
    """No user with this email address."""
    customer = CustomerFactory(email="code@pkimber.net", name="Patrick Kimber")
    user = UserFactory(first_name="Patrick", email="code@pkimber.net")
    assert "Patrick" == customer.name_or_first_name_of_user()


@pytest.mark.django_db
def test_name_or_first_name_of_user_two_matching():
    """No user with this email address."""
    customer = CustomerFactory(email="code@pkimber.net", name="Patrick Kimber")
    user = UserFactory(first_name="Patrick", email="code@pkimber.net")
    user = UserFactory(first_name="Pat", email="code@pkimber.net")
    assert "Patrick Kimber" == customer.name_or_first_name_of_user()


@pytest.mark.django_db
def test_get_absolute_url():
    customer = CustomerFactory(email="code@pkimber.net")
    assert (
        "/checkout/customer/?email=code%40pkimber.net"
        == customer.get_absolute_url()
    )


@pytest.mark.django_db
def test_get_customer():
    customer = CustomerFactory(email="pat@kb.com")
    assert customer.pk == Customer.objects.get_customer("Pat@kb.com").pk


@pytest.mark.django_db
def test_get_customer_duplicate():
    c1 = CustomerFactory(email="Pat@kb.com")
    c2 = CustomerFactory(email="pat@kb.com")
    assert c2.pk > c1.pk
    assert c2.pk == Customer.objects.get_customer("Pat@kb.com").pk


@pytest.mark.django_db
def test_get_customer_no_email():
    with pytest.raises(CheckoutError) as e:
        Customer.objects.get_customer(None)
    assert "Cannot get a customer without an email address" in str(e.value)


@pytest.mark.django_db
def test_get_customer_missing():
    with pytest.raises(CustomerDoesNotExistError) as e:
        Customer.objects.get_customer("pat@kb.com")
    assert "Cannot find a customer record for 'pat@kb.com'" in str(e.value)


@pytest.mark.django_db
def test_init_customer():
    CustomerFactory(name="Pat", email="code@pkimber.net")
    # customer already exists
    with mock.patch("stripe.Customer.modify"):
        customer = Customer.objects.init_customer("Patrick", "code@pkimber.net")
    assert "Patrick" == customer.name
    assert "code@pkimber.net" == customer.email


@pytest.mark.django_db
def test_init_customer_create():
    with mock.patch("stripe.Customer.create") as mock_customer_create:
        mock_customer_create.return_value = MockCustomer()
        customer = Customer.objects.init_customer("Patrick", "code@pkimber.net")
    assert "Patrick" == customer.name
    assert "code@pkimber.net" == customer.email


@pytest.mark.django_db
def test_is_expiring_future():
    customer = CustomerFactory(expiry_date=date(3000, 2, 1))
    assert customer.is_expiring() is False


@pytest.mark.django_db
def test_is_expiring_none():
    customer = CustomerFactory()
    assert customer.is_expiring() is False


@pytest.mark.django_db
def test_is_expiring_past():
    customer = CustomerFactory(expiry_date=date(2015, 2, 1))
    assert customer.is_expiring() is True


@pytest.mark.django_db
def test_pks_with_email():
    c1 = CustomerFactory(email="code@pkimber.net")
    c2 = CustomerFactory(email="patrick@pkimber.net")
    c3 = CustomerFactory(email="code@PKimber.net")
    assert [c3.pk, c1.pk] == [
        x
        for x in Customer.objects.pks_with_email(
            "code@pkimber.net", [c1.pk, c2.pk, c3.pk]
        )
    ]


@pytest.mark.django_db
def test_str():
    assert "code@pkimber.net orange" == str(
        CustomerFactory(customer_id="orange", email="code@pkimber.net")
    )


@pytest.mark.parametrize(
    "customer_email,user_email,expect",
    [
        ("code@pkimber.net", "code@pkimber.net", "Pat"),
        ("code@pkimber.net", "patrick@kbsoftware.co.uk", "Patrick Kimber"),
    ],
)
@pytest.mark.django_db
def test_update_card_expiry(customer_email, user_email, expect):
    MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_CARD_EXPIRY)
    customer = CustomerFactory(email="code@pkimber.net", name="Patrick Kimber")
    UserFactory(email=user_email, first_name="Pat")
    assert customer.is_expiring() is False
    assert customer.refresh is False
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = {
            "invoice_settings": {"default_payment_method": "876"}
        }
        mock_payment_method.return_value = {
            "data": [
                {
                    "id": "543",
                    "card": {"exp_month": 9, "exp_year": 2020},
                    "created": 123,
                },
                {
                    "id": "876",
                    "card": {"exp_month": 7, "exp_year": 2020},
                    "created": 123,
                },
            ]
        }
        with freeze_time(datetime(2020, 8, 17, 15, 23)):
            customer.update_card_expiry()
    customer.refresh_from_db()
    assert customer.is_expiring() is True
    assert customer.refresh is True
    assert date(2020, 7, 31) == customer.expiry_date
    # mail
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert Customer.MAIL_TEMPLATE_CARD_EXPIRY == message.template.slug
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert "code@pkimber.net" == mail.email
    assert 1 == mail.mailfield_set.count()
    mail_field = mail.mailfield_set.first()
    assert "name" == mail_field.key
    assert expect == mail_field.value


@pytest.mark.django_db
def test_update_card_catch_exception(caplog):
    Customer.objects.update_card_expiry("code@pkimber.net")
    assert "Cannot find a customer with email: code@pkimber.net" in str(
        caplog.records
    )


@pytest.mark.django_db
def test_update_card_expiry_no_payment_method():
    MailTemplateFactory(slug=Customer.MAIL_TEMPLATE_CARD_EXPIRY)
    customer = CustomerFactory(
        email="code@pkimber.net", name="Patrick", expiry_date=date(3000, 2, 1)
    )
    assert customer.is_expiring() is False
    assert customer.refresh is False
    with mock.patch("stripe.Customer.retrieve") as mock_customer, mock.patch(
        "stripe.PaymentMethod.list"
    ) as mock_payment_method:
        mock_customer.return_value = {
            "invoice_settings": {"default_payment_method": "876"}
        }
        mock_payment_method.return_value = {"data": []}
        with freeze_time(datetime(2020, 8, 17, 15, 23)):
            customer.update_card_expiry()
    customer.refresh_from_db()
    assert customer.is_expiring() is False
    assert customer.refresh is False
    assert date(3000, 2, 1) == customer.expiry_date
    # mail
    assert 0 == Message.objects.count()


@pytest.mark.django_db
def test_update_card_no_email():
    with pytest.raises(CheckoutError) as e:
        Customer.objects.update_card_expiry("")
    assert "Cannot get a customer without an email address" in str(e.value)
