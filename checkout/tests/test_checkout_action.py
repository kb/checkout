# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from checkout.models import CheckoutAction


@pytest.mark.django_db
def test_is_card_refresh():
    call_command("init_app_checkout")
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    assert action.is_card_refresh() is False
    action = CheckoutAction.objects.get(slug=CheckoutAction.CARD_REFRESH)
    assert action.is_card_refresh() is True


@pytest.mark.django_db
def test_is_charge():
    call_command("init_app_checkout")
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    assert action.is_charge() is False
    action = CheckoutAction.objects.get(slug=CheckoutAction.CHARGE)
    assert action.is_charge() is True


@pytest.mark.django_db
def test_is_invoice():
    call_command("init_app_checkout")
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    assert action.is_invoice() is False
    action = CheckoutAction.objects.get(slug=CheckoutAction.INVOICE)
    assert action.is_invoice() is True


@pytest.mark.django_db
def test_is_manual():
    call_command("init_app_checkout")
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    assert action.is_manual() is False
    action = CheckoutAction.objects.get(slug=CheckoutAction.MANUAL)
    assert action.is_manual() is True


@pytest.mark.django_db
def test_is_pay_on_session():
    call_command("init_app_checkout")
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN)
    assert action.is_pay_on_session() is False
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    assert action.is_pay_on_session() is True


@pytest.mark.django_db
def test_is_payment():
    call_command("init_app_checkout")
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    assert action.is_payment() is False
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
    assert action.is_payment() is True


@pytest.mark.django_db
def test_is_payment_plan():
    call_command("init_app_checkout")
    CheckoutAction(slug=CheckoutAction.PAYMENT_PLAN_OPTION_2).save()
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    assert action.is_payment_plan() is False
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT_PLAN)
    assert action.is_payment_plan() is True
    action = CheckoutAction.objects.get(
        slug=CheckoutAction.PAYMENT_PLAN_OPTION_2
    )
    assert action.is_payment_plan() is True


@pytest.mark.django_db
def test_str():
    call_command("init_app_checkout")
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAY_ON_SESSION)
    assert "Pay on Session" == str(action)
