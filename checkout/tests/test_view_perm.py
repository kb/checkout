# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from base.tests.test_utils import PermTestCase
from checkout.models import PaymentRun
from login.tests.fixture import perm_check
from .factories import PaymentPlanFactory


@pytest.mark.django_db
def test_dash(perm_check):
    perm_check.staff(reverse("checkout.dash"))


@pytest.mark.django_db
def test_payment_plan_create(perm_check):
    perm_check.staff(reverse("checkout.payment.plan.create"))


@pytest.mark.django_db
def test_payment_plan_delete(perm_check):
    plan = PaymentPlanFactory()
    perm_check.staff(reverse("checkout.payment.plan.delete", args=[plan.pk]))


@pytest.mark.django_db
def test_payment_plan_list(perm_check):
    perm_check.staff(reverse("checkout.payment.plan.list"))


@pytest.mark.django_db
def test_payment_plan_update(perm_check):
    plan = PaymentPlanFactory()
    perm_check.staff(reverse("checkout.payment.plan.update", args=[plan.pk]))


@pytest.mark.django_db
def test_payment_run_item_list(perm_check):
    obj = PaymentRun.objects.create_payment_run()
    perm_check.staff(reverse("checkout.payment.run.item.list", args=[obj.pk]))


@pytest.mark.django_db
def test_payment_run_list(perm_check):
    perm_check.staff(reverse("checkout.payment.run.list"))


@pytest.mark.django_db
def test_settings(perm_check):
    perm_check.staff(reverse("checkout.settings"))
