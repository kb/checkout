# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from checkout.models import CheckoutState
from checkout.tests.factories import CheckoutStateFactory


@pytest.mark.django_db
def test_is_cancel():
    CheckoutStateFactory(slug=CheckoutState.CANCEL)
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.CANCEL)
    assert checkout_state.is_cancel() is True


@pytest.mark.django_db
def test_is_fail():
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    assert checkout_state.is_fail() is True


@pytest.mark.django_db
def test_is_pay_on_session():
    call_command("init_app_checkout")
    checkout_state = CheckoutState.objects.get(
        slug=CheckoutState.PAY_ON_SESSION
    )
    assert checkout_state.is_pay_on_session() is True


@pytest.mark.django_db
def test_is_pending():
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.PENDING)
    assert checkout_state.is_pending() is True


@pytest.mark.django_db
def test_is_request():
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.REQUEST)
    assert checkout_state.is_request() is True


@pytest.mark.django_db
def test_is_success():
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    assert checkout_state.is_success() is True


@pytest.mark.django_db
def test_str():
    checkout_state = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    assert "Success" == str(checkout_state)
