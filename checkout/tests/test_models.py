# -*- encoding: utf-8 -*-
import pytest

from checkout.models import Checkout, CheckoutError, system_generated_user
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_system_generated_user():
    user = UserFactory(username=Checkout.SYSTEM_GENERATED_USER_NAME)
    assert user == system_generated_user()


@pytest.mark.django_db
def test_system_generated_user_does_not_exist():
    with pytest.raises(CheckoutError) as e:
        system_generated_user()
    assert "Cannot find '{}' user".format(
        Checkout.SYSTEM_GENERATED_USER_NAME
    ) in str(e.value)
