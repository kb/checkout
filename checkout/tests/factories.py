# -*- encoding: utf-8 -*-
import factory

from datetime import date
from decimal import Decimal

from django.utils import timezone

from checkout.models import (
    Checkout,
    CheckoutAdditional,
    CheckoutSettings,
    CheckoutState,
    Customer,
    CustomerPayment,
    ObjectPaymentPlan,
    ObjectPaymentPlanInstalment,
    PaymentPlan,
)
from login.tests.fixture import UserFactory


class CustomerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Customer

    @factory.sequence
    def customer_id(n):
        return "{:02d}_customer_id".format(n)

    @factory.sequence
    def email(n):
        return "{:02d}@pkimber.net".format(n)

    @factory.sequence
    def name(n):
        return "{:02d}_name".format(n)


class CustomerPaymentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CustomerPayment

    customer = factory.SubFactory(CustomerFactory)
    user = factory.SubFactory(UserFactory)

    @factory.sequence
    def description(n):
        return "{:02d}_description".format(n)

    @factory.sequence
    def total(n):
        return Decimal("{}".format(n))


class CheckoutFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Checkout

    customer = factory.SubFactory(CustomerFactory)
    checkout_date = timezone.now()


class CheckoutAdditionalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CheckoutAdditional

    checkout = factory.SubFactory(CheckoutFactory)


class CheckoutStateFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CheckoutState


class PaymentPlanFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PaymentPlan

    deposit = 50
    count = 2
    interval = 1

    @factory.sequence
    def name(n):
        return "{:02d}_name".format(n)

    @factory.sequence
    def slug(n):
        return "{:02d}_slug".format(n)


class ObjectPaymentPlanFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ObjectPaymentPlan

    payment_plan = factory.SubFactory(PaymentPlanFactory)
    total = Decimal("100.00")


class ObjectPaymentPlanInstalmentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ObjectPaymentPlanInstalment

    deposit = False
    due = date.today()
    amount = Decimal("99.99")

    @factory.sequence
    def count(n):
        return n


class CheckoutSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CheckoutSettings

    default_payment_plan = factory.SubFactory(PaymentPlanFactory)
    default_payment_plan_option_2 = factory.SubFactory(PaymentPlanFactory)
