# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from .factories import PaymentPlanFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_checkout_settings(client):
    payment_plan_1 = PaymentPlanFactory()
    payment_plan_2 = PaymentPlanFactory()
    user = UserFactory(is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("checkout.settings"),
        data={
            "default_payment_plan": payment_plan_1.pk,
            "default_payment_plan_option_2": payment_plan_2.pk,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("project.settings") == response.url
