# -*- encoding: utf-8 -*-
import pytest

from checkout.models import Checkout
from checkout.tasks import (
    payment_intent_fulfillment,
    payment_intent_on_session_fulfillment,
    process_payments,
    refresh_card_expiry_dates,
    send_failure_emails,
    send_payment_reminder_emails,
    setup_intent_fulfillment,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_payment_intent_fulfillment():
    assert 0 == payment_intent_fulfillment()


@pytest.mark.django_db
def test_payment_intent_on_session_fulfillment():
    assert 0 == payment_intent_on_session_fulfillment()


@pytest.mark.django_db
def test_process_payments():
    UserFactory(username=Checkout.SYSTEM_GENERATED_USER_NAME)
    assert (0, 0) == process_payments()


@pytest.mark.django_db
def test_refresh_card_expiry_dates():
    refresh_card_expiry_dates()


@pytest.mark.django_db
def test_send_failure_emails():
    send_failure_emails()


# See 'example_checkout/tasks.py'
# def test_send_on_session_payment_emails():


@pytest.mark.django_db
def test_send_payment_reminder_emails():
    send_payment_reminder_emails()


@pytest.mark.django_db
def test_setup_intent_fulfillment():
    assert 0 == setup_intent_fulfillment()
