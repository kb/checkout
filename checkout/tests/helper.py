# -*- encoding: utf-8 -*-
from decimal import Decimal

from django.urls import reverse

from base.tests.model_maker import clean_and_save
from checkout.models import CheckoutAction, CheckoutState
from checkout.tests.factories import CheckoutFactory


def check_contact(model_instance):
    """The ``Contact`` model.

    - The manager needs an ``os_fees`` method.
    - The project must include a view showing users with outstanding fees.

    """
    model_instance.objects.os_fees()
    reverse("report.contact.osfees")


def check_checkout(model_instance):
    """The 'Checkout' model links to generic content."""
    # @property list valid of actions e.g. ``return [CheckoutAction.PAYMENT]``
    # model_instance.checkout_actions
    # @property check the model is in the correct state for taking payment
    #
    # TODO PJK 22/08/2019
    # Not sure if we need this for the SCA version of checkout?
    # model_instance.checkout_can_charge
    # @property the email address of the person who is paying
    model_instance.checkout_email
    # @property ``list`` of strings
    model_instance.checkout_description
    # method called on success, passing in the checkout action
    # model_instance.checkout_mail(CheckoutAction.objects.payment)
    # @property the name of the person who is paying
    model_instance.checkout_first_name
    model_instance.checkout_name
    # @property the total payment
    model_instance.checkout_total
    # ``method`` to update the object to record the payment failure.
    # Called from within a transaction so you can update the model.
    # Note: This method should update the ``model_instance`` AND ``save`` it.
    action = CheckoutAction.objects.get(slug=CheckoutAction.PAYMENT)
    state_fail = CheckoutState.objects.get(slug=CheckoutState.FAIL)
    checkout = CheckoutFactory(
        action=action,
        content_object=model_instance,
        state=state_fail,
        total=Decimal("98.76"),
    )
    model_instance.checkout_fail(checkout)
    # TODO PJK 22/08/2019
    # Not sure if we need this for the SCA version of checkout?
    # method returning a url
    # model_instance.checkout_fail_url(1)
    # Update the object to record the payment success.
    # Called from within a transaction so you can update the model.
    # Note: This method should update the ``model_instance`` AND ``save`` it.
    # We pass in the 'checkout' so the model instance can send an email etc.
    state_success = CheckoutState.objects.get(slug=CheckoutState.SUCCESS)
    checkout = CheckoutFactory(
        action=action,
        content_object=model_instance,
        state=state_success,
        total=Decimal("123.45"),
    )
    model_instance.checkout_success(checkout)
    # TODO PJK 22/08/2019
    # Not sure if we need this for the SCA version of checkout?
    # method returning a url
    # model_instance.checkout_success_url(1)
    model_instance.get_absolute_url()
    clean_and_save(model_instance)
    # old _____________________________________________________________________
    # can we create a payment instance (need to set url before save).
    # checkout = model_instance.create_checkout(token='123')
    # assert payment.paymentline_set.count() > 0, "no payment lines"
    # checkout.url = reverse('project.home')
    # checkout.url_failure = reverse('project.home')
    # clean_and_save(checkout)
    # can the generic content be paid?
    # required attributes
    # obj = model_instance.checkout('token')
    # if not type(obj) == Checkout:
    #     raise CheckoutError("{}.checkout' should create a 'Checkout' instance")
    # description = model_instance.checkout_description
    # if not type(description) == List:
    #     raise CheckoutError("{}.checkout_description' should create a 'List'")
    # actions = model_instance.checkout_actions
    # if not isinstance(actions, list):
    #    raise CheckoutError(
    #        "{}.checkout_actions' should return a list of "
    #        "checkout actions".format(model_instance.__class__.__name__)
    #    )
    # model_instance.checkout_state
    # model_instance.set_checkout_state(CheckoutState.objects.success)
    # if not url:
    #    raise CheckoutError("{}.checkout_fail_url' should return a url")
    # if not url:
    #    raise CheckoutError("{}.checkout_success_url' should return a url")
    # do we have mail templates for paid and pay later?
    # assert model_instance.mail_template_name
    # the generic content must implement 'allow_pay_later'
    # model_instance.allow_pay_later()
    # old _____________________________________________________________________
    # the generic content must implement 'get_absolute_url'


def check_object_payment_plan(model_instance):
    """The 'ObjectPaymentPlan' model links to generic content."""
    model_instance.checkout_email
    model_instance.checkout_name
    # called by the payment plan when a deposit or instalment charge fails.
    model_instance.instalment_fail
    # called by the payment plan when a deposit or instalment charge succeeds.
    model_instance.instalment_success


class MockCustomer:
    """I can't work out how to setup mock objects, so hacked it..."""

    @property
    def id(self):
        return 321


class MockIntent:
    """I can't work out how to setup mock objects, so hacked it..."""

    def __init__(
        self,
        intent_id=None,
        code=None,
        decline_code=None,
        message=None,
        status=None,
    ):
        self.code = code
        self.decline_code = decline_code
        self.intent_id = intent_id
        self.message = message or ""
        self.status = status

    def __getitem__(self, item):
        if item == "id":
            return self.intent_id
        elif item == "last_payment_error":
            if self.code and self.decline_code:
                raise AttributeError(
                    "'MockIntent' needs a 'code' or a 'decline_code' (not both)"
                )
            if self.code:
                return {"code": self.code, "message": self.message}
            elif self.decline_code:
                return {
                    "decline_code": self.decline_code,
                    "message": self.message,
                }
            else:
                raise AttributeError(
                    "'MockIntent' needs a 'code' or 'decline_code'"
                )
        elif item == "status":
            return self.status
        else:
            raise AttributeError(
                "'MockIntent' does not know what to do with '{}'".format(item)
            )

    @property
    def client_secret(self):
        return "shhhh"

    @property
    def payment_method(self):
        return {}
