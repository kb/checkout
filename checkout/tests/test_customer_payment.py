# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal

from checkout.models import CustomerPayment
from checkout.tests.factories import CustomerFactory, CustomerPaymentFactory
from checkout.tests.helper import check_checkout


@pytest.mark.django_db
def test_check_checkout():
    x = CustomerPaymentFactory()
    check_checkout(x)


@pytest.mark.django_db
def test_pks_with_email():
    c1 = CustomerPaymentFactory(
        customer=CustomerFactory(email="code@pkimber.net")
    )
    c2 = CustomerPaymentFactory(
        customer=CustomerFactory(email="patrick@pkimber.net")
    )
    c3 = CustomerPaymentFactory(
        customer=CustomerFactory(email="code@PKimber.net")
    )
    assert [c1.pk, c3.pk] == [
        x
        for x in CustomerPayment.objects.pks_with_email(
            "code@pkimber.net", [c1.pk, c2.pk, c3.pk]
        )
    ]


@pytest.mark.django_db
def test_str():
    assert "code@pkimber.net Apple 12.34" == str(
        CustomerPaymentFactory(
            customer=CustomerFactory(email="code@pkimber.net"),
            description="Apple",
            total=Decimal("12.34"),
        )
    )
