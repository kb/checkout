# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_init_app():
    call_command("init_app_checkout")


@pytest.mark.django_db
def test_payment_intent_fulfillment():
    call_command("payment_intent_fulfillment")


@pytest.mark.django_db
def test_payment_intent_fulfillment():
    call_command("payment_intent_on_session_fulfillment")


@pytest.mark.django_db
def test_send_on_session_payment_emails():
    call_command("send_on_session_payment_emails")


@pytest.mark.django_db
def test_setup_intent_fulfillment():
    call_command("setup_intent_fulfillment")
