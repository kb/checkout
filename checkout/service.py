# -*- encoding: utf-8 -*-
import attr
import collections
import logging
import stripe

from decimal import Decimal
from django.conf import settings


CURRENCY = "GBP"
logger = logging.getLogger(__name__)


def _stripe_error(e):
    return "http body: '{}' http status: '{}'".format(
        e.http_body, e.http_status
    )


@attr.s
class PaymentMethod:
    """Payment method.

    https://stripe.com/docs/api/payment_methods/object#payment_method_object

    """

    created = attr.ib()
    expiry_month = attr.ib()
    expiry_year = attr.ib()


class StripeError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class StripeCheckout:
    def as_pennies(self, total):
        return int(total * Decimal("100"))

    def attach_payment_method_to_customer(self, setup_intent, customer_id):
        """Attach a Stripe payment method to a customer.

        .. tip:: The ``setup_intent`` is not a simple ``dict`` as the Stripe
                 documentation suggests.  It is a *proper* class with methods.

        """
        payment_method_id = None
        stripe.api_key = settings.STRIPE_SECRET_KEY
        try:
            payment_method = stripe.PaymentMethod.attach(
                setup_intent.payment_method, customer=customer_id
            )
            payment_method_id = payment_method["id"]
        except stripe.error.InvalidRequestError as e:
            msg = "Cannot attach payment method to customer: {}".format(
                _stripe_error(e)
            )
            logger.error(msg)
            logger.exception(e)
            raise StripeError(msg) from e
        return payment_method_id

    def create_customer(self, name, email):
        """Use the Stripe API to create a customer.

        .. tip:: The ``setup_intent`` is not a simple ``dict`` as the Stripe
                 documentation suggests.  It is a *proper* class with methods.

        """
        try:
            stripe.api_key = settings.STRIPE_SECRET_KEY
            customer = stripe.Customer.create(email=email, description=name)
            # print(customer.id)
            return customer.id
        except (
            stripe.error.InvalidRequestError,
            stripe.error.StripeError,
        ) as e:
            msg = "Error creating Stripe customer '{}': {}".format(
                email, _stripe_error(e)
            )
            logger.error(msg)
            logger.exception(e)
            raise StripeError(msg) from e

    def create_payment_intent(self, total):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        try:
            intent = stripe.PaymentIntent.create(
                amount=self.as_pennies(total), currency=CURRENCY
            )
            return intent["id"]
        except stripe.error.StripeError as e:
            msg = "Create payment intent - failed: {}".format(_stripe_error(e))
            logger.error(msg)
            logger.exception(e)
            raise StripeError(msg) from e

    def create_payment_intent_for_customer(
        self, customer_id, payment_method_id, total
    ):
        """Create a payment intent and collect the cash.

        From, https://stripe.com/docs/api/payment_intents/create

          After the ``PaymentIntent`` is created, attach a payment method and
          confirm to continue the payment.
          When ``confirm=true`` is used during creation, it is equivalent to
          creating and confirming the ``PaymentIntent`` in the same call.

        """
        stripe.api_key = settings.STRIPE_SECRET_KEY
        try:
            intent = stripe.PaymentIntent.create(
                amount=self.as_pennies(total),
                confirm=True,
                currency=CURRENCY,
                customer=customer_id,
                off_session=True,
                payment_method=payment_method_id,
                payment_method_types=["card"],
                # setup_future_usage="off_session",
            )
            return intent
        except stripe.error.CardError as e:
            # Since it's a decline, stripe.error.CardError will be caught
            # body = e.json_body
            # err = body.get("error", {})
            # print("Status is: {}".format(e.http_status))
            # print("Type is: {}".format(err.get("type")))
            # print("Code is: {}".format(err.get("code")))
            ## param is '' in this case
            # print("Param is: {}".format(err.get("param")))
            # print("Message is: {}".format(err.get("message")))
            # print(json.dumps(body, indent=4))
            return e.json_body["error"]["payment_intent"]
        except stripe.error.StripeError as e:
            msg = "Create payment intent for customer - failed: {}".format(
                _stripe_error(e)
            )
            logger.error(msg)
            logger.exception(e)
            raise StripeError(msg) from e

    def create_payment_intent_on_session(
        self, customer_id, payment_method_id, total
    ):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        try:
            intent = stripe.PaymentIntent.create(
                amount=self.as_pennies(total),
                currency=CURRENCY,
                customer=customer_id,
                payment_method=payment_method_id,
                # setup_future_usage="off_session",
            )
            return intent["id"]
        except stripe.error.StripeError as e:
            msg = "Create pay on-session intent - failed: {}".format(
                _stripe_error(e)
            )
            logger.error(msg)
            logger.exception(e)
            raise StripeError(msg) from e

    def create_setup_intent(self):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        try:
            intent = stripe.SetupIntent.create(
                payment_method_types=["card"], usage="off_session"
            )
            return intent["id"]
        except stripe.error.StripeError as e:
            msg = "Create setup intent - failed: {}".format(_stripe_error(e))
            logger.error(msg)
            logger.exception(e)
            raise StripeError(msg) from e

    def customer(self, customer_id):
        """Get the Stripe customer."""
        stripe.api_key = settings.STRIPE_SECRET_KEY
        return stripe.Customer.retrieve(customer_id)

    def payment_intent(self, payment_intent_id):
        """Get the Stripe payment intent."""
        stripe.api_key = settings.STRIPE_SECRET_KEY
        return stripe.PaymentIntent.retrieve(payment_intent_id)

    def payment_intent_list(self, customer_id):
        """Get the Stripe payment intent."""
        stripe.api_key = settings.STRIPE_SECRET_KEY
        return stripe.PaymentIntent.list(customer_id)

    def payment_methods(self, customer_id):
        result = collections.OrderedDict()
        stripe.api_key = settings.STRIPE_SECRET_KEY
        json_data = stripe.PaymentMethod.list(
            customer=customer_id, type="card", limit=10
        )
        # print(json.dumps(json_data, indent=4))
        data = json_data["data"]
        for row in data:
            payment_method_id = row["id"]
            card = row["card"]
            created = int(row["created"])
            month = int(card["exp_month"])
            year = int(card["exp_year"])
            result[payment_method_id] = PaymentMethod(
                created=created, expiry_month=month, expiry_year=year
            )
        # print(json.dumps(result, indent=4))
        return result

    def setup_intent(self, setup_intent_id):
        """Get the Stripe payment plan intent."""
        stripe.api_key = settings.STRIPE_SECRET_KEY
        return stripe.SetupIntent.retrieve(setup_intent_id)

    def update_customer(self, customer_id, name):
        """Use the Stripe API to update a customer."""
        try:
            stripe.api_key = settings.STRIPE_SECRET_KEY
            stripe.Customer.modify(customer_id, name=name)
        except stripe.error.StripeError as e:
            msg = "Error updating Stripe customer name '{}': {}".format(
                customer_id, _stripe_error(e)
            )
            logger.error(msg)
            logger.exception(e)
            raise StripeError(msg) from e

    def update_customer_default_payment_method(
        self, customer_id, payment_method_id
    ):
        """Update the 'default_payment_method' for a 'Customer'."""
        try:
            stripe.api_key = settings.STRIPE_SECRET_KEY
            stripe.Customer.modify(
                customer_id,
                invoice_settings={"default_payment_method": payment_method_id},
            )
        except stripe.error.StripeError as e:
            msg = (
                "Error updating Stripe customer payment method "
                "'{}': {}".format(customer_id, _stripe_error(e))
            )
            logger.error(msg)
            logger.exception(e)
            raise StripeError(msg) from e
