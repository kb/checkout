# -*- encoding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand

from checkout.models import (
    CheckoutAction,
    CheckoutState,
    Customer,
    ObjectPaymentPlanInstalment,
)
from mail.models import MailTemplate


class Command(BaseCommand):
    help = "Initialise 'checkout' application"

    def _init_action(self):
        try:
            CheckoutAction.objects.get(slug=CheckoutState.PAY_ON_SESSION)
        except CheckoutAction.DoesNotExist:
            x = CheckoutAction(
                slug=CheckoutAction.PAY_ON_SESSION, name="Pay on Session"
            )
            x.save()

    def _init_state(self):
        try:
            CheckoutState.objects.get(slug=CheckoutState.CANCEL)
        except CheckoutState.DoesNotExist:
            x = CheckoutState(slug=CheckoutState.CANCEL, name="Cancel")
            x.save()
        try:
            CheckoutState.objects.get(slug=CheckoutState.PAY_ON_SESSION)
        except CheckoutState.DoesNotExist:
            x = CheckoutState(
                slug=CheckoutState.PAY_ON_SESSION, name="Pay on Session"
            )
            x.save()

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        MailTemplate.objects.init_mail_template(
            Customer.MAIL_TEMPLATE_CARD_EXPIRY,
            "Re: Card Expiry",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the customer\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Card Expiry",
            description="Your card will expire soon...",
        )
        MailTemplate.objects.init_mail_template(
            Customer.MAIL_TEMPLATE_CARD_REFRESH_REQUEST,
            "Please update your card details.",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the customer.\n"
                "{{ message }} an optional extra message.\n"
                "{{ url }} the URL of the page.\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Please update your card details",
            description="Please update your card details.",
        )
        # https://www.kbsoftware.co.uk/crm/ticket/3085/
        MailTemplate.objects.init_mail_template(
            Customer.MAIL_TEMPLATE_REMINDER,
            "Automated Payment Reminder",
            (
                "You can add the following variables to the template:\n"
                "{{ due }} due date of the payment\n"
                "{{ name }} name of the customer\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Automated Payment Reminder",
            description="Your payment is due soon...",
        )
        # https://www.kbsoftware.co.uk/crm/ticket/4075/
        # https://www.kbsoftware.co.uk/crm/ticket/4539/
        # See duplicate code below ``Customer.MAIL_TEMPLATE_PAY_ON_SESSION``
        # MailTemplate.objects.init_mail_template(
        #     ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_AUTH,
        #     "On session payment emails",
        #     (
        #         "You can add the following variables to the template:\n"
        #         "{{ name }} name of the customer\n"
        #         "{{ description }} description of the payment\n"
        #         "{{ amount }} of the instalment\n"
        #     ),
        #     False,
        #     settings.MAIL_TEMPLATE_TYPE,
        #     subject="On session payment",
        #     description="Please authorise an instalment for your payment plan",
        # )
        # https://www.kbsoftware.co.uk/crm/ticket/4075/
        # https://www.kbsoftware.co.uk/crm/ticket/4539/
        # See duplicate code below ``Customer.MAIL_TEMPLATE_PAY_ON_SESSION``
        # User *not* logged in
        MailTemplate.objects.init_mail_template(
            ObjectPaymentPlanInstalment.MAIL_TEMPLATE_ON_SESSION_PAYMENT_ANON,
            "On session payment emails",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the customer\n"
                "{{ description }} description of the payment\n"
                "{{ url }} payment URL\n"
                "{{ amount }} of the instalment\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="On session payment",
            description="Please authorise an instalment for your payment plan",
        )
        # https://www.kbsoftware.co.uk/crm/ticket/4401/
        MailTemplate.objects.init_mail_template(
            Customer.MAIL_TEMPLATE_PAYMENT_FAILED,
            "Payment Failed",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the customer\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Payment Failed",
            description="Your payment failed...",
        )
        # https://www.kbsoftware.co.uk/crm/ticket/4539/
        # MailTemplate.objects.init_mail_template(
        #    Customer.MAIL_TEMPLATE_PAY_ON_SESSION,
        #    "Please authorise your payment",
        #    (
        #        "You can add the following variables to the template:\n"
        #        "{{ name }} name of the customer\n"
        #        "{{ description }} description of the payment\n"
        #    ),
        #    False,
        #    settings.MAIL_TEMPLATE_TYPE,
        #    subject="Login and authorise your payment",
        #    description="Please login and authorise your payment...",
        # )
        self._init_action()
        self._init_state()
        self.stdout.write("{} - Complete".format(self.help))
