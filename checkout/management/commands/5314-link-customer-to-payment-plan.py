# -*- encoding: utf-8 -*-
import logging

from django.core.management.base import BaseCommand

from checkout.models import (
    CheckoutError,
    Customer,
    CustomerDoesNotExistError,
    ObjectPaymentPlan,
)


class Command(BaseCommand):
    """Link customer to payment plan.

    https://www.kbsoftware.co.uk/crm/ticket/5314/

    """

    help = "Link customer to payment plan"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        qs = ObjectPaymentPlan.objects.all().order_by("pk")
        pks = [x.pk for x in qs]
        for pk in pks:
            object_payment_plan = ObjectPaymentPlan.objects.get(pk=pk)
            customer = None
            if not customer:
                try:
                    customer = Customer.objects.get_customer(
                        object_payment_plan.content_object.checkout_email
                    )
                except (CheckoutError, CustomerDoesNotExistError):
                    pass
            if not customer:
                try:
                    # Try the 'Enrol' 'email' field...
                    customer = Customer.objects.get_customer(
                        object_payment_plan.content_object.email
                    )
                    self.stdout.write(
                        "Cannot find {} (found {})".format(
                            object_payment_plan.content_object.checkout_email,
                            object_payment_plan.content_object.email,
                        )
                    )
                except (CheckoutError, CustomerDoesNotExistError):
                    pass
            if not customer:
                email = None
                if (
                    object_payment_plan.content_object.checkout_email
                    == "kathryn.perrin1960@gmail.com"
                ):
                    email = "kathryn.perrin@btinternet.com"
                elif (
                    object_payment_plan.content_object.checkout_email
                    == "CBrotherton@ridge.co.uk"
                ):
                    email = "christine.brotherton@capita.co.uk"
                if email:
                    try:
                        # Try the 'Enrol' 'email' field...
                        customer = Customer.objects.get_customer(email)
                        self.stdout.write(
                            "Cannot find {} (found {} - via lookup)".format(
                                object_payment_plan.content_object.checkout_email,
                                email,
                            )
                        )
                    except (CheckoutError, CustomerDoesNotExistError):
                        pass

            if not customer:
                self.stdout.write(
                    "{} - {} {} {}".format(
                        object_payment_plan.created,
                        object_payment_plan.pk,
                        object_payment_plan.content_object.checkout_email,
                        object_payment_plan.content_object.email,
                    )
                )
        self.stdout.write(
            "{} - {} payments plans - Complete...".format(len(pks), self.help)
        )
