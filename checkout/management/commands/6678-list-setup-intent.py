# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from checkout.models import Checkout, CheckoutAction


class Command(BaseCommand):
    help = "List pending setup intent #6678"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        qs = Checkout.objects.pending(
            [
                CheckoutAction.PAYMENT_PLAN,
                CheckoutAction.PAYMENT_PLAN_OPTION_2,
                CheckoutAction.CARD_REFRESH,
            ]
        )
        for x in qs:
            self.stdout.write("\n")
            self.stdout.write(f"Checkout: {x.pk}")
            self.stdout.write(
                f"  {type(x.content_object)} pk {x.content_object.pk}"
            )
            self.stdout.write(f"  {x.content_object.checkout_name}")
            if x.content_object.checkout_email:
                self.stdout.write(f"  {x.content_object.checkout_email}")
            else:
                self.stdout.write(
                    "  **** Missing 'content_object.checkout_email' ****"
                )
        self.stdout.write(f"{self.help} - Complete")
