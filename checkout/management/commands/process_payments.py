# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from checkout.models import PaymentRun


class Command(BaseCommand):
    help = "Payment run - process payments"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count, error_count = PaymentRun.objects.process_payments()
        self.stdout.write(
            "{} - Complete.  {} payments due.  Returned {} errors".format(
                self.help, count, error_count
            )
        )
