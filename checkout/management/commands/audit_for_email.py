# -*- encoding: utf-8 -*-
import prettyprinter

from django.core.management.base import BaseCommand

from checkout.models import Customer
from checkout.service import StripeCheckout


class Command(BaseCommand):
    help = "Checkout audit for an email (payment and setup intent)"

    def add_arguments(self, parser):
        parser.add_argument("email", type=str)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        prettyprinter.prettyprinter.install_extras(["attrs"])
        email = options["email"]
        self.stdout.write("email: {}".format(email))
        customer = Customer.objects.get_customer(email)
        stripe_checkout = StripeCheckout()
        payment_intents = stripe_checkout.payment_intent_list(
            customer.customer_id
        )
        for x in payment_intents:
            prettyprinter.pprint(x)
        self.stdout.write("{} - Complete".format(self.help))
