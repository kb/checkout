# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from checkout.tasks import payment_intent_on_session_fulfillment


class Command(BaseCommand):
    help = "Checkout - Stripe payment intent (on session) fulfillment"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = payment_intent_on_session_fulfillment()
        self.stdout.write(
            "{} - Complete - {} records".format(self.help, count or 0)
        )
