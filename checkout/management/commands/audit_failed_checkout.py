# -*- encoding: utf-8 -*-
import pytz

from datetime import datetime
from django.core.management.base import BaseCommand

from checkout.models import Checkout, CheckoutState
from checkout.service import StripeCheckout


class Command(BaseCommand):
    help = "Checkout audit for failed transactions #5212"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        failed_state = CheckoutState.objects.get(slug=CheckoutState.FAIL)
        qs = (
            Checkout.objects.filter(
                state=failed_state,
                created__gt=datetime(2020, 8, 31, tzinfo=pytz.utc),
            )
            .exclude(setup_intent_id__isnull=True)
            .exclude(setup_intent_id__exact="")
        )
        checkout_pks = [x.pk for x in qs]
        self.stdout.write("Found {} failed checkouts".format(len(checkout_pks)))
        stripe_checkout = StripeCheckout()
        for pk in checkout_pks:
            setup_intent_id = None
            checkout = Checkout.objects.get(pk=pk)
            if checkout.setup_intent_id:
                setup_intent_id = checkout.setup_intent_id
                setup_intent = stripe_checkout.setup_intent(setup_intent_id)
                status = setup_intent["status"]
                self.stdout.write(
                    "{}. {} - {} - {}".format(
                        checkout.pk,
                        checkout.created.strftime("%d/%m/%Y %H:%M"),
                        setup_intent_id,
                        status,
                    )
                )
        self.stdout.write("{} - Complete".format(self.help))
