# -*- encoding: utf-8 -*-
from collections import OrderedDict
from datetime import datetime, time

from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from django.db import transaction

from checkout.models import (
    Checkout,
    ObjectPaymentPlanInstalment,
    PaymentRun,
    PaymentRunItem,
)


class Command(BaseCommand):
    help = "Initialise Payment Run #1342"

    def _date_and_midday(self, d):
        return datetime.combine(d, time(12, 0))

    def handle(self, *args, **options):
        content_type = ContentType.objects.get_for_model(
            ObjectPaymentPlanInstalment
        )
        qs = Checkout.objects.filter(
            content_type=content_type, user__isnull=True
        ).order_by("pk")
        self.stdout.write("{} checkout rows for instalments".format(qs.count()))
        data = OrderedDict()
        for obj in qs:
            try:
                # is the checkout already assigned to a payment run?
                PaymentRunItem.objects.get(checkout=obj)
            except PaymentRunItem.DoesNotExist:
                d = obj.created.date()
                if not d in data:
                    data[d] = []
                data[d].append(obj.pk)
        for k, v in data.items():
            with transaction.atomic():
                payment_run = PaymentRun.objects.create_payment_run()
                payment_run.created = self._date_and_midday(k)
                payment_run.save()
                self.stdout.write(
                    "{} [{}] {}".format(
                        k,
                        payment_run.created.strftime("%d/%m/%Y %H:%M"),
                        len(v),
                    )
                )
                for pk in v:
                    checkout = Checkout.objects.get(pk=pk)
                    instalment = checkout.content_object
                    item = PaymentRunItem.objects.create_payment_run_item(
                        payment_run, instalment
                    )
                    item.created = checkout.created
                    item.checkout = checkout
                    item.save()
        self.stdout.write("Payment run initialised...")
