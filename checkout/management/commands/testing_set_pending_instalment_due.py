# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from checkout.models import CheckoutState, ObjectPaymentPlanInstalment
from datetime import date
from dateutil.relativedelta import relativedelta


class Command(BaseCommand):
    """Testing - set pending instalment due for an object payment plan.

    FOR TESTING ONLY...

    - To set the due date for the next instalment
    - find your payment plan e.g.
    - http://localhost:8000/checkout/object/payment/plan/1295/
    - django-admin.py testing_set_pending_instalment_due 1295
    - django-admin.py process_payments

    """

    help = "Testing - set pending instalment due"

    def add_arguments(self, parser):
        parser.add_argument("payment_plan_pk", type=int)

    def handle(self, *args, **options):
        payment_plan_pk = options["payment_plan_pk"]
        self.stdout.write(
            "{} - payment plan {}".format(self.help, payment_plan_pk)
        )
        due = date.today() + relativedelta(months=-1)
        found_success = is_pending = False
        pks = [
            x.pk
            for x in ObjectPaymentPlanInstalment.objects.filter(
                object_payment_plan__pk=payment_plan_pk
            ).order_by("count")
        ]
        for pk in pks:
            x = ObjectPaymentPlanInstalment.objects.get(pk=pk)
            state = ""
            if x.state.slug == CheckoutState.SUCCESS:
                found_success = True
                state = "<<< {}".format(due)
                x.due = due
                x.save()
                due = due + relativedelta(days=+1)
            if found_success and x.state.slug == CheckoutState.PENDING:
                x.due = date.today()
                x.save()
                is_pending = True
            self.stdout.write(
                "{}. {} {} {}".format(
                    x.count, x.due.strftime("%d/%m/%Y"), x.state.slug, state
                )
            )
            if is_pending:
                break
        self.stdout.write("{} - Complete".format(self.help))
