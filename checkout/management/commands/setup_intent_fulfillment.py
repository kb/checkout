# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from checkout.tasks import setup_intent_fulfillment


class Command(BaseCommand):
    help = "Checkout - Stripe setup intent fulfillment"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = setup_intent_fulfillment()
        self.stdout.write(
            "{} - Complete - {} records".format(self.help, count or 0)
        )
