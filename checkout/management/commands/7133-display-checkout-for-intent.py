# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from rich.pretty import pprint

from checkout.models import Checkout


class Command(BaseCommand):
    help = "Display checkout for an intent ID #7133"

    def add_arguments(self, parser):
        parser.add_argument("intent_id", type=str)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        intent_id = options["intent_id"]
        checkout = Checkout.objects.get(payment_intent_id=intent_id)
        pprint(
            {
                "payment_intent_id": checkout.payment_intent_id,
                "action": checkout.action,
                "created": checkout.created,
                "checkout_date": checkout.checkout_date,
                "customer": checkout.customer,
                "state": checkout.state,
                "total": checkout.total,
                "decline_code": checkout.decline_code,
                "content_object": checkout.content_object,
            },
            expand_all=True,
        )
        # If you want to cancel this checkout!!
        # from checkout.models import CheckoutState
        # checkout_state = CheckoutState.objects.get(slug=CheckoutState.CANCEL)
        # checkout.state = checkout_state
        # checkout.save()
        self.stdout.write(f"{self.help} - Complete")
